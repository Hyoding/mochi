package com.ryusoftwareinc.mochi.core.notification;

import com.ryusoftwareinc.mochi.core.config.Config;
import com.ryusoftwareinc.mochi.core.config.Environment;
import com.ryusoftwareinc.mochi.core.exception.ClientException;
import com.ryusoftwareinc.mochi.core.exception.ServerException;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.time.ZonedDateTime;

@Service
@Transactional
public class NotificationService {
	
	private static final Logger logger = Logger.getLogger(NotificationService.class);
	
	@Autowired JavaMailSender emailSender;
	
	@Autowired Config config;

	@PostConstruct
	public void init() {
		Twilio.init(config.getSmsTwilioAccountSid(), config.getSmsTwilioAuthToken());
	}

	@Async
	public void sendEmail(Email email) {
		if (config.getEnv() == Environment.DEV) {
			logger.info(email);
			return;
		}
		
		MimeMessage message = emailSender.createMimeMessage();
	    try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(email.getFrom());
			helper.setTo(email.getTo().toArray(new String[email.getTo().size()]));
			helper.setCc(email.getCc().toArray(new String[email.getCc().size()]));
			helper.setBcc(email.getBcc().toArray(new String[email.getBcc().size()]));
			helper.setSubject(email.getSubject());
			helper.setText(email.getHtmlText(), true);
			for (File file : email.getAttachments()) {
				helper.addAttachment(file.getName(), file);
			}
			emailSender.send(message);
		} catch (MessagingException e) {
			logger.error(e);
			throw new ServerException("Error when sending email");
		}
	}

	@Async
	public void sendEmailToSystem(String subject, String htmlBody) {
		Email email = new Email();
		email.setSubject(subject);
		email.setFrom(config.getSystemEmail());
		email.addTo(config.getSystemEmail());
		email.setHtmlText(htmlBody);
		sendEmail(email);
	}

	@Async
	public void sendSms(SMS sms) {
		if (config.getEnv() == Environment.DEV) {
			logger.info(sms);
			return;
		}

		try {
			Message message = Message
					.creator(new PhoneNumber(sms.getTo()), // to
							new PhoneNumber(sms.getFrom()), // from
							sms.getBody())
					.create();
		} catch (Exception ex) {
			throw new ClientException(ex.getMessage());
		}
	}

	@Async
	public void sendErrorToDev(Exception ex) {
		StringBuilder sb = new StringBuilder();
		sb.append("Time: " + ZonedDateTime.now());
		sb.append("\n\n");
		sb.append(ExceptionUtils.getStackTrace(ex));
		
		Email email = new Email();
		email.setFrom(config.getSystemEmail());
		email.addTo(config.getSystemEmail());
		email.setSubject("Runtime Error caught");
		email.setHtmlText(sb.toString());
		sendEmail(email);
	}
	
	public String htmlLink(String link, String text) {
		return "<a href='" + link + "'>" + text + "</a>";
	}

}
