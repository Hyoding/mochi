package com.ryusoftwareinc.mochi.employee.feedback;

import com.ryusoftwareinc.mochi.core.http.BaseHttpRequest;

public class FeedbackDto extends BaseHttpRequest {

	private String message;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
