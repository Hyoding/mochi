package com.ryusoftwareinc.mochi.business.service;

import com.ryusoftwareinc.mochi.business.model.*;
import com.ryusoftwareinc.mochi.business.model.Order.OrderBuilder;
import com.ryusoftwareinc.mochi.business.repository.BusinessRepository;
import com.ryusoftwareinc.mochi.business.repository.TripRepository;
import com.ryusoftwareinc.mochi.core.config.Config;
import com.ryusoftwareinc.mochi.core.exception.ClientException;
import com.ryusoftwareinc.mochi.core.exception.ConcerningClientException;
import com.ryusoftwareinc.mochi.core.exception.EntityKeyNotProvidedException;
import com.ryusoftwareinc.mochi.core.model.Address;
import com.ryusoftwareinc.mochi.core.notification.Email;
import com.ryusoftwareinc.mochi.core.notification.NotificationService;
import com.ryusoftwareinc.mochi.core.service.AbstractEntityService;
import com.ryusoftwareinc.mochi.core.util.DateTimeUtil;
import com.ryusoftwareinc.mochi.core.util.PasswordAuthentication;
import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.employee.model.EmployeeDto;
import com.ryusoftwareinc.mochi.employee.model.EmployeeType;
import com.ryusoftwareinc.mochi.employee.resetPassword.ResetPasswordService;
import com.ryusoftwareinc.mochi.employee.service.EmployeeService;
import com.ryusoftwareinc.mochi.location.mapbox.LocationService;
import com.ryusoftwareinc.mochi.subscription.SubscriptionConfigDto;
import com.ryusoftwareinc.mochi.subscription.SubscriptionConfigRepository;
import com.ryusoftwareinc.mochi.subscription.SubscriptionPlans;
import com.ryusoftwareinc.mochi.subscription.SubscriptionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class BusinessService extends AbstractEntityService<Business, BusinessRepository> {

	@Autowired
	BusinessRepository repo;
	@Autowired
	EmployeeService employeeService;
	@Autowired
	NotificationService notificationService;
	@Autowired
	Config config;
	@Autowired
	LocationService locationService;
	@Autowired
	ResetPasswordService resetPasswordService;
	@Autowired
	TagService tagService;
	@Autowired
	OrderService orderService;
	@Autowired
	SubscriptionConfigRepository subscriptionConfigRepository;
	@Autowired
	SubscriptionService subscriptionService;
	@Autowired
	TripRepository tripRepository;

	@Override
	public BusinessRepository getRepo() {
		return repo;
	}

	@Override
	public void delete(Business business) {
		business.setName(business.getName() + "_deleted_" + System.currentTimeMillis());
		super.delete(business);
	}

	public Business register(Business business, Employee owner) {
		if (repo.existsByName(business.getName())) {
			throw new ClientException("This name is already taken");
		}
		if (employeeService.getRepo().existsByEmail(owner.getEmail().toLowerCase())) {
			throw new ClientException("This employee email is already in use");
		}
		owner.setType(EmployeeType.OWNER);
		PasswordAuthentication pwdAuth = new PasswordAuthentication();
		String hashedPwd = pwdAuth.hash(owner.getPassword().toCharArray());
		owner.setPassword(hashedPwd);
		Address address = business.getAddress();
		locationService.validateAndFillAddress(address);
		business.addEmployee(owner);
		business.setSubscription(subscriptionService.getTestPlan());
		business = save(business);
		notificationService.sendEmail(generateValidateBusinessRegistrationEmail(business, owner));
		subscriptionService.auditChange(business, business.getSubscription());
		return business;
	}

	public Business validateRegistration(String businessKey) {
		Business business = findByKey(businessKey);
		if (!business.isEnabled()) {
			throw new ClientException("Registration was already rejected. Please register again");
		}
		business.setValidated(true);
		// TODO: Send another email??
		return business;
	}

	public void rejectRegistration(Business business) {
		if (!business.isEnabled()) {
			throw new ClientException("Registration was already rejected");
		}
		Employee owner = business.getEmployees().stream().findFirst().get();
		employeeService.delete(owner);
		delete(business);
	}

	public void rejectRegistration(String businessKey) {
		Business business = findByKey(businessKey);
		rejectRegistration(business);
	}

	public List<Employee> getEmployees(String businessKey) {
		return employeeService.getRepo().findEnabledByBusinessKey(businessKey);
	}

	public Employee addEmployee(EmployeeDto dto) {
		Employee newEmployee = new Employee();
		newEmployee.setType(EmployeeType.labelOf(dto.getType()));
		newEmployee.setFirstName(dto.getFirstName());
		newEmployee.setLastName(dto.getLastName());
		newEmployee.setEmail(dto.getEmail());
		newEmployee.setPhoneNumber(dto.getPhoneNumber());
		newEmployee.setPassword(createTempPwd());
		return addEmployee(newEmployee, dto.getRequester().getUserKey());
	}

	public Employee addEmployee(Employee newEmployee, String requesterKey) {
		Employee requester = employeeService.findByKey(requesterKey);
		if (!requester.isAdmin()) {
			throw new ConcerningClientException("You don't have permission to add an employee");
		}
		subscriptionService.validateAddEmployee(requester.getBusiness());
		final String emailLC = newEmployee.getEmail().toLowerCase();
		if (employeeService.getRepo().existsByEmail(emailLC)) {
			throw new ClientException("This employee email is already taken");
		}
		Business business = requester.getBusiness();
		business.addEmployee(newEmployee);
		newEmployee.setBusiness(business);
		newEmployee = employeeService.save(newEmployee);
		employeeService.sendNewEmployeeEmail(newEmployee);
		return newEmployee;
	}

	public void removeTag(String tagKey, String requesterKey) {
		Employee requester = employeeService.findByKey(requesterKey);
		if (!requester.isAdmin()) {
			throw new ConcerningClientException("You don't have permission to add an employee");
		}
		if (StringUtils.isBlank(tagKey)) {
			throw new EntityKeyNotProvidedException("Tag key needs to be provided");
		}
		Tag tag = tagService.findByKey(tagKey);
		tagService.delete(tag);
	}

	public void removeEmployee(String employeeKey, String requesterKey) {
		Employee requester = employeeService.findByKey(requesterKey);
		if (!requester.isAdmin()) {
			throw new ConcerningClientException("You don't have permission to add an employee");
		}
		if (StringUtils.isBlank(employeeKey)) {
			throw new EntityKeyNotProvidedException("Employee key needs to be provided");
		}
		Employee employee = employeeService.findByKey(employeeKey);
		employeeService.delete(employee); // we don't actually remove this
	}

	public Employee updateEmployee(EmployeeDto employeeDto) {
		Employee requester = employeeService.findByKey(employeeDto.getRequester().getUserKey());
		if (!requester.isAdmin()) {
			throw new ConcerningClientException("You don't have permission to update an employee");
		}
		Employee employee = employeeService.findByKey(employeeDto.getKey());
		employee.setType(EmployeeType.labelOf(employeeDto.getType()));
		employee.setFirstName(employeeDto.getFirstName());
		employee.setLastName(employeeDto.getLastName());
		employee.setEmail(employeeDto.getEmail());
		employee.setPhoneNumber(employeeDto.getPhoneNumber());
		return employee;
	}

	public List<OrderDto> getActiveOrders(String businessKey) {
		validateKey(businessKey);
		List<OrderDto> activeOrderDtoList = new ArrayList<>();
		for (Order order : orderService.getRepo().findActiveOrders(businessKey)) {
			Trip trip = order.getTrip();
			final EmployeeDto driver = trip == null ? null : new EmployeeDto(trip.getEmployee());
			OrderDto orderDto = new OrderDto(order, driver);
			orderDto.setTrackUrl(config.getUrlWebTrack() + "/" + order.getKey());
			activeOrderDtoList.add(orderDto);
		}
		return activeOrderDtoList;
	}

	public List<Order> getOrdersToDeliver(String businessKey) {
		validateKey(businessKey);
		return orderService.getRepo().findOrdersToDeliver(businessKey);
	}

	public List<Order> getOrderHistory(String businessKey) {
		validateKey(businessKey);
		return orderService.getRepo().findOrderHistory(businessKey);
	}

	public Order createManualOrder(ManualOrder orderRequest) {
		checkDeliveryTime(orderRequest.getEstimatedDeliveryTime());
		Business business = findByKey(orderRequest.getBusinessKey());
		Employee employee = employeeService.findByKey(orderRequest.getEmployeeKey());
		Order order = new OrderBuilder()
				.setCreatedEmp(employee)
				.setBusiness(business)
				.setNote(orderRequest.getMsgFromBusiness())
				.setAddress(orderRequest.getAddress())
				.setDeliverBy(orderRequest.getEstimatedDeliveryTime())
				.setEmail(orderRequest.getEmail())
				.setPhoneNumber(orderRequest.getContactNumber())
				.setOrderStatus(orderRequest.getOrderStatus())
				.setTags(orderRequest.getTags())
				.build();
		order = orderService.save(order);
		orderService.sendOrderNotification(order);
		return order;
	}

	public MobileAppConfig getMobileAppConfig(String employeeEmail) {
		final String lcEmail = employeeEmail.toLowerCase();
		Employee employee = employeeService.getRepo().findByEmail(lcEmail);
		if (employee == null) {
			throw new ClientException("Your account is inactivated");
		}
		Business business = employee.getBusiness();
		MobileAppConfig appConfig = new MobileAppConfig();
		appConfig.setEmployee(new EmployeeDto(employee));
		appConfig.setBusiness(new BusinessDto(business));
		appConfig.setMapboxKey(config.getMapboxAccessToken());
		appConfig.setUpdateLocationFrequency(config.getUpdateLocationFrequency());
		appConfig.setUpdateLocationDistance(config.getUpdateLocationDistance());
		appConfig.setMapboxStyle(config.getMapboxStyle());
		appConfig.setTripMaxNumbOrders(config.getTripMaxNumbOrders());
		appConfig.setMapboxSupportedCountries(config.getMapboxSupportedCountries());
		List<Tag> tags = tagService.getRepo().findEnabledByBusinessKey(business.getKey());
		appConfig.setTags(tags.stream().map(t -> new TagDto(t)).collect(Collectors.toList()));
		appConfig.setDefaultETA(config.getDefaultETA());
		appConfig.setUrlWeb(config.getUrlWebBase());
		appConfig.setUrlWebImportOrders(config.getUrlWebImportOrders());
		return appConfig;
	}

	public boolean validateLogin(String employeeEmail, String password) {
		final String lcEmail = employeeEmail.toLowerCase();
		Employee employee = employeeService.getRepo().findByEmail(lcEmail);
		if (employee == null) {
			throw new ClientException("Email does not exist");
		}
		Business business = employee.getBusiness();
		if (!business.isValidated()) {
			throw new ClientException("This business has not been validated yet. Validation email has been sent to the business owner");
		}
		PasswordAuthentication pwdAuth = new PasswordAuthentication();
		if (!pwdAuth.authenticate(password.toCharArray(), employee.getPassword())) {
			throw new ClientException("Password is incorrect");
		}
		return true;
	}

	private void checkDeliveryTime(ZonedDateTime time) {
		if (time == null || time.isBefore(DateTimeUtil.now())) {
			throw new ClientException("Please provide correct estimated delivery time. Provide value " + time);
		}
	}

	private Email generateValidateBusinessRegistrationEmail(Business business, Employee owner) {
		Email email = new Email();
		email.setSubject("Validate registration for: " + business.getName());
		email.setFrom(config.getSystemEmail());
		email.addTo(owner.getEmail());
		if (config.isMailBccToSystem()) {
			email.addBcc(config.getSystemEmail());
		}
		StringBuilder sb = new StringBuilder();
		sb.append("Hi " + owner.getFirstName() + ",<br/><br/>");
		sb.append("Thank you for registering for " + business.getName() + " with Mochi.<br/><br/>");
		sb.append("Please click " + notificationService.htmlLink(config.getUrlApiValidateBusiness() + "?key=" + business.getKey(), "here") + " to validate your registration.<br/><br/>");
		sb.append("If you weren't expecting this email, please click " + notificationService.htmlLink(config.getUrlApiRejectBusiness() + "?key=" + business.getKey(), "here") + " to reject this registration.<br/><br/>");
		sb.append("This will expire in " + config.getRegistrationExpiredInHour() + " hour(s)<br/><br/>");
		sb.append("Please save your business key: " + business.getKey() + "<br/><br/>");
		sb.append("Thank you,<br/>");
		sb.append("Mochi Team");
		email.setHtmlText(sb.toString());
		return email;
	}

	private String createTempPwd() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	public List<Tag> getTags(String businessKey) {
		return tagService.getRepo().findEnabledByBusinessKey(businessKey);
	}

	public Tag addTag(TagDto dto) {
		Employee requester = employeeService.findByKey(dto.getRequester().getUserKey());
		if (!requester.isAdmin()) {
			throw new ConcerningClientException("You don't have permission to add an employee");
		}
		Business business = findByKey(dto.getBusinessKey());
		Tag tag = new Tag();
		tag.setBusiness(business);
		tag.setName(dto.getName());
		return addTag(tag);
	}

	public Tag addTag(Tag tag) {
		return tagService.save(tag);
	}

	public Tag updateTag(TagDto dto) {
		Employee requester = employeeService.findByKey(dto.getRequester().getUserKey());
		if (!requester.isAdmin()) {
			throw new ConcerningClientException("You don't have permission to add an employee");
		}
		Tag tag = tagService.findByKey(dto.getKey());
		tagService.validateInsertOrUpdate(tag.getBusiness(), dto.getName());
		tag.setName(dto.getName());
		return tag;
	}

	public void resetEmployeePassword(String resetKey, String newPwd) {
		resetPasswordService.resetPassword(resetKey, newPwd);
	}

	public SubscriptionPlans getSubscriptionPlans(String businessKey) {
		return subscriptionService.getSubscriptionPlans(businessKey);
	}

	public void changeSubscriptionPlan(SubscriptionConfigDto dto) {
		subscriptionService.changeSubscriptionPlan(dto);
	}

	public ActiveOrdersSummary getActiveOrdersSummary(String businessKey) {
		ActiveOrdersSummary activeOrdersSummary = new ActiveOrdersSummary();
		List<TripDto> tripDtos = new ArrayList<>();
		Set<OrderDto> allActiveOrders = new HashSet<>();
		for (Trip trip : tripRepository.findActiveByBusinessKey(businessKey)) {
			TripDto tripDto = new TripDto(trip);
			Set<OrderDto> tripOrders = new HashSet<>();
			for (Order order : trip.getOrders()) {
				final EmployeeDto driver = new EmployeeDto(trip.getEmployee());
				OrderDto orderDto = new OrderDto(order, driver);
				orderDto.setTrackUrl(config.getUrlWebTrack() + "/" + order.getKey());
				tripOrders.add(orderDto);
			}
			allActiveOrders.addAll(tripOrders);
			tripDto.setOrders(tripOrders);
			tripDtos.add(tripDto);
		}
		allActiveOrders.addAll(getActiveOrders(businessKey));
		activeOrdersSummary.setTrips(tripDtos);
		activeOrdersSummary.setActiveOrders(allActiveOrders);
		return activeOrdersSummary;
	}

}
