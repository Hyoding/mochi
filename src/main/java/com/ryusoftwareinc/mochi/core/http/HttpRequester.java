package com.ryusoftwareinc.mochi.core.http;

public class HttpRequester {

	private String userKey;
	private String from; // mobile / web?

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

}
