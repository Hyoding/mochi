package com.ryusoftwareinc.mochi.business.model;

import com.ryusoftwareinc.mochi.employee.model.EmployeeDto;

import java.util.ArrayList;
import java.util.List;

public class MobileAppConfig {
	
	private BusinessDto business;
	private EmployeeDto employee;
	private String mapboxKey;
	private String mapboxStyle;
	private String mapboxSupportedCountries;
	private Integer updateLocationFrequency;
	private Double updateLocationDistance;
	private Integer tripMaxNumbOrders;
	private List<TagDto> tags;
	private Integer defaultETA;
	private String urlWebImportOrders;
	private String urlWeb;

	public MobileAppConfig() {
		tags = new ArrayList();
	}

	public BusinessDto getBusiness() {
		return business;
	}
	public void setBusiness(BusinessDto business) {
		this.business = business;
	}
	public EmployeeDto getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDto employee) {
		this.employee = employee;
	}
	public String getMapboxKey() {
		return mapboxKey;
	}
	public void setMapboxKey(String mapboxKey) {
		this.mapboxKey = mapboxKey;
	}
	public Integer getUpdateLocationFrequency() {
		return updateLocationFrequency;
	}
	public void setUpdateLocationFrequency(Integer updateLocationFrequency) {
		this.updateLocationFrequency = updateLocationFrequency;
	}

	public String getMapboxStyle() {
		return mapboxStyle;
	}

	public void setMapboxStyle(String mapboxStyle) {
		this.mapboxStyle = mapboxStyle;
	}

	public Integer getTripMaxNumbOrders() {
		return tripMaxNumbOrders;
	}

	public void setTripMaxNumbOrders(Integer tripMaxNumbOrders) {
		this.tripMaxNumbOrders = tripMaxNumbOrders;
	}

	public List<TagDto> getTags() {
		return tags;
	}

	public void setTags(List<TagDto> tags) {
		this.tags = tags;
	}

	public String getMapboxSupportedCountries() {
		return mapboxSupportedCountries;
	}

	public void setMapboxSupportedCountries(String mapboxSupportedCountries) {
		this.mapboxSupportedCountries = mapboxSupportedCountries;
	}

	public Integer getDefaultETA() {
		return defaultETA;
	}

	public void setDefaultETA(Integer defaultETA) {
		this.defaultETA = defaultETA;
	}

	public Double getUpdateLocationDistance() {
		return updateLocationDistance;
	}

	public void setUpdateLocationDistance(Double updateLocationDistance) {
		this.updateLocationDistance = updateLocationDistance;
	}

	public String getUrlWebImportOrders() {
		return urlWebImportOrders;
	}

	public void setUrlWebImportOrders(String urlWebImportOrders) {
		this.urlWebImportOrders = urlWebImportOrders;
	}

	public String getUrlWeb() {
		return urlWeb;
	}

	public void setUrlWeb(String urlWeb) {
		this.urlWeb = urlWeb;
	}
}
