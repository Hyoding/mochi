package com.ryusoftwareinc.mochi.core.exception;

public class BaseAppException extends RuntimeException {

	private static final long serialVersionUID = 6210185004086540140L;
	
	public BaseAppException(String msg) {
		super(msg);
	}

}
