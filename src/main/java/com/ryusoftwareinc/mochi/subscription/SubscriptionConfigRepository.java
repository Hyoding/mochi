package com.ryusoftwareinc.mochi.subscription;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SubscriptionConfigRepository extends CrudRepository<SubscriptionConfig, String> {

    List<SubscriptionConfig> findAllByOrderByPrice();

}
