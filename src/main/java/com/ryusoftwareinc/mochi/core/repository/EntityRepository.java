package com.ryusoftwareinc.mochi.core.repository;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.ryusoftwareinc.mochi.core.exception.EntityNotFoundException;
import com.ryusoftwareinc.mochi.core.model.AbstractEntity;

@NoRepositoryBean
public interface EntityRepository<T extends AbstractEntity> extends CrudRepository<T, Long> {
	
	T findByKey(String key);
	
	Iterable<T> findAllByKeyIn(Set<String> keys);
	
	default void validateKey(String key) {
		T entity = findByKey(key);
		if (entity == null) {
			throw new EntityNotFoundException("Invalid key");
		}
	}

	boolean existsByKey(String key);

}