package com.ryusoftwareinc.mochi.service;

import com.ryusoftwareinc.mochi.EntityFactory;
import com.ryusoftwareinc.mochi.api.ApiService;
import com.ryusoftwareinc.mochi.business.model.ApiOrder;
import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.business.model.ManualOrder;
import com.ryusoftwareinc.mochi.business.model.Order;
import com.ryusoftwareinc.mochi.business.model.Order.OrderStatus;
import com.ryusoftwareinc.mochi.business.repository.OrderRepository;
import com.ryusoftwareinc.mochi.business.service.BusinessService;
import com.ryusoftwareinc.mochi.core.exception.ClientException;
import com.ryusoftwareinc.mochi.core.exception.ConcerningClientException;
import com.ryusoftwareinc.mochi.core.http.HttpRequester;
import com.ryusoftwareinc.mochi.core.model.Address;
import com.ryusoftwareinc.mochi.core.util.DateTimeUtil;
import com.ryusoftwareinc.mochi.core.validator.ValidationException;
import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.employee.model.EmployeeDto;
import com.ryusoftwareinc.mochi.employee.model.EmployeeType;
import com.ryusoftwareinc.mochi.employee.repository.EmployeeRepository;
import com.ryusoftwareinc.mochi.subscription.SubscriptionConfig;
import com.ryusoftwareinc.mochi.subscription.SubscriptionConfigRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.time.ZonedDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class BusinessServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	BusinessService service;
	@Autowired
	EmployeeRepository empRepo;
	@Autowired
	OrderRepository orderRepo;
	@Autowired
	EntityFactory entityFactory;
	@Autowired
	ApiService apiService;
	@Autowired
	SubscriptionConfigRepository subscriptionConfigRepository;

	@Test
	public void registerShouldFailIfNameNotProvided() {
		Business business = new Business();
		try {
			service.save(business);
			fail("Shouldn't reach here");
		} catch (Exception ex) {
		}

	}

	@Test
	public void registerShouldFailIfNameAlreadyTaken() {
		SubscriptionConfig subscriptionConfig = new SubscriptionConfig();
		subscriptionConfig.setName(System.currentTimeMillis() + "");
		subscriptionConfig = subscriptionConfigRepository.save(subscriptionConfig);

		Business business = entityFactory.generateBusiness();
		business.setSubscription(subscriptionConfig);
		service.save(business);

		Business business2 = entityFactory.generateBusiness();
		business2.setName(business.getName());
		try {
			service.save(business2);
			fail("Shouldn't reach here");
		} catch (Exception ex) {
		}

	}

	@Test
	public void registerShouldWorkWhenNormal() {
		SubscriptionConfig subscriptionConfig = new SubscriptionConfig();
		subscriptionConfig.setName(System.currentTimeMillis() + "");
		subscriptionConfig = subscriptionConfigRepository.save(subscriptionConfig);

		Business business = entityFactory.generateBusiness();
		business.setSubscription(subscriptionConfig);
		Employee employee = new Employee();
		employee.setFirstName("Joseph");
		employee.setLastName("Ryu");
		employee.setEmail("g");
		employee.setPhoneNumber("efefef");
		employee.setPassword("12122");

		business = service.register(business, employee);
		employee = empRepo.findByKey(employee.getKey());
		assertNotNull(employee);
		assertTrue(employee.getType() == EmployeeType.OWNER);
	}


	@Test
	public void addEmployeeShouldWorkWhenNormal() {
		Business business = entityFactory.generateBusiness();
		Employee owner = new Employee();
		owner.setFirstName("Joseph");
		owner.setLastName("Ryu");
		owner.setEmail("g");
		owner.setPhoneNumber("efefef");
		owner.setPassword("12122");
		business = service.register(business, owner);
		business.getSubscription().setMaxEmployees(10);

		Employee newEmployee = new Employee();
		newEmployee.setFirstName("Joseph");
		newEmployee.setLastName("Ryu");
		newEmployee.setEmail("g2");
		newEmployee.setPhoneNumber("efefef");
		newEmployee.setPassword("12122");
		newEmployee.setType(EmployeeType.OPERATOR);

		newEmployee = service.addEmployee(newEmployee, owner.getKey());
		assertTrue(business.getEmployees().size() == 2);
		assertNotNull(newEmployee.getId());
		assertTrue(newEmployee.getType() == EmployeeType.OPERATOR);

		Employee newManager = new Employee();
		newManager.setFirstName("Joseph");
		newManager.setLastName("Ryu");
		newManager.setEmail("g3");
		newManager.setPhoneNumber("efefef");
		newManager.setPassword("12122");
		newManager.setType(EmployeeType.MANAGER);
		newManager = service.addEmployee(newManager, owner.getKey());
		assertTrue(business.getEmployees().size() == 3);

		Employee newDriver = new Employee();
		newDriver.setFirstName("Joseph");
		newDriver.setLastName("Ryu");
		newDriver.setEmail("g4");
		newDriver.setPhoneNumber("efefef");
		newDriver.setPassword("12122");
		newDriver.setType(EmployeeType.DRIVER);
		service.addEmployee(newDriver, newManager.getKey());
		assertTrue(business.getEmployees().size() == 4);
	}

	@Test
	public void addEmployeeShouldFailWhenNoPermission() {
		Business business = entityFactory.generateBusiness();
		Employee owner = new Employee();
		owner.setFirstName("Joseph");
		owner.setLastName("Ryu");
		owner.setEmail("g");
		owner.setPhoneNumber("efefef");
		owner.setPassword("12122");
		service.register(business, owner);
		business.getSubscription().setMaxEmployees(10);

		Employee newOperator = new Employee();
		newOperator.setFirstName("Joseph");
		newOperator.setLastName("Ryu");
		newOperator.setEmail("g2");
		newOperator.setPhoneNumber("efefef");
		newOperator.setPassword("12122");
		newOperator.setType(EmployeeType.OPERATOR);
		newOperator = service.addEmployee(newOperator, owner.getKey());

		Employee newDriver = new Employee();
		newDriver.setFirstName("Joseph");
		newDriver.setLastName("Ryu");
		newDriver.setEmail("g4");
		newDriver.setPhoneNumber("efefef");
		newDriver.setPassword("12122");
		newDriver.setType(EmployeeType.DRIVER);
		try {
			service.addEmployee(newDriver, newOperator.getKey());
			fail();
		} catch (ConcerningClientException ex) {

		}
	}

	@Test
	public void removeEmployee() {
		Business business = entityFactory.generateBusiness();
		Employee owner = new Employee();
		owner.setFirstName("Joseph");
		owner.setLastName("Ryu");
		owner.setEmail("g");
		owner.setPhoneNumber("efefef");
		owner.setPassword("12122");
		service.register(business, owner);
		business.getSubscription().setMaxEmployees(10);

		Employee employee = new Employee();
		employee.setFirstName("Joseph");
		employee.setLastName("Ryu");
		employee.setEmail("g2");
		employee.setPhoneNumber("efefef");
		employee.setPassword("12122");

		employee = service.addEmployee(employee, owner.getKey());
		service.removeEmployee(employee.getKey(), owner.getKey());
	}

	@Test
	public void updateEmployee() {
		Business business = entityFactory.generateBusiness();
		Employee owner = new Employee();
		owner.setFirstName("Joseph");
		owner.setLastName("Ryu");
		owner.setEmail("g");
		owner.setPhoneNumber("efefef");
		owner.setPassword("12122");
		service.register(business, owner);
		business.getSubscription().setMaxEmployees(10);

		Employee employee = new Employee();
		employee.setFirstName("Joseph");
		employee.setLastName("Ryu");
		employee.setEmail("g2");
		employee.setPhoneNumber("efefef");
		employee.setPassword("12122");

		employee = service.addEmployee(employee, owner.getKey());

		final String changedName = "changedName";

		EmployeeDto updated = new EmployeeDto(employee);
		HttpRequester requester = new HttpRequester();
		requester.setUserKey(owner.getKey());
		updated.setRequester(requester);
		updated.setFirstName(changedName);

		service.updateEmployee(updated);
		for (Employee emp : business.getEmployees()) {
			if (emp.equals(employee)) {
				assertTrue(emp.getFirstName().equals(changedName));
				assertEquals(employee, emp);
			}
		}
	}

	@Test
	public void createManualOrderShouldWorkWhenNormal() {
		SubscriptionConfig subscriptionConfig = new SubscriptionConfig();
		subscriptionConfig.setName(System.currentTimeMillis() + "");
		subscriptionConfig.setIncOrders(10);
		subscriptionConfig = subscriptionConfigRepository.save(subscriptionConfig);

		Business business = entityFactory.generateBusiness();
		business.setSubscription(subscriptionConfig);
		business = service.save(business);

		Employee employee = entityFactory.generateEmployee();
		employee = empRepo.save(employee);

		final String email = "joseph.ryu1@gmail.com";
		final String msgFromBusiness = "2 hotdog, 1 chilidog, 1 hamburger";
		final Address deliveryAddress = entityFactory.generateAddress();
		final String contactNumber = "2897073022";
		final ZonedDateTime edt = DateTimeUtil.now().plusHours(2);

		ManualOrder orderRequest = new ManualOrder();
		orderRequest.setBusinessKey(business.getKey());
		orderRequest.setEmployeeKey(employee.getKey());
		orderRequest.setMsgFromBusiness(msgFromBusiness);
		orderRequest.setContactNumber(contactNumber);
		orderRequest.setEmail(email);
		orderRequest.setAddress(deliveryAddress);
		orderRequest.setEstimatedDeliveryTime(edt);

		Order order = service.createManualOrder(orderRequest);
		assertEquals(order.getEmail(), email);
		assertEquals(order.getNote(), msgFromBusiness);
		assertEquals(order.getAddress(), deliveryAddress);
		assertEquals(order.getPhoneNumber(), contactNumber);
		assertEquals(order.getDeliverBy(), edt);
	}

	@Test
	public void createSystemGeneratedOrderShouldCreateWhenNormal() {
		SubscriptionConfig subscriptionConfig = new SubscriptionConfig();
		subscriptionConfig.setName(System.currentTimeMillis() + "");
		subscriptionConfig.setIncOrders(10);
		subscriptionConfig.setUseApi(true);
		subscriptionConfig = subscriptionConfigRepository.save(subscriptionConfig);

		Address address = entityFactory.generateAddress();
		Business business = entityFactory.generateBusiness();
		business.setSubscription(subscriptionConfig);
		business = service.save(business);

		final String testMsg = "test msg";
		final String testPhone = "2897073022";
		final String testEmail = "joseph.ryu1@gmail.com";
		final String testStreet = "5481 Clyde Street";
		final ZonedDateTime testDate = DateTimeUtil.now().plusHours(1);

		ApiOrder or = new ApiOrder();
		or.setKey(business.getKey());
		or.setNote(testMsg);
		or.setPhoneNumber(testPhone);
		or.setEmail(testEmail);
		or.setAddress(address);
		or.setDeliverBy(testDate);
		ApiOrder order = apiService.saveApiOrder(or);
		assertNotNull(order.getOrderKey());

		assertEquals(OrderStatus.ACCEPTED, order.getStatus());
		assertEquals(testDate, order.getDeliverBy());
		assertEquals(testDate, order.getDeliverBy());
		assertEquals(testStreet, order.getAddress().getStreetAddress());
		assertEquals(testEmail, order.getEmail());
		assertEquals(testMsg, order.getNote());
	}

	@Test
	public void createSystemGeneratedOrderShouldFailWhenMissingRequired() {
		SubscriptionConfig subscriptionConfig = new SubscriptionConfig();
		subscriptionConfig.setName(System.currentTimeMillis() + "");
		subscriptionConfig.setUseApi(true);
		subscriptionConfig = subscriptionConfigRepository.save(subscriptionConfig);

		Address address = entityFactory.generateAddress();
		Business business = entityFactory.generateBusiness();
		business.setSubscription(subscriptionConfig);
		business = service.save(business);

		final String testMsg = "test msg";
		final String testPhone = "2897073022";
		final String testEmail = "joseph.ryu1@gmail.com";
		final String testStreet = "5481 Clyde Street";
		final ZonedDateTime testDate = DateTimeUtil.now().plusHours(1);

		ApiOrder or = new ApiOrder();
//		or.setBusinessKey(business.getKey());
		or.setNote(testMsg);
		or.setPhoneNumber(testPhone);
		or.setEmail(testEmail);
		or.setAddress(address);
		or.setDeliverBy(testDate);
		try {
			or = apiService.saveApiOrder(or);
			fail();
		} catch (Exception ex) {}

		or = new ApiOrder();
		or.setKey(business.getKey());
		or.setNote(testMsg);
//		or.setContactNumber(testPhone);
//		or.setEmail(testEmail);
		or.setAddress(address);
		or.setDeliverBy(testDate);
		try {
			or = apiService.saveApiOrder(or);
			fail();
		} catch (ValidationException ex) {}

		or = new ApiOrder();
		or.setKey(business.getKey());
		or.setNote(testMsg);
		or.setPhoneNumber(testPhone);
		or.setEmail(testEmail);
//		or.setAddress(address);
		or.setDeliverBy(testDate);
		try {
			or = apiService.saveApiOrder(or);
			fail();
		} catch (ClientException ex) {}

		or = new ApiOrder();
		or.setKey(business.getKey());
		or.setNote(testMsg);
		or.setPhoneNumber(testPhone);
		or.setEmail(testEmail);
		or.setAddress(address);
//		or.setEdt(testDate);
		try {
			or = apiService.saveApiOrder(or);
			fail();
		} catch (ValidationException ex) {}

	}

	@Test
	public void findOrdersToDeliverAndFindOrderHistoryShouldRetrieveCorrectOnes() {
		SubscriptionConfig subscriptionConfig = new SubscriptionConfig();
		subscriptionConfig.setName(System.currentTimeMillis() + "");
		subscriptionConfig = subscriptionConfigRepository.save(subscriptionConfig);

		Order CANCELED = entityFactory.generateOrder();
		CANCELED.setStatus(OrderStatus.CANCELED);
		CANCELED.setCanceledTime(DateTimeUtil.now());
		Business business = CANCELED.getBusiness();
		business.setSubscription(subscriptionConfig);
		business = service.save(business);
		orderRepo.save(CANCELED);

		Order REJECTED = entityFactory.generateOrder();
		REJECTED.setBusiness(business);
		REJECTED.setCanceledTime(DateTimeUtil.now());
		REJECTED.setStatus(OrderStatus.REJECTED);
		orderRepo.save(REJECTED);

		Order ACCEPTED = entityFactory.generateOrder();
		ACCEPTED.setBusiness(business);
		ACCEPTED.setStatus(OrderStatus.ACCEPTED);
		orderRepo.save(ACCEPTED);

		Order OUT_FOR_DELIVERY = entityFactory.generateOrder();
		OUT_FOR_DELIVERY.setBusiness(business);
		OUT_FOR_DELIVERY.setStatus(OrderStatus.OUT_FOR_DELIVERY);
		orderRepo.save(OUT_FOR_DELIVERY);

		Order DELIVERED = entityFactory.generateOrder();
		DELIVERED.setBusiness(business);
		DELIVERED.setStatus(OrderStatus.DELIVERED);
		DELIVERED.setDeliveredTime(DateTimeUtil.now());
		orderRepo.save(DELIVERED);

		List<Order> ordersToDeliver = service.getOrdersToDeliver(business.getKey());
		assertFalse(ordersToDeliver.isEmpty());
		for (Order order : ordersToDeliver) {
			assertTrue(order.getStatus() == OrderStatus.ACCEPTED);
		}

		List<Order> orderHistory = service.getOrderHistory(business.getKey());
		assertEquals(orderHistory.size(), OrderStatus.values().length);
	}



}
