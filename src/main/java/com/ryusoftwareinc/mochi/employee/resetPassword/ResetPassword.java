package com.ryusoftwareinc.mochi.employee.resetPassword;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.core.model.AbstractEntity;

@Entity
public class ResetPassword extends AbstractEntity {

	@OneToOne
	private Employee employee;
	private boolean isReset;

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public boolean isReset() {
		return isReset;
	}
	public void setReset(boolean isReset) {
		this.isReset = isReset;
	}
	
}
