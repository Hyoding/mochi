package com.ryusoftwareinc.mochi.business.repository;

import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.core.repository.EntityRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BusinessRepository extends EntityRepository<Business> {
	
	boolean existsByName(String name);

	@Query(value = "select * from business where id = (select business_id from employee where email = ?)", nativeQuery = true)
	Business findByEmployeeEmail(String email);

	@Query(value = "select * from business where id = (select business_id from employee where key = ?)", nativeQuery = true)
	Business findByEmployeeKey(String employeeKey);

	List<Business> findByEnabledTrue();

	List<Business> findByEnabledFalse();

	List<Business> findByIsValidatedFalseAndEnabledTrue();

	@Query(value = "select exists(select 1 from business where key = ? and enabled is true)", nativeQuery = true)
	boolean isBusinessEnabled(String businessKey);

	@Query(value = "select * from business where id = (select id from orders where key = ?)", nativeQuery = true)
	Business findByOrderKey(String key);

	@Query(value = "select * from business where enabled is true and id in (select business_id from invoice where payment_id is null)", nativeQuery = true)
	List<Business> findByActiveAndUnpaidInvoice();
	
}
