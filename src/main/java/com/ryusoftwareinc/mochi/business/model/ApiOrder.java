package com.ryusoftwareinc.mochi.business.model;

import com.ryusoftwareinc.mochi.core.model.Address;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.ZonedDateTime;
import java.util.Set;

public class ApiOrder {

	// required
	private String key;
	private String orderKey;
	private Address address;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private ZonedDateTime deliverBy;
	private String phoneNumber; // one of this is required
	private String email;

	// optional
	private String note;
	private Set<String> tags;

	private Order.OrderStatus status;

	public ApiOrder() {}

	public ApiOrder(Order order, Set<String> tags) {
		this.orderKey = order.getKey();
		this.address = order.getAddress();
		this.deliverBy = order.getDeliverBy();
		this.phoneNumber = order.getPhoneNumber();
		this.email = order.getEmail();
		this.note = order.getNote();
		this.tags = tags;
		this.status = order.getStatus();
	}

	public String getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public ZonedDateTime getDeliverBy() {
		return deliverBy;
	}

	public void setDeliverBy(ZonedDateTime deliverBy) {
		this.deliverBy = deliverBy;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	public Order.OrderStatus getStatus() {
		return status;
	}

	public void setStatus(Order.OrderStatus status) {
		this.status = status;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}