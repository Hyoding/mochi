package com.ryusoftwareinc.mochi.api;

import com.ryusoftwareinc.mochi.business.model.*;
import com.ryusoftwareinc.mochi.business.service.BusinessService;
import com.ryusoftwareinc.mochi.business.service.OrderService;
import com.ryusoftwareinc.mochi.business.service.TagService;
import com.ryusoftwareinc.mochi.core.exception.ClientException;
import com.ryusoftwareinc.mochi.core.exception.EntityKeyNotProvidedException;
import com.ryusoftwareinc.mochi.core.exception.ServerException;
import com.ryusoftwareinc.mochi.core.model.Address;
import com.ryusoftwareinc.mochi.core.util.DateTimeUtil;
import com.ryusoftwareinc.mochi.location.mapbox.LocationService;
import com.ryusoftwareinc.mochi.subscription.SubscriptionService;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.NotOfficeXmlFileException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class ApiService {

    @Autowired
    BusinessService businessService;

    @Autowired
    OrderService orderService;

    @Autowired
    TagService tagService;

    @Autowired
    LocationService locationService;

    @Autowired
    SubscriptionService subscriptionService;

    public ApiOrder saveApiOrder(ApiOrder orderRequest) {
        subscriptionService.validateApiConfiguration(orderRequest.getKey());
        Order order = orderService.convertToOrder(orderRequest);
        order = orderService.save(order);
        orderService.sendOrderNotification(order);
        ApiOrder apiOrder = new ApiOrder(order, orderRequest.getTags());
        return apiOrder;
    }

    public void importOrders(List<ApiOrder> apiOrders) {
        subscriptionService.validateImportConfiguration(apiOrders.get(0).getKey());
        for (ApiOrder apiOrder : apiOrders) {
            Order order = orderService.convertToOrder(apiOrder);
            orderService.save(order);
            orderService.sendOrderNotification(order);
        }
    }

    public ApiOrder updateApiOrder(ApiOrder updateRequest) {
        subscriptionService.validateApiConfiguration(updateRequest.getKey());
        Business business = businessService.findByKey(updateRequest.getKey());
        Set<String> tagKeys = tagService.convertTagNamesToTagKeys(business.getKey(), updateRequest.getTags());
        OrderDto dto = new OrderDto(updateRequest, tagKeys);
        Order updatedOrder = orderService.updateOrder(dto);
        ApiOrder updatedApiOrder = new ApiOrder(updatedOrder, updateRequest.getTags());
        return updatedApiOrder;
    }

    public void cancelOrder(String orderKey) {
        if (StringUtils.isBlank(orderKey)) {
            throw new EntityKeyNotProvidedException("Order key needs to be provided");
        }
        Order order = orderService.findByKey(orderKey);
        subscriptionService.validateApiConfiguration(order.getBusiness().getKey());
        order.setStatus(Order.OrderStatus.CANCELED);
        order.setCanceledTime(DateTimeUtil.now());
    }

    private ApiOrder convertRowToOrder(XSSFRow row, String businessKey, String timeZone) {
        DataFormatter formatter = new DataFormatter();
        int cellIdx = 0;

        String phoneNumber = formatter.formatCellValue(row.getCell(cellIdx++));
        phoneNumber = StringUtils.isNotEmpty(phoneNumber) ? phoneNumber.trim() : phoneNumber;

        String email = formatter.formatCellValue(row.getCell(cellIdx++));
        email = StringUtils.isNotEmpty(email) ? email.trim() : email;

        LocalDateTime deliverByLDT = row.getCell(cellIdx++).getLocalDateTimeCellValue();

        String streetAddress = formatter.formatCellValue(row.getCell(cellIdx++));
        streetAddress = StringUtils.isNotEmpty(streetAddress) ? streetAddress.trim() : streetAddress;

        final String unit = formatter.formatCellValue(row.getCell(cellIdx++));

        String city = formatter.formatCellValue(row.getCell(cellIdx++));
        city = StringUtils.isNotEmpty(city) ? city.trim() : city;

        String province = formatter.formatCellValue(row.getCell(cellIdx++));
        province = StringUtils.isNotEmpty(province) ? province.trim() : province;

        String country = formatter.formatCellValue(row.getCell(cellIdx++));
        country = StringUtils.isNotEmpty(country) ? country.trim() : country;

        String postalCode = formatter.formatCellValue(row.getCell(cellIdx++));
        postalCode = StringUtils.isNotEmpty(postalCode) ? postalCode.trim() : postalCode;

        final String note = formatter.formatCellValue(row.getCell(cellIdx++));

        String tag1 = formatter.formatCellValue(row.getCell(cellIdx++));
        tag1 = StringUtils.isNotEmpty(tag1) ? tag1.trim() : tag1;

        String tag2 = formatter.formatCellValue(row.getCell(cellIdx++));
        tag2 = StringUtils.isNotEmpty(tag2) ? tag2.trim() : tag2;

        String tag3 = formatter.formatCellValue(row.getCell(cellIdx++));
        tag3 = StringUtils.isNotEmpty(tag3) ? tag3.trim() : tag3;

        ZoneId zoneId = ZoneId.of(timeZone);
        ZonedDateTime deliveryBy = deliverByLDT.atZone(zoneId);
        Address address = new Address();
        address.setStreetAddress(streetAddress);
        address.setUnit(unit);
        address.setCity(city);
        address.setProvince(province);
        address.setCountry(country);
        address.setPostalCode(postalCode);
        locationService.validateAndFillAddress(address);

        Set<String> tagNames = new HashSet<>();
        if (!tag1.isBlank()) {
            tagNames.add(tag1);
        }
        if (!tag2.isBlank()) {
            tagNames.add(tag2);
        }
        if (!tag3.isBlank()) {
            tagNames.add(tag3);
        }
        ApiOrder order = new ApiOrder();
        order.setKey(businessKey);
        order.setAddress(address);
        order.setDeliverBy(deliveryBy);
        order.setPhoneNumber(phoneNumber);
        order.setEmail(email);
        order.setNote(note);
        order.setTags(tagNames);
        return order;
    }

    public List<ApiOrder> convertToOrders(LoadOrdersDto dto) {
        final int startIdx = 5;
        List<ApiOrder> orders = new ArrayList<>();
        for (MultipartFile file : dto.getFiles()) {
            try (XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream())) {
                XSSFSheet worksheet = workbook.getSheetAt(0);
                for (int i = startIdx; i < worksheet.getPhysicalNumberOfRows() ; i++) {
                    XSSFRow row = worksheet.getRow(i);
                    ApiOrder order = convertRowToOrder(row, dto.getBusinessKey(), dto.getTimeZone());
                    orders.add(order);
                }
            } catch (IOException e) {
                throw new ServerException("Error while reading file");
            } catch (NotOfficeXmlFileException e) {
                throw new ClientException("Not an excel file");
            }
        }
        return orders;
    }

}
