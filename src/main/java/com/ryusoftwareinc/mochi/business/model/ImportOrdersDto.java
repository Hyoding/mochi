package com.ryusoftwareinc.mochi.business.model;

import com.ryusoftwareinc.mochi.core.http.BaseHttpRequest;

import java.util.List;

public class ImportOrdersDto extends BaseHttpRequest {

	private List<ApiOrder> orders;

	public List<ApiOrder> getOrders() {
		return orders;
	}

	public void setOrders(List<ApiOrder> orders) {
		this.orders = orders;
	}

}
