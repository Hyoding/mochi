package com.ryusoftwareinc.mochi.billing.model;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class BillableItem {

    @Enumerated(EnumType.STRING)
    private Billable billable;

    /*
        Number, days, etc
     */
    private int count;

    private double price;

    public Billable getBillable() {
        return billable;
    }

    public void setBillable(Billable billable) {
        this.billable = billable;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public enum Billable {
        SUBSCRIPTION("Subscription"),
        ADDL_ORDERS("Additional orders"),
        SMS("SMS")
        ;

        private String label;

        private Billable(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }

    }

}
