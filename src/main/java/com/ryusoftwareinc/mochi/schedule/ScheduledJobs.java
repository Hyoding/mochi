package com.ryusoftwareinc.mochi.schedule;

import com.ryusoftwareinc.mochi.billing.model.Invoice;
import com.ryusoftwareinc.mochi.billing.service.InvoiceService;
import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.business.service.BusinessService;
import com.ryusoftwareinc.mochi.core.config.Config;
import com.ryusoftwareinc.mochi.core.util.DateTimeUtil;
import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.employee.service.EmployeeService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.YearMonth;
import java.time.ZonedDateTime;
import java.util.List;

@Service
@Transactional
public class ScheduledJobs {

    private static final Logger log = Logger.getLogger(ScheduledJobs.class);

    private static final int ONE_HR =  1000 * 60 * 60;

    @Autowired
    BusinessService businessService;

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    Config config;

    @Scheduled(fixedDelay = ONE_HR)
    public void cleanUpBusinessRegistration() {
        int counter = 0;
        for (Business business : businessService.getRepo().findByIsValidatedFalseAndEnabledTrue()) {
            ZonedDateTime deleteAfterThisTime = business.getCreatedDate().plusHours(config.getRegistrationExpiredInHour());
            if (DateTimeUtil.now().isAfter(deleteAfterThisTime)) {
                businessService.rejectRegistration(business);
                counter++;
            }
        }
        log.info("Cleaned up not validated business registration: " + counter);
    }

    @Scheduled(fixedDelay = ONE_HR)
    public void cleanUpEmployees() {
        int counter = 0;
        for (Employee emp: employeeService.getRepo().findByNeverResetPwd()) {
            ZonedDateTime deleteAfterThisTime = emp.getCreatedDate().plusHours(config.getRegistrationExpiredInHour());
            if (DateTimeUtil.now().isAfter(deleteAfterThisTime)) {
                employeeService.delete(emp);
                counter++;
            }
        }
        log.info("Cleaned up expired employees: " + counter);
    }

    /*
        This is to run 1st day of month at 6AM
     */
    @Scheduled(cron = "0 0 6 1 * *")
    public int generateInvoiceForPreviousMonth() {
        log.info("Generating invoices for previous month");
        YearMonth previousMonth = YearMonth.now().minusMonths(1);
        List<Business> businessList = businessService.getRepo().findByEnabledTrue();
        int count = 0;
        for (Business business : businessList) {
            Invoice invoice = invoiceService.generateInvoice(business, previousMonth);
            if (invoice != null) {
                count++;
            }
            // TODO: send an email to businesses?
            log.info("invoice generated for " + business.getName());
        }
        log.info("Generated invoices count: " + count);
        return count;
    }

    /*
        Run 1 day after due date every month at 6AM
     */
    @Scheduled(cron = "0 0 6 " + (InvoiceService.DUE_DAY_OF_MONTH + 1) +  " * *")
    public int disableBusinessesThatDidNotPayBill() {
        log.info("Checking businesses that did not pay");
        ZonedDateTime now = DateTimeUtil.now();
        // get all invoice that is not paid and their business is enabled
        List<Business> businessList = businessService.getRepo().findByActiveAndUnpaidInvoice();
        for (Business business : businessList) {
            business.setEnabled(false);
            // TODO: send an email to business
            log.info("Disabling business for unpaid invoice: " + business.getName());
        }
        log.info("Disabled business count: " + businessList.size());
        return businessList.size();
    }

}
