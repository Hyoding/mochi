package com.ryusoftwareinc.mochi.report;

import com.ryusoftwareinc.mochi.core.repository.EntityRepository;

public interface ReportRequestRepository extends EntityRepository<ReportRequest> {
}
