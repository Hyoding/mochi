package com.ryusoftwareinc.mochi.core.model;

import javax.persistence.Embeddable;

@Embeddable
public class Coordinate {

	private Double latitude;
	private Double longitude;
	
	public Coordinate() {}
	
	public Coordinate(Double lat, Double lng) {
		this.latitude = lat;
		this.longitude = lng;
	}
	
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return latitude + ", " + longitude;
	}
	
}
