package com.ryusoftwareinc.mochi.employee.feedback;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ryusoftwareinc.mochi.core.controller.AbstractBaseController;
import com.ryusoftwareinc.mochi.core.http.HttpResponse;

@RequestMapping("/api/feedback/v1")
@RestController
@CrossOrigin
public class FeedbackController extends AbstractBaseController {

	private static final Logger log = Logger.getLogger(FeedbackController.class);

	@Autowired FeedbackService service;

	@PostMapping("employeeSubmit")
	public HttpResponse employeeSubmit(@RequestBody FeedbackDto dto) {
		log.info("employeeSubmit " + dto.getRequester().getUserKey());
		service.submit(dto);
		return formResponse(null, "Feedback submitted");
	}

}
