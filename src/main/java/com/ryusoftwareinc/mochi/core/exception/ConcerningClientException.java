package com.ryusoftwareinc.mochi.core.exception;

/*
    Use this if we need to detect problem with user facing code.
    Typically if we implemented right we shouldn't see this exception
 */
public class ConcerningClientException extends ClientException implements Concerning {

    public ConcerningClientException(String msg) {
        super(msg);
    }

}
