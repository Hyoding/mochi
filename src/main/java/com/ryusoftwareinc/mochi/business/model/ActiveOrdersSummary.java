package com.ryusoftwareinc.mochi.business.model;

import java.util.List;
import java.util.Set;

public class ActiveOrdersSummary {

    private List<TripDto> trips;
    private Set<OrderDto> activeOrders;

    public List<TripDto> getTrips() {
        return trips;
    }

    public void setTrips(List<TripDto> trips) {
        this.trips = trips;
    }

    public Set<OrderDto> getActiveOrders() {
        return activeOrders;
    }

    public void setActiveOrders(Set<OrderDto> activeOrders) {
        this.activeOrders = activeOrders;
    }

}
