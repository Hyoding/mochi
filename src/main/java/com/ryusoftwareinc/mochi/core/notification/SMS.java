package com.ryusoftwareinc.mochi.core.notification;

import java.util.stream.Collectors;

public class SMS {

    private String from;
    private String to;
    private String body;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append("From: " + from);
        sb.append("\n");
        sb.append("To: " + String.join(", ", to));
        sb.append("\n\n");
        sb.append(body);
        return sb.toString();
    }

}
