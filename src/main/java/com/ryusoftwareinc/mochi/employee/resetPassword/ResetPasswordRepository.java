package com.ryusoftwareinc.mochi.employee.resetPassword;

import com.ryusoftwareinc.mochi.core.repository.EntityRepository;

public interface ResetPasswordRepository extends EntityRepository<ResetPassword> {

}
