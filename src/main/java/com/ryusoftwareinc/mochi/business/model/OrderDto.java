package com.ryusoftwareinc.mochi.business.model;

import com.ryusoftwareinc.mochi.core.http.BaseHttpRequest;
import com.ryusoftwareinc.mochi.core.model.Address;
import com.ryusoftwareinc.mochi.employee.model.EmployeeDto;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Set;

public class OrderDto extends BaseHttpRequest {
	
	private String key;
	private String businessKey;
	private String status;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private ZonedDateTime deliverBy;
	private Address address;
	private String email;
	private String phoneNumber;
	private String note;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private ZonedDateTime createdDate;
	private Set<String> tagsKey;
	private EmployeeDto driver;
	private String trackUrl;

	public OrderDto() {}
	
	public OrderDto(Order order) {
		this.createdDate = order.getCreatedDate();
		this.key = order.getKey();
		this.status = order.getStatus() == null ? null : order.getStatus().getLabel();
		this.deliverBy = order.getDeliverBy();
		this.address = order.getAddress();
		this.email = order.getEmail();
		this.phoneNumber = order.getPhoneNumber();
		this.note = order.getNote();
		this.tagsKey = order.getTags();
	}

	public OrderDto(ApiOrder order, Set<String> tagsKey) {
		this.key = order.getOrderKey();
		this.address = order.getAddress();
		this.deliverBy = order.getDeliverBy();
		this.phoneNumber = order.getPhoneNumber();
		this.email = order.getEmail();
		this.note = order.getNote();
		this.tagsKey = tagsKey;
	}

	public OrderDto(Order order, EmployeeDto driver) {
		this(order);
		this.driver = driver;
	}

	public String getBusinessKey() {
		return businessKey;
	}
	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ZonedDateTime getDeliverBy() {
		return deliverBy;
	}
	public void setDeliverBy(ZonedDateTime deliverBy) {
		this.deliverBy = deliverBy;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}

	public ZonedDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(ZonedDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public Set<String> getTagsKey() {
		return tagsKey;
	}

	public void setTagsKey(Set<String> tagsKey) {
		this.tagsKey = tagsKey;
	}

	public EmployeeDto getDriver() {
		return driver;
	}

	public void setDriver(EmployeeDto driver) {
		this.driver = driver;
	}

	public String getTrackUrl() {
		return trackUrl;
	}

	public void setTrackUrl(String trackUrl) {
		this.trackUrl = trackUrl;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		OrderDto orderDto = (OrderDto) o;
		return Objects.equals(key, orderDto.key);
	}

	@Override
	public int hashCode() {
		return Objects.hash(key);
	}

}
