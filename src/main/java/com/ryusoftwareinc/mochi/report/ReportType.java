package com.ryusoftwareinc.mochi.report;

public enum ReportType {
    DETAILED_ORDER("Detailed Orders Report"),
    TRIPS("Trips Report"),
    ;

    private String label;

    private ReportType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

}
