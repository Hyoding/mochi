package com.ryusoftwareinc.mochi.employee.feedback;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.core.model.AbstractEntity;

@Entity
public class Feedback extends AbstractEntity {
	
	@OneToOne
	private Employee employee;
	private String message;
	
	public Feedback() {}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
