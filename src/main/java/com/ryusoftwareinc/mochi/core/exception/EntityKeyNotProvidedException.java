package com.ryusoftwareinc.mochi.core.exception;

public class EntityKeyNotProvidedException extends ClientException implements Concerning {

	private static final long serialVersionUID = 6210185004086540140L;
	
	public EntityKeyNotProvidedException(String msg) {
		super(msg);
	}

}
