package com.ryusoftwareinc.mochi.location.routexl;

public class OptimizedRouteRouteXLException extends RuntimeException {

    public OptimizedRouteRouteXLException() {}

    public OptimizedRouteRouteXLException(String msg) {
        super(msg);
    }

}
