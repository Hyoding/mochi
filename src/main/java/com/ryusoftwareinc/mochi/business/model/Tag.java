package com.ryusoftwareinc.mochi.business.model;

import com.ryusoftwareinc.mochi.core.model.AbstractEntity;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Tag extends AbstractEntity {

    @JsonIgnore
    @OneToOne
    private Business business;

    /*
        This should be unique per business
     */
    private String name;

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
