package com.ryusoftwareinc.mochi.core.util;

import java.time.Instant;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

public class DateTimeUtil {
	
	public static final String DEFAULT_ZONE = "America/Halifax";
	public static final ZoneId ZONE_ID = ZoneId.of(DEFAULT_ZONE);

	static {
		TimeZone.setDefault(TimeZone.getTimeZone(DEFAULT_ZONE));
	}
	
	private static final long ALMOST_EQUAL_CRITERIA_SEC = 5;

	private static final DateTimeFormatter DEFAULT_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm a z");

	private DateTimeUtil() {}
	
	public static ZonedDateTime now() {
		return ZonedDateTime.now(ZONE_ID);
	}

	public static ZonedDateTime ofEpochSeconds(long epochSeconds) {
		Instant i = Instant.ofEpochSecond(epochSeconds);
		return ZonedDateTime.ofInstant(i, ZONE_ID);
	}

	public static String format(ZonedDateTime dateTime) {
		return dateTime.withZoneSameInstant(ZONE_ID).format(DEFAULT_FORMATTER);
	}

	public static String format(ZonedDateTime dateTime, String timeZone) {
		String formatted;
		try {
			formatted = dateTime.withZoneSameInstant(ZoneId.of(timeZone)).format(DEFAULT_FORMATTER);
		} catch (Exception ex) {
			formatted = format(dateTime);
		}
		return formatted;
	}
	
	public static int differenceInMin(ZonedDateTime dateTime1, ZonedDateTime dateTime2) {
		long diffInSec = dateTime1.toEpochSecond() - dateTime2.toEpochSecond();
		return (int) diffInSec / 60;
	}
	
	public static boolean almostEqual(ZonedDateTime dateTime1, ZonedDateTime dateTime2) {
		long diffInSec = differenceInMin(dateTime1, dateTime2);
		return diffInSec > -ALMOST_EQUAL_CRITERIA_SEC && diffInSec < ALMOST_EQUAL_CRITERIA_SEC;
	}

	public static ZonedDateTime convertToDateTime(YearMonth yearMonth) {
		return ZonedDateTime.of(yearMonth.getYear(), yearMonth.getMonthValue(), 1, 0, 0, 0, 0, DateTimeUtil.ZONE_ID);
	}
	
}
