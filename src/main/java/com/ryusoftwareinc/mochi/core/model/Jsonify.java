package com.ryusoftwareinc.mochi.core.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public interface Jsonify {

	default String toJSON() {
		String json = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			json = "[ERROR]Unable to parse this object";
		}
		return json;
	}
	
}
