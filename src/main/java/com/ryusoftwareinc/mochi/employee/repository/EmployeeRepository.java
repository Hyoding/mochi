package com.ryusoftwareinc.mochi.employee.repository;

import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.core.repository.EntityRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeRepository extends EntityRepository<Employee> {

	Employee findByEmail(String email);

	boolean existsByEmail(String email);

	@Query(value = "SELECT * FROM EMPLOYEE WHERE business_id = (select id from business where key = ?) and enabled = true order by created_date", nativeQuery = true)
	List<Employee> findEnabledByBusinessKey(String businessKey);

	@Query(value = "select count(*) from employee where business_id = ? and enabled = true", nativeQuery = true)
	int countEnabledById(Long id);

	@Query(value = "select * from employee where enabled is true and last_modified_date is null and id in (select employee_id from reset_password where employee_id not in (select distinct employee_id from reset_password where is_reset is true))", nativeQuery = true)
	List<Employee> findByNeverResetPwd();

}
