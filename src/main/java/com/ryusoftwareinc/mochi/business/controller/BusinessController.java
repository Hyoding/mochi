package com.ryusoftwareinc.mochi.business.controller;

import com.ryusoftwareinc.mochi.business.model.*;
import com.ryusoftwareinc.mochi.business.service.BusinessService;
import com.ryusoftwareinc.mochi.business.service.OrderService;
import com.ryusoftwareinc.mochi.core.controller.AbstractBaseController;
import com.ryusoftwareinc.mochi.core.http.HttpResponse;
import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.employee.model.EmployeeDto;
import com.ryusoftwareinc.mochi.employee.resetPassword.ResetPasswordDto;
import com.ryusoftwareinc.mochi.subscription.SubscriptionConfigDto;
import com.ryusoftwareinc.mochi.subscription.SubscriptionPlans;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/api/business/v1")
@RestController
@CrossOrigin
public class BusinessController extends AbstractBaseController {
	
	private static final Logger log = Logger.getLogger(BusinessController.class);

	@Autowired
	BusinessService service;

	@Autowired
	OrderService orderService;
	
	@PostMapping("register")
	public HttpResponse register(@RequestBody BusinessDto dto) {
		log.info("register: " + dto.getName());
		Business business = new Business(dto);
		Employee owner = new Employee();
		owner.setFirstName(dto.getEmployeeFirstName());
		owner.setLastName(dto.getEmployeeLastName());
		owner.setEmail(dto.getEmployeeEmail());
		owner.setPhoneNumber(dto.getPhoneNumber());
		owner.setPassword(dto.getEmployeePassword());
		business = service.register(business, owner);
		return formResponse(business, "Business created, please check your email to validate registration");
	}

	@GetMapping("validateRegistration")
	public HttpResponse validateRegistration(@RequestParam String key) {
		log.info("validateRegistration");
		Business business = service.validateRegistration(key);
		return formResponse(null, "Thanks for registering " + business.getName());
	}

	@GetMapping("rejectRegistration")
	public HttpResponse rejectRegistration(@RequestParam String key) {
		log.info("rejectRegistration");
		service.rejectRegistration(key);
		return formResponse(null, "Rejected registration");
	}

	@GetMapping("getActiveOrders")
	public HttpResponse getActiveOrders(@RequestParam String businessKey) {
		log.info("getActiveOrders for business: " + businessKey);
		return formResponse(service.getActiveOrders(businessKey), "Delivery Orders retrieved");
	}
	
	@GetMapping("getOrdersToDeliver")
	public HttpResponse getOrdersToDeliver(@RequestParam String businessKey) {
		log.info("getOrdersToDeliver for business: " + businessKey);
		List<Order> orders = service.getOrdersToDeliver(businessKey);
		List<OrderDto> orderDtos = orders.stream().map(o -> new OrderDto(o)).collect(Collectors.toList());
		return formResponse(orderDtos, "Delivery Orders retrieved");
	}
	
	@GetMapping("getOrderHistory")
	public HttpResponse getOrderHistory(@RequestParam String businessKey) {
		log.info("getOrderHistory for business: " + businessKey);
		List<Order> orders = service.getOrderHistory(businessKey);
		List<OrderDto> orderDtos = orders.stream().map(o -> {
			Trip trip = o.getTrip();
			final EmployeeDto driver = trip == null ? null : new EmployeeDto(trip.getEmployee());
			return new OrderDto(o, driver);
		}).collect(Collectors.toList());
		return formResponse(orderDtos, "History Orders retrieved");
	}

	@PostMapping("createOrder")
	public HttpResponse createManualOrder(@RequestBody OrderDto dto) {
		log.info("creating order for " + dto.getBusinessKey());
		validateRequest(dto);
		ManualOrder request = new ManualOrder(dto);
		return formResponse(new OrderDto(service.createManualOrder(request)), "Order created");
	}
	
	@PostMapping("updateOrder")
	public HttpResponse updateOrder(@RequestBody OrderDto dto) {
		log.info("Updating order " + dto.getKey());
		validateRequest(dto);
		Order order = orderService.updateOrder(dto);
		return formResponse(new OrderDto(order), "Updated order");
	}
	
	@PostMapping("rejectOrder")
	public HttpResponse rejectOrder(@RequestBody OrderDto dto) {
		log.info("rejectOrder: " + dto.getKey());
		validateRequest(dto);
		orderService.rejectOrder(dto.getKey());
		return formResponse(null, "Order rejected");
	}
	
	@GetMapping("getMobileAppConfig")
	public HttpResponse getMobileAppConfig(@RequestParam String email) {
		log.info("getMobileAppConfig for employee: " + email);
		MobileAppConfig config = service.getMobileAppConfig(email);
		return formResponse(config, "Config retrieved");
	}
	
	@GetMapping("validateLogin")
	public HttpResponse validateLogin(@RequestParam String email, @RequestParam String password) {
		log.info("validateLogin for employee: " + email);
		boolean isValid = service.validateLogin(email, password);
		return formResponse(isValid, null);
	}

	@PostMapping("addEmployee")
	public HttpResponse addEmployee(@RequestBody EmployeeDto dto) {
		log.info("addEmployee");
		validateRequest(dto);
		Employee employee = service.addEmployee(dto);
		return formResponse(new EmployeeDto(employee), "Employee added");
	}

	@PostMapping("updateEmployee")
	public HttpResponse updateEmployee(@RequestBody EmployeeDto dto) {
		log.info("updateEmployee");
		validateRequest(dto);
		service.updateEmployee(dto);
		return formResponse(null, "Employee updated");
	}

	@PostMapping("removeEmployee")
	public HttpResponse removeEmployee(@RequestBody EmployeeDto dto) {
		log.info("removeEmployee");
		validateRequest(dto);
		service.removeEmployee(dto.getKey(), dto.getRequester().getUserKey());
		return formResponse(null, "Employee deleted");
	}

	@GetMapping("getEmployees")
	public HttpResponse getEmployees(@RequestParam String businessKey) {
		log.info("getEmployees");
		List<Employee> employees = service.getEmployees(businessKey);
		return formResponse(employees.stream().map(e -> new EmployeeDto(e)));
	}

	@GetMapping("getTags")
	public HttpResponse getTags(@RequestParam String businessKey) {
		log.info("getTags");
		List<Tag> tags = service.getTags(businessKey);
		return formResponse(tags.stream().map(e -> new TagDto(e)));
	}

	@PostMapping("addTag")
	public HttpResponse addTag(@RequestBody TagDto dto) {
		log.info("addTag");
		validateRequest(dto);
		Tag tag = service.addTag(dto);
		return formResponse(new TagDto(tag), "Tag created");
	}

	@PostMapping("updateTag")
	public HttpResponse updateTag(@RequestBody TagDto dto) {
		log.info("updateTag");
		validateRequest(dto);
		service.updateTag(dto);
		return formResponse(null, "Tag updated");
	}

	@PostMapping("removeTag")
	public HttpResponse removeTag(@RequestBody TagDto dto) {
		log.info("removeTag");
		validateRequest(dto);
		service.removeTag(dto.getKey(), dto.getRequester().getUserKey());
		return formResponse(null, "Tag deleted");
	}

	@PostMapping("resetEmpPwd")
	public HttpResponse resetEmpPwd(@RequestBody ResetPasswordDto dto) {
		log.info("resetEmpPwd");
		service.resetEmployeePassword(dto.getKey(), dto.getPassword());
		return formResponse(null, "Password reset");
	}

	@GetMapping("getSubscriptionPlans")
	public HttpResponse getSubscriptionPlans(@RequestParam String businessKey) {
		log.info("getSubscriptionPlans");
		SubscriptionPlans subscriptionPlans = service.getSubscriptionPlans(businessKey);
		return formResponse(subscriptionPlans);
	}

	@PostMapping("changeSubscriptionPlan")
	public HttpResponse changeSubscriptionPlan(@RequestBody SubscriptionConfigDto dto) {
		log.info("changeSubscriptionPlan");
		service.changeSubscriptionPlan(dto);
		return formResponse(null, "Changed plan");
	}

	@GetMapping("getActiveOrdersSummary")
	public HttpResponse getActiveOrdersSummary(@RequestParam String businessKey) {
		log.info("getActiveOrdersSummary");
		return formResponse(service.getActiveOrdersSummary(businessKey));
	}

}
