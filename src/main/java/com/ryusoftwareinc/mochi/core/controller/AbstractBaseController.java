package com.ryusoftwareinc.mochi.core.controller;

import com.ryusoftwareinc.mochi.business.model.ApiOrder;
import com.ryusoftwareinc.mochi.business.service.BusinessService;
import com.ryusoftwareinc.mochi.core.exception.ClientException;
import com.ryusoftwareinc.mochi.core.exception.Concerning;
import com.ryusoftwareinc.mochi.core.http.BaseHttpRequest;
import com.ryusoftwareinc.mochi.core.http.HttpRequester;
import com.ryusoftwareinc.mochi.core.http.HttpResponse;
import com.ryusoftwareinc.mochi.core.notification.NotificationService;
import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.employee.service.EmployeeService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@Component
public abstract class AbstractBaseController {
	
	private static final Logger logger = Logger.getLogger(AbstractBaseController.class);
	
	private static final HttpStatus CLIENT_ERROR_CODE = HttpStatus.BAD_REQUEST;
	private static final HttpStatus SERVER_ERROR_CODE = HttpStatus.INTERNAL_SERVER_ERROR;
		
	@Autowired private NotificationService notificationService;
	@Autowired private EmployeeService employeeService;
	@Autowired private BusinessService businessService;
	
	protected HttpResponse formResponse(Object data) {
		return formResponse(data, null);
	}
	
	protected HttpResponse formResponse(Object data, String msg) {
		HttpResponse response = new HttpResponse();
		response.setResponseData(data);
		response.setMessage(msg);
		return response;
	}

	protected void validateRequest(ApiOrder request) {
		validateBusiness(request.getKey());
	}

	protected void validateRequest(BaseHttpRequest request) {
		validateBusiness(request.getBusinessKey());

		// validate requester
		HttpRequester requester = request.getRequester();
		if (requester == null) {
			return;
		}
		String employeeKey = requester.getUserKey();
		Employee employee = employeeService.findByKey(employeeKey);
		validateEmployee(employee);
	}

	private void validateBusiness(String key) {
		if (!businessService.getRepo().isBusinessEnabled(key)) {
			throw new ClientException("This business is not enabled");
		}
	}

	private void validateEmployee(Employee employee) {
		if (employee == null || !employee.isEnabled()) {
			throw new ClientException("You are not authorized to perform this action");
		}
	}

	/*
	 * Think about how many status we want to use.
	 * For just now 400 and 500
	 */
	@ExceptionHandler({RuntimeException.class})
    public ResponseEntity<HttpResponse> handleException(RuntimeException ex, HttpServletRequest request) {
		logger.info("Handling " + ex.getClass().getSimpleName() + ": " + ex.getMessage());
		logger.info(request.getRequestURI());
		if (ex instanceof ClientException) {
			if (ex instanceof Concerning) {
				notificationService.sendErrorToDev(ex);
			}
			return new ResponseEntity<>(formResponse(null, ex.getMessage()), CLIENT_ERROR_CODE);
		}
		logger.error(ex);
		notificationService.sendErrorToDev(ex);
		return new ResponseEntity<>(formResponse(null, "Server error. If problem continues please contact the developer"), SERVER_ERROR_CODE);
    }

}
