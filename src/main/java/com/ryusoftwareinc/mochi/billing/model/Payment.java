package com.ryusoftwareinc.mochi.billing.model;

import com.ryusoftwareinc.mochi.core.model.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class Payment extends AbstractEntity {

    /*
        Use this to store payment info from payment APIs if needed
     */
    private String apiKey;

    @Enumerated(EnumType.STRING)
    private PaymentType type;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public PaymentType getType() {
        return type;
    }

    public void setType(PaymentType type) {
        this.type = type;
    }

}
