package com.ryusoftwareinc.mochi.customer;

import com.ryusoftwareinc.mochi.business.model.Order;
import com.ryusoftwareinc.mochi.core.repository.EntityRepository;

import java.util.List;

public interface CustomerSurveyRepository extends EntityRepository<CustomerSurvey> {

    CustomerSurvey findByOrder(Order order);

    List<CustomerSurvey> findByOrderIn(List<Order> orders);

}
