package com.ryusoftwareinc.mochi.location.mapbox;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Feature {

    private List<String> center;
    private List<MContext> context;

    public List<String> getCenter() {
        return center;
    }

    public void setCenter(List<String> center) {
        this.center = center;
    }

    public List<MContext> getContext() {
        return context;
    }

    public void setContext(List<MContext> context) {
        this.context = context;
    }
}
