package com.ryusoftwareinc.mochi.business.controller;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ryusoftwareinc.mochi.business.model.OrderDto;
import com.ryusoftwareinc.mochi.business.model.Trip;
import com.ryusoftwareinc.mochi.business.model.TripDto;
import com.ryusoftwareinc.mochi.business.service.TripService;
import com.ryusoftwareinc.mochi.core.controller.AbstractBaseController;
import com.ryusoftwareinc.mochi.core.http.HttpResponse;

@RequestMapping("/api/trip/v1")
@RestController
@CrossOrigin
public class TripController extends AbstractBaseController {
	
	private static final Logger logger = Logger.getLogger(TripController.class);
	
	@Autowired TripService service;
	
	@GetMapping("findCurrentTrip")
	public HttpResponse getMyTrip(@RequestParam String employeeKey) {
		logger.info("getMyTrip for employee " + employeeKey);
		Trip trip = service.getRepo().findCurrentTripByEmployeeKey(employeeKey);
		if (trip == null) {
			return formResponse(null, "No active trip");
		}
		return formResponse(new TripDto(trip), null);
	}

	@PostMapping("startTrip")
	public HttpResponse startTrip(@RequestBody TripDto dto) {
		logger.info("startTrip requested by employee: " + dto.getRequester().getUserKey());
		validateRequest(dto);
		synchronized (this) {
			Trip trip = service.startTrip(dto.getRequester().getUserKey(), dto.getOrderKeys(), dto.getLocation());
			return formResponse(new TripDto(trip), "Trip started");
		}
	}
	
	@PostMapping("deliveredOrder")
	public HttpResponse deliveredOrder(@RequestBody OrderDto dto) {
		logger.info("deliveredOrder " + dto.getKey());
		service.deliveredOrder(dto);
		return formResponse(null, "Customer will be informed the order is delivered");
	}
	
	@PostMapping("finishTrip")
	public HttpResponse finishTrip(@RequestBody TripDto dto) {
		logger.info("finishTrip " + dto.getKey());
		Trip trip = service.findByKey(dto.getKey());
		service.finishTrip(trip);
		return formResponse(null, "Trip finished");
	}

	@PostMapping("updateLocation")
	public HttpResponse updateLocation(@RequestBody TripDto dto) {
		logger.info("updateLocation " + dto.getKey());
		Trip trip = service.findByKey(dto.getKey());
		service.updateLocation(trip, dto.getLocation());
		return formResponse(null, "Trip updated");
	}

	@GetMapping("currentTripExists")
	public HttpResponse activeTripExists(@RequestParam String employeeKey) {
		logger.info("currentTripExists " + employeeKey);
		boolean exists = service.getRepo().currentTripExists(employeeKey);
		return formResponse(exists);
	}

	@PostMapping("updateOrderSequence")
	public HttpResponse updateOrderSequence(@RequestBody TripDto dto) {
		logger.info("updateOrderSequence");
		validateRequest(dto);
		Trip trip = service.updateOrderSequence(dto.getKey(), dto.getOrderSequence());
		return formResponse(new TripDto(trip), "Sequence Updated");
	}
	
}
