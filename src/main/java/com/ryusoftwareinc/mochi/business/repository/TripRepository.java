package com.ryusoftwareinc.mochi.business.repository;

import com.ryusoftwareinc.mochi.business.model.Trip;
import com.ryusoftwareinc.mochi.core.repository.EntityRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TripRepository extends EntityRepository<Trip> {

	@Query(
			value = "SELECT * FROM TRIP WHERE ID = (SELECT TRIP_ID FROM ORDERS WHERE ID = ?)",
			nativeQuery = true
	)
	Trip findByOrderId(Long orderId);
	
	@Query(value = "SELECT * FROM TRIP WHERE employee_id = (select id from employee where key = ?) and trip_end_time is null", nativeQuery = true)
	Trip findCurrentTripByEmployeeKey(String employeeKey);

	@Query(value = "SELECT * FROM TRIP WHERE trip_id = (select id from orders where key = ?)", nativeQuery = true)
	Trip findByOrderKey(String orderKey);

	@Query(value = "select exists(select 1 from trip where employee_id = (select id from employee where key = ?) and trip_end_time is null)", nativeQuery = true)
	boolean currentTripExists(String empKey);

	@Query(value = "select * from trip where trip_end_time is null and employee_id in (select id from employee where business_id = (select id from business where key = ?))", nativeQuery = true)
	List<Trip> findActiveByBusinessKey(String businessKey);

	@Query(value = "select * from trip where employee_id in (select id from employee where business_id = ?)", nativeQuery = true)
	List<Trip> findByBusinessId(Long id);

}
