package com.ryusoftwareinc.mochi.employee.feedback;

import com.ryusoftwareinc.mochi.core.repository.EntityRepository;

public interface FeedbackRepository extends EntityRepository<Feedback> {

}
