package com.ryusoftwareinc.mochi.core.controller;

import com.ryusoftwareinc.mochi.core.http.HttpResponse;
import org.jboss.logging.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CoreController extends AbstractBaseController {

    private static final Logger log = Logger.getLogger(CoreController.class);

    @GetMapping("/api/ping")
    public HttpResponse ping() {
        return formResponse(null);
    }

}
