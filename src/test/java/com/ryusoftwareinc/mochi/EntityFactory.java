package com.ryusoftwareinc.mochi;

import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.business.model.Order;
import com.ryusoftwareinc.mochi.business.model.Order.OrderBuilder;
import com.ryusoftwareinc.mochi.core.model.Address;
import com.ryusoftwareinc.mochi.core.model.Coordinate;
import com.ryusoftwareinc.mochi.core.util.DateTimeUtil;
import com.ryusoftwareinc.mochi.employee.model.Employee;
import org.springframework.stereotype.Component;

@Component
public class EntityFactory {

	public Business generateBusiness() {
		Business business = new Business();
		business.setName("testName" + System.currentTimeMillis());
		business.setPhoneNumber("123");
		Address address = new Address();
		address.setCity("Halifax");
		address.setCountry("Canada");
		address.setProvince("NS");
		address.setPostalCode("B3J 0C4");
		business.setAddress(address);
		return business;
	}

	public Order generateOrder() {
		OrderBuilder orderBuilder = new OrderBuilder();
        Address address = generateAddress();
		Order order = orderBuilder
		        .setBusiness(generateBusiness())
		        .setDeliverBy(DateTimeUtil.now())
                .setAddress(address)
                .setEmail("gg@gmail.com")
				.setOrderStatus(Order.OrderStatus.ACCEPTED)
		        .build();
		return order;
	}

	public Employee generateEmployee() {
		Employee emp = new Employee();
		emp.setFirstName("js");
		emp.setLastName("ef");
		emp.setEmail("efefef");
		emp.setPhoneNumber("aef");
		emp.setPassword("aef");
		return emp;
	}

	public Address generateAddress() {
		Address address = new Address();
		address.setStreetAddress("5481 Clyde Street");
		address.setUnit("805");
		address.setCity("Halifax");
		address.setCountry("Canada");
		address.setPostalCode("B3J 0C4");
		address.setPostalCode("Nova Scotia");
		Coordinate coordinate = new Coordinate();
		coordinate.setLatitude(44.642133);
		coordinate.setLongitude(-63.575931);
		address.setCoordinate(coordinate);
		return address;
	}

}
