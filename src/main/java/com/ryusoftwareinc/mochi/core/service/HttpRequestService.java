package com.ryusoftwareinc.mochi.core.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import okhttp3.RequestBody;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ryusoftwareinc.mochi.core.exception.ServerException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@Component
public class HttpRequestService {

	private static final Logger logger = Logger.getLogger(HttpRequestService.class);

	// one instance, reuse
	private final static OkHttpClient HTTP_CLIENT = new OkHttpClient();
	private final static ObjectMapper MAPPER = new ObjectMapper();

	public <T> T post(String url, RequestBody body, Map<String, String> headers, Class<T> klazz) {
		Request.Builder requestBuilder = new Request.Builder()
				.url(url)
				.method("POST", body);
		for (String headerKey : headers.keySet()) {
			requestBuilder.addHeader(headerKey, headers.get(headerKey));
		}
		Request request = requestBuilder.build();
		return executeRequest(request, klazz);
	}

	private <T> T executeRequest(Request request, Class<T> klazz) {
		try (Response response = HTTP_CLIENT.newCall(request).execute()) {
			if (!response.isSuccessful()) {
				throw new IOException("Unexpected code " + response);
			}
			String responseBody = response.body().string();
			return MAPPER.readValue(responseBody, klazz);
		} catch (Exception e) {
			logger.error(e);
			throw new ServerException(e.getMessage());
		}
	}


	public <T> T get(String url, Map<String, String> parms, Class<T> klazz) {
		String params = getParamsString(parms);
		Request request = new Request.Builder()
				.url(url + "?" + params)
				.build();
		return executeRequest(request, klazz);
	}

	public void testGet() {
		Request request = new Request.Builder().url("https://www.google.com/search?q=mkyong")
				.addHeader("custom-key", "mkyong") // add request headers
				.addHeader("User-Agent", "OkHttp Bot").build();

		try (Response response = HTTP_CLIENT.newCall(request).execute()) {

			if (!response.isSuccessful())
				throw new IOException("Unexpected code " + response);

			// Get response body
			System.out.println(response.body().string());
		} catch (IOException e) {
			logger.error(e);
			throw new ServerException(e.getMessage());
		}
	}

	private String getParamsString(Map<String, String> params) {
		StringBuilder result = new StringBuilder();
		try {
			for (Map.Entry<String, String> entry : params.entrySet()) {
				result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
				result.append("=");
				result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
				result.append("&");
			}
		} catch (UnsupportedEncodingException ex) {
			logger.error(ex);
			throw new ServerException(ex.getMessage());
		}
		String resultString = result.toString();
		return resultString.length() > 0 ? resultString.substring(0, resultString.length() - 1) : resultString;
	}

}
