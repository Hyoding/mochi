package com.ryusoftwareinc.mochi.employee.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.core.model.AbstractEntity;

import javax.persistence.*;

@Entity
public class Employee extends AbstractEntity {

	private static final EmployeeType DEFAULT_USER_TYPE = EmployeeType.DRIVER;

	@JsonIgnore
	@OneToOne
	private Business business;
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private EmployeeType type;

	@Column(nullable = false)
	private String firstName;
	@Column(nullable = false)
	private String lastName;
	@Column(nullable = false, unique = true)
	private String email;
	@Column(nullable = false)
	private String phoneNumber;

	@Column(nullable = false)
	private String password;

	public Employee() {
		this.type = DEFAULT_USER_TYPE;
	}
	
	public Business getBusiness() {
		return business;
	}

	public void setBusiness(Business business) {
		this.business = business;
	}

	public boolean isAdmin() {
		return this.type == EmployeeType.MANAGER || this.type == EmployeeType.OWNER;
	}

	public EmployeeType getType() {
		return type;
	}

	public void setType(EmployeeType type) {
		this.type = type;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	protected void beforeSave() {
		this.email = email == null ? null : this.email.toLowerCase();
	}

}
