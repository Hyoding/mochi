package com.ryusoftwareinc.mochi.employee.model;

import com.ryusoftwareinc.mochi.core.http.BaseHttpRequest;

public class EmployeeDto extends BaseHttpRequest {
	
	private String key;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private String email;
	private String type;
	
	public EmployeeDto() {}
	
	public EmployeeDto(Employee emp) {
		this.key = emp.getKey();
		this.firstName = emp.getFirstName();
		this.lastName = emp.getLastName();
		this.phoneNumber = emp.getPhoneNumber();
		this.email = emp.getEmail();
		this.type = emp.getType().getLabel();
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
