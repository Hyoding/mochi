package com.ryusoftwareinc.mochi.location.mapbox;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ryusoftwareinc.mochi.core.model.Jsonify;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OptimizeRoute implements Jsonify {

	private String code;
	private List<Waypoint> waypoints;
	private List<MBTrip> trips;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public List<Waypoint> getWaypoints() {
		return waypoints;
	}
	public void setWaypoints(List<Waypoint> waypoints) {
		this.waypoints = waypoints;
	}
	public List<MBTrip> getTrips() {
		return trips;
	}
	public void setTrips(List<MBTrip> trips) {
		this.trips = trips;
	}
	
	@Override
	public String toString() {
		return toJSON();
	}
	
}
