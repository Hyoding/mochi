delete from subscription_config;
insert into subscription_config (name, inc_orders, max_employees, max_tags, price, use_api, use_import, use_report, use_route_optimization, use_shopify, can_have_addl_Orders, can_send_sms, addl_order_cost, sms_cost)
values
    (
      'Free', /* name */
      100, /* inc_orders */
      100, /* max_employees */
      100, /* max_tags */
      0, /* price */
      true, /* use_api */
      true, /* use_import */
      true, /* use_report */
      true, /* use_route_optimization */
      true, /* use_shopify */
      true, /* can_have_addl_Orders */
      false, /* can_send_sms */
      0, /* addl_order_cost */
      0 /* sms_cost */
    )
;
