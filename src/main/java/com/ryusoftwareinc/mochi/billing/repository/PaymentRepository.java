package com.ryusoftwareinc.mochi.billing.repository;

import com.ryusoftwareinc.mochi.billing.model.Payment;
import com.ryusoftwareinc.mochi.core.repository.EntityRepository;

public interface PaymentRepository extends EntityRepository<Payment> {

}
