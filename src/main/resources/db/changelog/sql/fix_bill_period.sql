alter table invoice drop column bill_period;
alter table invoice add column bill_period timestamp;
update invoice set bill_period = make_date(2021, 1, 1);
