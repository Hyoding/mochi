package com.ryusoftwareinc.mochi.business.service;

import com.ryusoftwareinc.mochi.business.model.*;
import com.ryusoftwareinc.mochi.business.model.Order.OrderStatus;
import com.ryusoftwareinc.mochi.business.repository.OrderRepository;
import com.ryusoftwareinc.mochi.business.repository.TripRepository;
import com.ryusoftwareinc.mochi.core.exception.ClientException;
import com.ryusoftwareinc.mochi.core.exception.ServerException;
import com.ryusoftwareinc.mochi.core.model.Coordinate;
import com.ryusoftwareinc.mochi.core.service.AbstractEntityService;
import com.ryusoftwareinc.mochi.core.util.DateTimeUtil;
import com.ryusoftwareinc.mochi.core.validator.ValidationException;
import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.employee.model.EmployeeDto;
import com.ryusoftwareinc.mochi.employee.service.EmployeeService;
import com.ryusoftwareinc.mochi.location.mapbox.LocationService;
import com.ryusoftwareinc.mochi.location.mapbox.Waypoint;
import com.ryusoftwareinc.mochi.subscription.SubscriptionService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class TripService extends AbstractEntityService<Trip, TripRepository> {

	private final static Logger log = Logger.getLogger(TripService.class);

	@Autowired
	TripRepository repo;
	@Autowired
	OrderRepository orderRepo;
	@Autowired
	EmployeeService empService;
	@Autowired
	LocationService locationService;
	@Autowired
	SubscriptionService subscriptionService;

	@Override
	public TripRepository getRepo() {
		return repo;
	}

	public Trip startTrip(String employeeKey, Set<String> orderKeys, Coordinate startLocation) {
		Employee employee = empService.findByKey(employeeKey);
		Set<Order> orders = new HashSet<>();
		for (Order o : orderRepo.findAllByKeyIn(orderKeys)) {
			orders.add(o);
		}
		return startTrip(employee, orders, startLocation);
	}

    public Trip startTrip(Employee employee, Set<Order> orders, Coordinate startLocation) {
		if (employee == null || orders == null || orders.isEmpty()) {
			throw new ValidationException("Employee and orders cannot be null");
		}

        if (repo.currentTripExists(employee.getKey())) {
            throw new ClientException("This employee already is on a trip");
        }
        orders.stream().forEach(o -> {
            if (o.getStatus() != OrderStatus.ACCEPTED) {
                throw new ValidationException(
                        "Order for address: " + o.getAddress() + " cannot be taken by another trip. Status: " + o.getStatus().getLabel());
            }
        });
        Trip trip = new Trip();
        trip.setEmployee(employee);
        trip.setOrders(orders);
        trip.setLocation(startLocation);
        populateOptimizedRouteDurationAndDistance(trip, startLocation);
        orders.stream().forEach(o -> o.setStatus(OrderStatus.OUT_FOR_DELIVERY));
        return save(trip);
	}

	public void updateLocation(Trip trip, Coordinate location) {
		trip.setLocation(location);
	}

	public TrackOrder trackOrder(Order order) {
		TrackOrder trackOrder = new TrackOrder();

		Business business = order.getBusiness();
		trackOrder.setBusiness(business.getName());
		trackOrder.setOrder(new OrderDto(order));
		trackOrder.setBusinessLocation(order.getBusiness().getAddress().getCoordinate());
		Trip trip = repo.findByOrderId(order.getId());
        trackOrder.setDeliveredTime(order.getDeliveredTime());
		if (trip != null) { // trip not created yet
			trackOrder.setEmployee(new EmployeeDto(trip.getEmployee()));
			trackOrder.setLocation(trip.getLocation());
		}
		if (order.getStatus() == OrderStatus.OUT_FOR_DELIVERY) {
			trackOrder.setTripStartedTime(trip.getCreatedDate());
			OrderSequence seq = trip.getOrderSequence().stream().filter(os -> os.getOrderKey().equals(order.getKey())).findFirst().get();
			final String orderSeq = (seq.getSequence() + 1) + "/" + trip.getOrderSequence().size();
			trackOrder.setSequenceNumber(orderSeq);
			ZonedDateTime latestTripTime = trip.getLastModifiedDate() == null ? trip.getCreatedDate() : trip.getLastModifiedDate();
			ZonedDateTime latestOrderTime = order.getLastModifiedDate() == null ? order.getCreatedDate() : order.getLastModifiedDate();
			if (latestTripTime.isAfter(latestOrderTime)) {
				trackOrder.setLastUpdated(latestTripTime);
			} else {
				trackOrder.setLastUpdated(latestOrderTime);
			}
		} else {
			trackOrder.setLastUpdated(order.getLastModifiedDate() == null ? order.getCreatedDate() : order.getLastModifiedDate());
		}
		return trackOrder;
	}

	public void deliveredOrder(OrderDto dto) {
		Order order = orderRepo.findByKey(dto.getKey());
		Coordinate location = dto.getAddress().getCoordinate();
		deliveredOrder(order, location);
	}

	public void deliveredOrder(Order order, Coordinate location) {
		order.setDeliveredTime(DateTimeUtil.now());
		if (order.getStatus() != OrderStatus.OUT_FOR_DELIVERY) {
			throw new ClientException("This order is already " + order.getStatus().getLabel());
		}
		order.setCompletedLocation(location);
		order.setStatus(OrderStatus.DELIVERED);
		Trip trip = order.getTrip();
		trip.setLocation(location);
	}

	public void finishTrip(Trip trip) {
		Set<Order> ordersNotCompleted = new HashSet<>();
		for (Order o : trip.getOrders()) {
			if (o.getStatus() == OrderStatus.OUT_FOR_DELIVERY) {
				o.setStatus(OrderStatus.ACCEPTED);
				ordersNotCompleted.add(o);
			}
		}
		trip.getOrders().removeAll(ordersNotCompleted);
		trip.setTripEndTime(DateTimeUtil.now());
	}

	public Trip updateOrderSequence(String tripKey, List<OrderSequence> updatedSequence) {
		Trip trip = findByKey(tripKey);
		List<OrderSequence> newOrderSequence = new ArrayList();
		for (int i = 0; i < updatedSequence.size(); ++i) {
			OrderSequence updatedOs = updatedSequence.get(i);
			OrderSequence os = new OrderSequence();
			os.setSequence(i);
			os.setOrderKey(updatedOs.getOrderKey());
			os.setCoordinate(updatedOs.getCoordinate());
			os.setDistance(null);
			os.setDuration(null);
			newOrderSequence.add(os);
		}
		trip.setOrderSequence(newOrderSequence);
		return trip;
	}

	public void populateOptimizedRouteDurationAndDistance(Trip trip, Coordinate startLocation) {
		if (trip == null || startLocation == null) {
			throw new ServerException("Trip and Start location cannot be null");
		}
		if (subscriptionService.shouldTripUseRouteOptimization(trip)) {
			locationService.setOptimizedRouteForTrip(trip, startLocation);
	//		OptimizeRoute route = locationService.getMapboxOptimizedRoute(formCoordinatesSeparatedBySemicolon(trip, startLocation));
	//		List<OrderSequence> orderSequence = guessOrderSequence(trip.getOrders(), route.getWaypoints());
	//		trip.setOrderSequence(orderSequence);
	//		MBTrip mbTrip = route.getTrips().get(0);
	//		trip.setDuration(mbTrip.getDuration() / 60); // to min
	//		trip.setDistance(mbTrip.getDistance() / 1000); // to KM
		} else {
			locationService.setRouteForTrip(trip);
		}
	}

	private String formCoordinatesSeparatedBySemicolon(Trip trip, Coordinate startLocation) {
		List<Coordinate> coordinates = new ArrayList<>();
		coordinates.add(startLocation);
		for (Order order : trip.getOrders()) {
			Coordinate coordinate = order.getAddress().getCoordinate();
			coordinates.add(coordinate);
		}
		List<String> cStrings = coordinates.stream().map(c -> c.getLongitude() + "," + c.getLatitude())
				.collect(Collectors.toList());
		return String.join(";", cStrings);
	}
	
	public List<OrderSequence> guessOrderSequence(Set<Order> orders, List<Waypoint> waypoints) {
		Set<Order> notYetSelectedOrders = new HashSet<>();
		notYetSelectedOrders.addAll(orders);
		List<OrderSequence> orderSequence = new ArrayList<>();
		
		for (Waypoint wp : waypoints) {
			List<String> coords = wp.getLocation();
			String lng = coords.get(0);
			String lat = coords.get(1);
			Coordinate coord = new Coordinate(Double.parseDouble(lat), Double.parseDouble(lng));
			OrderSequence seq = new OrderSequence();
			seq.setCoordinate(coord);
			seq.setSequence(wp.getWaypoint_index());
			if (!wp.getWaypoint_index().equals(0)) { // skip start location
				Order order = guessOrder(notYetSelectedOrders, coord);
				notYetSelectedOrders.remove(order);
				seq.setOrderKey(order.getKey());
			}
			orderSequence.add(seq);
		}
		return orderSequence;
	}
	
	public Order guessOrder(Set<Order> orders, Coordinate wpCoord) {
		double minDistance = Double.MAX_VALUE;
		Order guessedOrder = null;
		for (Order o : orders) {
			Coordinate orderCoord = o.getAddress().getCoordinate();
			double distance = locationService.calculateDistanceInMeters(orderCoord, wpCoord);
			if (distance < minDistance) {
				minDistance = distance;
				guessedOrder = o;
			}
		}
		log.info("distance " + minDistance);
		return guessedOrder;
	}
	
}
