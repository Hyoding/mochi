package com.ryusoftwareinc.mochi.service;

import com.twilio.Twilio;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertTrue;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@SpringBootTest
public class NotificationServiceTest {

    public static final String ACCOUNT_SID =
            "AC4275339addb888f62bdc61195446f3ae";
    public static final String AUTH_TOKEN =
            "dbc53646e355ece12c8f75bc853e4666";

    @Test
    @Disabled
    public void sendSmsShouldWorkWhenNormal() {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        Message message = Message
                .creator(new PhoneNumber("+12897073022"), // to
                        new PhoneNumber("+19027012158"), // from
                        "Where's Wallace? http://192.168.0.103:3000/track/6fb2286d-2b79-40f5-bd64-49a337385ccd")
                .create();

        System.out.println(message.getSid());
        assertTrue(true);
    }

}
