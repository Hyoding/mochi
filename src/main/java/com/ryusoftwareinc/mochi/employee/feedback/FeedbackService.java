package com.ryusoftwareinc.mochi.employee.feedback;

import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.employee.service.EmployeeService;
import com.ryusoftwareinc.mochi.core.notification.NotificationService;
import com.ryusoftwareinc.mochi.core.service.AbstractEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FeedbackService extends AbstractEntityService<Feedback, FeedbackRepository> {

    @Autowired FeedbackRepository repo;
    @Autowired NotificationService notificationService;
    @Autowired
    EmployeeService empService;

    @Override
    public FeedbackRepository getRepo() {
        return repo;
    }

    public Feedback submit(FeedbackDto dto) {
        Employee employee = empService.findByKey(dto.getRequester().getUserKey());
        Feedback feedback = new Feedback();
        feedback.setEmployee(employee);
        feedback.setMessage(dto.getMessage());
        // send notification
        StringBuilder sb = new StringBuilder();
        sb.append("Feedback received from " + employee.getType().getLabel() + ": " + employee.getFirstName() + " " + employee.getLastName());
        sb.append("<br/><br/>");
        sb.append("Feedback key: " + feedback.getKey() + "<br/><br/>");
        sb.append(feedback.getMessage());
        notificationService.sendEmailToSystem("Feedback received", sb.toString());
        return save(feedback);
    }

}
