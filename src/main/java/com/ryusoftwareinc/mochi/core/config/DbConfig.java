package com.ryusoftwareinc.mochi.core.config;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

// Use this??
@Entity(name = "config")
public class DbConfig {
	
	@Id
	private DbConfigID id;
	@Column(nullable=false)
	private String value;
	@Enumerated(EnumType.STRING)
	@Column(nullable=false)
	private DataType dataType;
		
	public DbConfigID getId() {
		return id;
	}

	public void setId(DbConfigID id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public DataType getDataType() {
		return dataType;
	}

	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}

	enum DataType {
		INTEGER, DOUBLE, STRING, DATE
	}
	
	@Embeddable
	public static class DbConfigID implements Serializable {
		
		private static final long serialVersionUID = 9153087767170822106L;
		
		private String type;
		private String name;
		
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
	}
}
