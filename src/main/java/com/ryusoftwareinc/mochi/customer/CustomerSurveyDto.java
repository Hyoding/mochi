package com.ryusoftwareinc.mochi.customer;

import com.ryusoftwareinc.mochi.core.http.BaseHttpRequest;

public class CustomerSurveyDto extends BaseHttpRequest {

    private String orderKey;
    private int productRating;
    private int courierRating;
    private String comment;

    public String getOrderKey() {
        return orderKey;
    }

    public void setOrderKey(String orderKey) {
        this.orderKey = orderKey;
    }

    public int getProductRating() {
        return productRating;
    }

    public void setProductRating(int productRating) {
        this.productRating = productRating;
    }

    public int getCourierRating() {
        return courierRating;
    }

    public void setCourierRating(int courierRating) {
        this.courierRating = courierRating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
