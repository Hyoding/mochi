package com.ryusoftwareinc.mochi.core.service;

import java.util.Optional;
import java.util.Set;

import com.ryusoftwareinc.mochi.core.exception.EntityNotFoundException;
import com.ryusoftwareinc.mochi.core.exception.ServerException;
import com.ryusoftwareinc.mochi.core.model.AbstractEntity;
import com.ryusoftwareinc.mochi.core.repository.EntityRepository;

public abstract class AbstractEntityService<T extends AbstractEntity, X extends EntityRepository<T>> implements EntityRepository<T> {
	
	public abstract X getRepo();

	@Override
	public <S extends T> S save(S entity) {
		return getRepo().save(entity);
	}

	@Override
	public <S extends T> Iterable<S> saveAll(Iterable<S> entities) {
		return getRepo().saveAll(entities);
	}

	@Override
	public Optional<T> findById(Long id) {
		return getRepo().findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return getRepo().existsById(id);
	}

	@Override
	public Iterable<T> findAll() {
		return getRepo().findAll();
	}

	@Override
	public Iterable<T> findAllById(Iterable<Long> ids) {
		return getRepo().findAllById(ids);
	}

	@Override
	public long count() {
		return getRepo().count();
	}

	@Override
	public void deleteById(Long id) {
		T entity = getRepo().findById(id).get();
		delete(entity);
	}

	@Override
	public void delete(T entity) {
		entity.setEnabled(false);
		getRepo().save(entity);
	}

	@Override
	public void deleteAll(Iterable<? extends T> entities) {
		for (T entity : entities) {
			delete(entity);
		}
	}

	@Override
	public void deleteAll() {
		throw new ServerException("Think about this");
	}

	@Override
	public T findByKey(String key) {
		T entity = getRepo().findByKey(key);
		if (entity == null) {
			throw new EntityNotFoundException("Invalid key");
		}
		return entity;
	}
	
	@Override
	public Iterable<T> findAllByKeyIn(Set<String> keys) {
		return getRepo().findAllByKeyIn(keys);
	}

	public boolean existsByKey(String key) {
		return getRepo().existsByKey(key);
	}
	
}
