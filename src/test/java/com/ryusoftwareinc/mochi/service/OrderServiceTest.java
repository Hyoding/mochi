package com.ryusoftwareinc.mochi.service;

import com.ryusoftwareinc.mochi.EntityFactory;
import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.business.model.ManualOrder;
import com.ryusoftwareinc.mochi.business.model.Order;
import com.ryusoftwareinc.mochi.business.service.BusinessService;
import com.ryusoftwareinc.mochi.business.service.OrderService;
import com.ryusoftwareinc.mochi.core.exception.ClientException;
import com.ryusoftwareinc.mochi.core.util.DateTimeUtil;
import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.employee.service.EmployeeService;
import com.ryusoftwareinc.mochi.subscription.SubscriptionConfig;
import com.ryusoftwareinc.mochi.subscription.SubscriptionConfigRepository;
import com.ryusoftwareinc.mochi.subscription.SubscriptionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class OrderServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	EntityFactory entityFactory;

	@Autowired
	OrderService orderService;

	@Autowired
	BusinessService businessService;

	@Autowired
	SubscriptionConfigRepository subscriptionConfigRepository;

	@Autowired
	EmployeeService employeeService;

	@Autowired
	SubscriptionService subscriptionService;

	@Test
	public void getOrdersCountByYearAndMonthShouldRetrieveCorrectNumber() {
		SubscriptionConfig subscriptionConfig = new SubscriptionConfig();
		subscriptionConfig.setName(System.currentTimeMillis() + "");
		subscriptionConfig.setIncOrders(100);
		subscriptionConfig = subscriptionConfigRepository.save(subscriptionConfig);

		Business business = entityFactory.generateBusiness();
		business.setSubscription(subscriptionConfig);
		business = businessService.save(business);

		ZonedDateTime d_2020_05_13 = ZonedDateTime.of(2020, 5, 13, 0, 0, 0, 0, DateTimeUtil.ZONE_ID);
		Order order1 = entityFactory.generateOrder();
		order1.setCreatedDate(d_2020_05_13);
		order1.setBusiness(business);
		orderService.save(order1);

		Order order2 = entityFactory.generateOrder();
		order2.setCreatedDate(d_2020_05_13);
		order2.setBusiness(business);
		orderService.save(order2);

		int ordersCount_2020_05 = orderService.getRepo().getCountByBusinessAndYearAndMonth(business, 2020, 05);
		assertEquals(ordersCount_2020_05, 2);

		ZonedDateTime d_2021_05_13 = ZonedDateTime.of(2021, 5, 13, 0, 0, 0, 0, DateTimeUtil.ZONE_ID);
		Order order3 = entityFactory.generateOrder();
		order3.setCreatedDate(d_2021_05_13);
		order3.setBusiness(business);
		orderService.save(order3);

		Order order4 = entityFactory.generateOrder();
		order4.setCreatedDate(d_2021_05_13);
		order4.setBusiness(business);
		orderService.save(order4);

		Order order5 = entityFactory.generateOrder();
		order5.setCreatedDate(d_2021_05_13);
		order5.setBusiness(business);
		orderService.save(order5);

		// check prev yr one more time
		ordersCount_2020_05 = orderService.getRepo().getCountByBusinessAndYearAndMonth(business, 2020, 05);
		assertEquals(ordersCount_2020_05, 2);

		int ordersCount_2021_05 = orderService.getRepo().getCountByBusinessAndYearAndMonth(business, 2021, 05);
		assertEquals(ordersCount_2021_05, 3);

		ZonedDateTime d_2021_06_13 = ZonedDateTime.of(2021, 6, 13, 0, 0, 0, 0, DateTimeUtil.ZONE_ID);
		Order order6 = entityFactory.generateOrder();
		order6.setCreatedDate(d_2021_06_13);
		order6.setBusiness(business);
		orderService.save(order6);

		int ordersCount_2021_06 = orderService.getRepo().getCountByBusinessAndYearAndMonth(business, 2021, 06);
		assertEquals(ordersCount_2021_06, 1);
	}

	@Test
	public void saveOrderShouldWorkIfIncOrdersNotExceededAndCanHaveAddlOrdersFalse() {
		SubscriptionConfig subscriptionConfig = new SubscriptionConfig();
		subscriptionConfig.setName(System.currentTimeMillis() + "");
		subscriptionConfig.setIncOrders(100);
		subscriptionConfig.setCanHaveAddlOrders(false);
		subscriptionConfig = subscriptionConfigRepository.save(subscriptionConfig);

		Business business = entityFactory.generateBusiness();
		business.setSubscription(subscriptionConfig);
		business = businessService.save(business);

		Order order1 = entityFactory.generateOrder();
		order1.setBusiness(business);
		orderService.save(order1);
	}

	@Test
	public void saveOrderShouldWorkIfIncOrdersNotExceededAndCanHaveAddlOrdersTrue() {
		SubscriptionConfig subscriptionConfig = new SubscriptionConfig();
		subscriptionConfig.setName(System.currentTimeMillis() + "");
		subscriptionConfig.setIncOrders(100);
		subscriptionConfig.setCanHaveAddlOrders(true);
		subscriptionConfig = subscriptionConfigRepository.save(subscriptionConfig);

		Business business = entityFactory.generateBusiness();
		business.setSubscription(subscriptionConfig);
		business = businessService.save(business);

		Order order1 = entityFactory.generateOrder();
		order1.setBusiness(business);
		orderService.save(order1);
	}

	@Test
	public void saveOrderShouldWorkIfIncOrdersExceededAndCanHaveAddlOrders() {
		SubscriptionConfig subscriptionConfig = new SubscriptionConfig();
		subscriptionConfig.setName(System.currentTimeMillis() + "");
		subscriptionConfig.setIncOrders(0);
		subscriptionConfig.setCanHaveAddlOrders(true);
		subscriptionConfig = subscriptionConfigRepository.save(subscriptionConfig);

		Business business = entityFactory.generateBusiness();
		business.setSubscription(subscriptionConfig);
		business = businessService.save(business);

		Order order1 = entityFactory.generateOrder();
		order1.setBusiness(business);
		orderService.save(order1);
	}

	@Test
	public void saveOrderShouldFailIfIncOrdersExceededAndCanHaveAddlOrdersFalse() {
		SubscriptionConfig subscriptionConfig = new SubscriptionConfig();
		subscriptionConfig.setName(System.currentTimeMillis() + "");
		subscriptionConfig.setIncOrders(0);
		subscriptionConfig.setCanHaveAddlOrders(false);
		subscriptionConfig = subscriptionConfigRepository.save(subscriptionConfig);

		Business business = entityFactory.generateBusiness();
		business.setSubscription(subscriptionConfig);
		business = businessService.save(business);

		Order order1 = entityFactory.generateOrder();
		order1.setBusiness(business);
		try {
			orderService.save(order1);
			fail();
		} catch (ClientException ex) {}
	}

	@Test
	public void freePlanShouldNotSendSMS() {
		SubscriptionConfig freePlan = subscriptionService.getTestPlan();
		Business business = entityFactory.generateBusiness();
		business.setSubscription(freePlan);
		business = businessService.save(business);

		Employee employee = entityFactory.generateEmployee();
		employee.setBusiness(business);
		employee = employeeService.save(employee);
		ZonedDateTime edt = DateTimeUtil.now().plusHours(2);

		// create a few orders and test
		ManualOrder orderRequest1 = new ManualOrder();
		orderRequest1.setBusinessKey(business.getKey());
		orderRequest1.setEmployeeKey(employee.getKey());
		orderRequest1.setContactNumber("1231231234");
		orderRequest1.setEmail(null);
		orderRequest1.setAddress(entityFactory.generateAddress());
		orderRequest1.setEstimatedDeliveryTime(edt);

		Order order = businessService.createManualOrder(orderRequest1);
		assertFalse(order.isSentSms());
	}

}
