package com.ryusoftwareinc.mochi.report;

import com.ryusoftwareinc.mochi.core.util.DateTimeUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.ArrayList;
import java.util.List;

/*
    Knows how to build 1 sheet
 */
public class ReportBuilder {

    private static final int DEFAULT_COLUMN_WIDTH = 4000;
    private static final int ROW_IDX_TITLE = 0;
    private static final int ROW_IDX_DATE = ROW_IDX_TITLE + 1;
    private static final int ROW_IDX_HEADER = ROW_IDX_DATE + 2;
    private static final int ROW_IDX_DATA = ROW_IDX_HEADER + 1;

    private String name;
    private List<String> headerColumns = new ArrayList<>();
    private List<List<String>> dataDataTable = new ArrayList<>();

    public ReportBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ReportBuilder setHeaderColumn(List<String> headerColumns) {
        this.headerColumns = headerColumns;
        return this;
    }

    public ReportBuilder addHeaderColumn(String column) {
        this.headerColumns.add(column);
        return this;
    }

    public ReportBuilder setDataTable(List<List<String>> dataDataTable) {
        this.dataDataTable = dataDataTable;
        return this;
    }

    public ReportBuilder addDataRow(List<String> row) {
        this.dataDataTable.add(row);
        return this;
    }

    public Workbook build() {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet(name);
        createTitleAndDateRows(sheet);
        createHeaderRow(workbook, sheet);
        createBody(sheet);
        return workbook;
    }

    private void createTitleAndDateRows(Sheet sheet) {
        Row titleRow = sheet.createRow(ROW_IDX_TITLE);
        Cell titleCell = titleRow.createCell(0);
        titleCell.setCellValue(name);
        sheet.addMergedRegion(new CellRangeAddress(ROW_IDX_TITLE, ROW_IDX_TITLE, 0, 2));

        Row dateRow = sheet.createRow(ROW_IDX_DATE);
        Cell dateCell = dateRow.createCell(0);
        dateCell.setCellValue("Generated date: " + DateTimeUtil.now().toString());
        sheet.addMergedRegion(new CellRangeAddress(ROW_IDX_DATE, ROW_IDX_DATE, 0, 2));
    }

    private void createHeaderRow(Workbook workbook, Sheet sheet) {
        // create headers
        CellStyle headerStyle = workbook.createCellStyle();
        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        headerStyle.setFont(font);
        Row header = sheet.createRow(ROW_IDX_HEADER);
        for (int i = 0; i < headerColumns.size(); ++i) {
            sheet.setColumnWidth(i, DEFAULT_COLUMN_WIDTH);
            String headerColumn = headerColumns.get(i);
            Cell headerCell = header.createCell(i);
            headerCell.setCellValue(headerColumn);
            headerCell.setCellStyle(headerStyle);
        }
    }

    private void createBody(Sheet sheet) {
        // create body
        int dataIdx = ROW_IDX_DATA;
        for (List<String> rowData : dataDataTable) {
            Row row = sheet.createRow(dataIdx++);
            for (int i = 0; i < rowData.size(); ++i) {
                Cell dataCell = row.createCell(i);
                String value = rowData.get(i);
                dataCell.setCellValue(value);
            }
        }
    }

}
