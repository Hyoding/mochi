package com.ryusoftwareinc.mochi.employee.resetPassword;

import com.ryusoftwareinc.mochi.core.http.BaseHttpRequest;
import com.ryusoftwareinc.mochi.employee.model.Employee;

public class ResetPasswordDto extends BaseHttpRequest {

	private String key;
	private String password;
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
