package com.ryusoftwareinc.mochi.business.model;

import com.ryusoftwareinc.mochi.core.model.AbstractEntity;
import com.ryusoftwareinc.mochi.core.model.Address;
import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.subscription.SubscriptionConfig;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Business extends AbstractEntity {

	@Column(nullable = false, unique = true)
	private String name;
	@Column(nullable = false)
	private String phoneNumber;
	@Embedded
	private Address address;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "business_id")
	private Set<Employee> employees;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "business_id")
	private Set<Tag> tags;
	private boolean isValidated;

	@ManyToOne
	@JoinColumn(nullable = false)
	private SubscriptionConfig subscription;

	/*
		Created so we can allow SMS during free period.
		Remove once we are ready to accept payments.

		Pre-charge 2 cents per SMS?
	 */
	private int freeSmsCount;
	
	public Business() {
		this.employees = new HashSet<>();
		this.tags = new HashSet<>();
		this.isValidated = false;
	}
	
	public Business(BusinessDto dto) {
		this();
		this.name = dto.getName();
		this.address = dto.getAddress();
		this.phoneNumber = dto.getPhoneNumber();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Set<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(Set<Employee> employees) {
		this.employees = employees;
	}

	public boolean addEmployee(Employee employee) {
		employee.setBusiness(this);
		return employees.add(employee);
	}
	
	public boolean updateEmployee(Employee employee) {
		boolean removed = employees.remove(employee);
		employees.add(employee);
		return removed;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	public boolean isValidated() {
		return isValidated;
	}

	public void setValidated(boolean validated) {
		isValidated = validated;
	}

	public SubscriptionConfig getSubscription() {
		return subscription;
	}

	public void setSubscription(SubscriptionConfig subscription) {
		this.subscription = subscription;
	}

	public int getFreeSmsCount() {
		return freeSmsCount;
	}

	public void setFreeSmsCount(int freeSmsCount) {
		this.freeSmsCount = freeSmsCount;
	}

}
