package com.ryusoftwareinc.mochi.employee.resetPassword;

import com.ryusoftwareinc.mochi.core.config.Config;
import com.ryusoftwareinc.mochi.core.exception.ClientException;
import com.ryusoftwareinc.mochi.core.notification.Email;
import com.ryusoftwareinc.mochi.core.notification.NotificationService;
import com.ryusoftwareinc.mochi.core.service.AbstractEntityService;
import com.ryusoftwareinc.mochi.core.util.DateTimeUtil;
import com.ryusoftwareinc.mochi.core.util.PasswordAuthentication;
import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.employee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ResetPasswordService extends AbstractEntityService<ResetPassword, ResetPasswordRepository> {

	@Autowired
	ResetPasswordRepository repo;

	@Autowired
	EmployeeService employeeService;

	@Autowired
	NotificationService notificationService;
	
	@Autowired
	Config config;

	@Override
	public ResetPasswordRepository getRepo() {
		return repo;
	}
	
	public void sendResetPasswordEmail(String email) {
		email = email.toLowerCase();
		Employee employee = employeeService.getRepo().findByEmail(email);
		if (employee == null) {
			throw new ClientException("We do not recognize this email");
		}
		ResetPassword resetPassword = new ResetPassword();
		resetPassword.setEmployee(employee);
		save(resetPassword);
		notificationService.sendEmail(generateResetEmail(employee, resetPassword));
	}

	public void resetPassword(String resetPasswordKey, String newPassword) {
		ResetPassword resetPwd = findByKey(resetPasswordKey);
		if (isExpired(resetPwd) || resetPwd.isReset()) {
			throw new ClientException("This session is expired. Please get a new token");
		}
		Employee employee = employeeService.findByKey(resetPwd.getEmployee().getKey());
		PasswordAuthentication pwdAuth = new PasswordAuthentication();
		String hashedPwd = pwdAuth.hash(newPassword.toCharArray());
		employee.setPassword(hashedPwd);
		resetPwd.setReset(true);
	}

	private boolean isExpired(ResetPassword resetPwd) {
		if (DateTimeUtil.now().minusHours(config.getPwdExpiredInHour()).isAfter(resetPwd.getCreatedDate())) {
			return true;
		}
		return false;
	}

	private Email generateResetEmail(Employee employee, ResetPassword rPwd) {
		Email email = new Email();
		email.setSubject("Reset your password");
		email.setFrom(config.getSystemEmail());
		email.addTo(employee.getEmail());
		email.addBcc(config.getSystemEmail());

		StringBuilder sb = new StringBuilder();
		sb.append("Hi " + employee.getFirstName() + ",<br/><br/>");
		sb.append("Click " + notificationService.htmlLink(config.getUrlWebResetPassword() + "/" + rPwd.getKey(), "here") + " to reset your password<br/><br/>");
		sb.append("This will expire in " + config.getPwdExpiredInHour() + " hour(s)<br/><br/>");
		sb.append("Thank you,<br/>");
		sb.append("Mochi Team");
		email.setHtmlText(sb.toString());
		return email;
	}

}
