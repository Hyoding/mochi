package com.ryusoftwareinc.mochi.report;

import com.ryusoftwareinc.mochi.core.model.AbstractEntity;
import com.ryusoftwareinc.mochi.employee.model.Employee;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import java.time.ZonedDateTime;

@Entity
public class ReportRequest extends AbstractEntity {

    @OneToOne
    private Employee requester;

    @Enumerated(EnumType.STRING)
    private ReportType reportType;

    private ZonedDateTime dateFrom;

    private ZonedDateTime dateTo;

    public Employee getRequester() {
        return requester;
    }

    public void setRequester(Employee requester) {
        this.requester = requester;
    }

    public ReportType getReportType() {
        return reportType;
    }

    public void setReportType(ReportType reportType) {
        this.reportType = reportType;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(ZonedDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ZonedDateTime getDateTo() {
        return dateTo;
    }

    public void setDateTo(ZonedDateTime dateTo) {
        this.dateTo = dateTo;
    }
}
