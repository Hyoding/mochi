package com.ryusoftwareinc.mochi.billing.service;

import com.ryusoftwareinc.mochi.billing.model.BillableItem;
import com.ryusoftwareinc.mochi.billing.model.Invoice;
import com.ryusoftwareinc.mochi.billing.model.Payment;
import com.ryusoftwareinc.mochi.billing.repository.InvoiceRepository;
import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.business.repository.OrderRepository;
import com.ryusoftwareinc.mochi.core.exception.ConcerningClientException;
import com.ryusoftwareinc.mochi.core.service.AbstractEntityService;
import com.ryusoftwareinc.mochi.core.util.DateTimeUtil;
import com.ryusoftwareinc.mochi.subscription.SubscriptionConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.YearMonth;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class InvoiceService extends AbstractEntityService<Invoice, InvoiceRepository> {

    public static final int DUE_DAY_OF_MONTH = 14;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    OrderRepository orderRepository;

    @Override
    public InvoiceRepository getRepo() {
        return invoiceRepository;
    }

    public Invoice generateInvoice(Business business) {
        YearMonth yearMonth = YearMonth.now();
        return generateInvoice(business, yearMonth);
    }

    /*
        Use this for generating invoice.
        Plan is to run it first day of the month to generate previous.
     */
    public Invoice generateInvoice(Business business, YearMonth yearMonth) {
        if (!canHaveInvoice(business, yearMonth)) {
            return null;
        }
        Invoice invoice = invoiceRepository.findByBusinessAndBillPeriod(business, DateTimeUtil.convertToDateTime(yearMonth));
        if (invoice == null) {
            invoice = createNewInvoice(business, yearMonth);
        } else {
            populateBillableItemsAndCalculateTotal(invoice);
        }
        return invoice;
    }

    private boolean canHaveInvoice(Business business, YearMonth yearMonth) {
        ZonedDateTime createdDate = business.getCreatedDate();
        YearMonth createdYearMonth = YearMonth.of(createdDate.getYear(), createdDate.getMonthValue());
        return createdYearMonth.isBefore(yearMonth) || createdYearMonth.equals(yearMonth);
    }

    public void payInvoice(Invoice invoice) {
        Payment payment = processPayment(invoice);
        invoice.setPayment(payment);
    }

    /*
        TODO: Link this to Stripe or other payment APIs
     */
    private Payment processPayment(Invoice invoice) {
        if (invoice.getPayment() != null) {
            throw new ConcerningClientException("This invoice was already paid");
        }

        Payment payment = new Payment();
        // use some API code here

        return payment;
    }

    private Invoice createNewInvoice(Business business, YearMonth yearMonth) {
        Invoice invoice = new Invoice();
        invoice.setBusiness(business);
        YearMonth yearMonthNow = YearMonth.now();
        ZonedDateTime dueDate = ZonedDateTime.of(yearMonthNow.getYear(), yearMonthNow.getMonthValue(), DUE_DAY_OF_MONTH, 0, 0, 0, 0, DateTimeUtil.ZONE_ID);
        invoice.setDueDate(dueDate);
        invoice.setBillPeriod(DateTimeUtil.convertToDateTime(yearMonth));
        populateBillableItemsAndCalculateTotal(invoice);
        return save(invoice);
    }

    private void populateBillableItemsAndCalculateTotal(Invoice invoice) {
        // redo below each time getting the invoice
        populateBillableItems(invoice);

        // calculate total
        double subtotal = 0;
        for (BillableItem billItem : invoice.getItems()) {
            subtotal += billItem.getPrice() * billItem.getCount();
        }
        invoice.setSubtotal(subtotal);
        final double total = subtotal * (1 + invoice.getTax());
        invoice.setTotal(total);
        if (total == 0) {
            Payment payment = new Payment();
            invoice.setPayment(payment);
        }
    }

    private void populateBillableItems(Invoice invoice) {
        List<BillableItem> itemList = new ArrayList<>();
        invoice.setItems(itemList);

        // By default always add subscription
        BillableItem billSubscription = new BillableItem();
        billSubscription.setBillable(BillableItem.Billable.SUBSCRIPTION);
        billSubscription.setCount(1);
        SubscriptionConfig subscriptionConfig = invoice.getBusiness().getSubscription();
        billSubscription.setPrice(subscriptionConfig.getPrice());
        itemList.add(billSubscription);

        Business business = invoice.getBusiness();

        // find out sms
        ZonedDateTime billPeriod = invoice.getBillPeriod();
        final int smsCount = orderRepository.getSmsCountByBusinessAndYearAndMonth(business, billPeriod.getYear(), billPeriod.getMonthValue());
        if (smsCount > 0) {
            BillableItem smsBill = new BillableItem();
            smsBill.setBillable(BillableItem.Billable.SMS);
            smsBill.setCount(smsCount);
            smsBill.setPrice(subscriptionConfig.getSmsCost());
            itemList.add(smsBill);
        }

        // find out addl orders
        final int cntOfOrdersThisMonth = orderRepository.getCountByBusinessAndYearAndMonth(business, billPeriod.getYear(), billPeriod.getMonthValue());
        final int includedOrders = subscriptionConfig.getIncOrders();
        final int addlOrders = cntOfOrdersThisMonth - includedOrders;
        if (addlOrders > 0) {
            BillableItem addlOrderBill = new BillableItem();
            addlOrderBill.setBillable(BillableItem.Billable.ADDL_ORDERS);
            addlOrderBill.setCount(addlOrders);
            addlOrderBill.setPrice(subscriptionConfig.getAddlOrderCost());
            itemList.add(addlOrderBill);
        }
    }

}
