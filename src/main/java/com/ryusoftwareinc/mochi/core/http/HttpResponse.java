package com.ryusoftwareinc.mochi.core.http;

import com.ryusoftwareinc.mochi.core.model.Jsonify;

public class HttpResponse implements Jsonify {
	
	private String message;
	private Object responseData;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getResponseData() {
		return responseData;
	}
	public void setResponseData(Object responseData) {
		this.responseData = responseData;
	}
	
	@Override
	public String toString() {
		return toJSON();
	}

}
