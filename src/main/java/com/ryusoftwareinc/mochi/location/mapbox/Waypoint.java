package com.ryusoftwareinc.mochi.location.mapbox;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Waypoint {

	private Double distance;
	private String name;
	private List<String> location;
	private Integer waypoint_index;
	private Integer trips_index;
	
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getLocation() {
		return location;
	}
	public void setLocation(List<String> location) {
		this.location = location;
	}
	public Integer getWaypoint_index() {
		return waypoint_index;
	}
	public void setWaypoint_index(Integer waypoint_index) {
		this.waypoint_index = waypoint_index;
	}
	public Integer getTrips_index() {
		return trips_index;
	}
	public void setTrips_index(Integer trips_index) {
		this.trips_index = trips_index;
	}
	
}
