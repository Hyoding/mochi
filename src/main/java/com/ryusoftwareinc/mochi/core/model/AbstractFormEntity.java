package com.ryusoftwareinc.mochi.core.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

@MappedSuperclass
public abstract class AbstractFormEntity extends AbstractEntity {

	private static final FormStatus DEFAULT_FORM_STATUS = FormStatus.SUBMITTED_FOR_REVIEW;
	
	@Column(nullable = false)
	private Long userId;
	private String userNote;
	@Enumerated(EnumType.STRING)
	private FormStatus status;
	private Long processorId;
	private String processorNote;
	
	@Transient
	private List<MultipartFile> files;
	
    public AbstractFormEntity() {
    	this.status = DEFAULT_FORM_STATUS;
    	this.files = new ArrayList<>();
    	this.userNote = "";
    	this.processorNote = "";
    }

	public FormStatus getStatus() {
		return status;
	}

	public void setStatus(FormStatus status) {
		this.status = status;
	}

	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}
	
	public String getUserNote() {
		return userNote;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public String getProcessorNote() {
		return processorNote;
	}

	public void setProcessorNote(String processorNote) {
		this.processorNote = processorNote;
	}

	public enum FormStatus {
		SUBMITTED_FOR_REVIEW, APPROVED, REJECTED
	}

	public Long getProcessorId() {
		return processorId;
	}

	public void setProcessorId(Long processorId) {
		this.processorId = processorId;
	}

}