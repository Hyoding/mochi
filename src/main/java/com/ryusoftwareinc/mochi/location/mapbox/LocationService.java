package com.ryusoftwareinc.mochi.location.mapbox;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ryusoftwareinc.mochi.business.model.Order;
import com.ryusoftwareinc.mochi.business.model.OrderSequence;
import com.ryusoftwareinc.mochi.business.model.Trip;
import com.ryusoftwareinc.mochi.core.config.Config;
import com.ryusoftwareinc.mochi.core.exception.ClientException;
import com.ryusoftwareinc.mochi.core.exception.ServerException;
import com.ryusoftwareinc.mochi.core.model.Address;
import com.ryusoftwareinc.mochi.core.model.Coordinate;
import com.ryusoftwareinc.mochi.core.service.HttpRequestService;
import com.ryusoftwareinc.mochi.core.util.DateTimeUtil;
import com.ryusoftwareinc.mochi.location.routexl.*;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import org.apache.commons.codec.binary.StringUtils;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class LocationService {

    private static final Logger log = Logger.getLogger(LocationService.class);

    @Autowired
    Config config;

    @Autowired
    HttpRequestService httpService;

    public void setRouteForTrip(Trip trip) {
        List<OrderSequence> orderSequences = new ArrayList<>();
        int i = 0;
        for (Order order : trip.getOrders()) {
            OrderSequence orderSequence = new OrderSequence();
            orderSequence.setSequence(i++);
            orderSequence.setOrderKey(order.getKey());
            orderSequence.setCoordinate(order.getAddress().getCoordinate());
            orderSequences.add(orderSequence);
        }
        trip.setOrderSequence(orderSequences);
    }

    public void setOptimizedRouteForTrip(Trip trip, Coordinate startLocation) {
        final String url = "https://api.routexl.com/tour";
        final String authEncoded = "am9lcnl1OmhrYWNoNzI5NjgwMg==";
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Basic " + authEncoded);
        RequestBody body = formOptimizedRouteRequestBody(trip, startLocation, true);
        TourOutput tourOutput = httpService.post(url, body, headers, TourOutput.class);
        if (!tourOutput.isFeasible()) {
            log.info("Not feasible with restrictions, trying again without it");
            body = formOptimizedRouteRequestBody(trip, startLocation, false);
            tourOutput = httpService.post(url, body, headers, TourOutput.class);
            if (!tourOutput.isFeasible()) {
                log.error("Still not feasible without restrictions! Think about what to do");
            }
        }
        populateTripFromTourOutput(trip, tourOutput, startLocation);
    }

    /*
        coords: String coords separated by ;
     */
    public OptimizeRoute getMapboxOptimizedRoute(String coords) {
        final String url = config.getMapboxUrlOptimization() + "/" + coords;
        Map<String, String> params = new HashMap<>();
        params.put("source", "first");
//        params.put("roundtrip", "true");
        params.put("access_token", config.getMapboxAccessToken());
        OptimizeRoute route =  httpService.get(url, params, OptimizeRoute.class);
        if (route.getTrips() == null) {
            log.error("couldn't find route with url: " + url);
            throw new ServerException("Couldn't find optimized route");
        }
        return route;
    }

    // streetAddress city    for best result
    public void validateAndFillAddress(Address address) {
        final String url = config.getMapboxUrlGeocoding() + "/" + utf8Encode(address) + ".json";
        Map<String, String> params = new HashMap<>();
        params.put("access_token", config.getMapboxAccessToken());
        GeocodingResponse geocodingResponse = httpService.get(url, params, GeocodingResponse.class);
        List<Feature> features = geocodingResponse.getFeatures();
        if (features == null || features.isEmpty()) {
            throw new ClientException("Could not recognize this address");
        }
        Feature feature = features.get(0);
        if (feature.getCenter().isEmpty())  {
            throw new ClientException("Could not recognize this address");
        }
        repopulateAddress(feature, address);
    }

    private void repopulateAddress(Feature feature, Address address) {
        for (MContext c : feature.getContext()) {
            if (c.getId().toLowerCase().startsWith("region")) {
                address.setProvince(c.getText());
                address.setIso3166(c.getShort_code());
            }
            if (c.getId().toLowerCase().startsWith("country")) {
                address.setCountry(c.getText());
            }
        }
        List<String> coordinates = feature.getCenter();
        String lng = coordinates.get(0);
        String lat = coordinates.get(1);
        address.setCoordinate(new Coordinate(Double.parseDouble(lat), Double.parseDouble(lng)));
    }

    private String utf8Encode(Address address) {
        String str = address.getStreetAddress() + " " + address.getCity() + " " + address.getProvince() + " " + address.getPostalCode();
        byte[] bytes = StringUtils.getBytesUtf8(str);
        return StringUtils.newStringUtf8(bytes);
    }

    private List<Location> getLocations(Set<Order> orders, Coordinate startLocation, boolean addRestriction) {
        ZonedDateTime now = DateTimeUtil.now();
        List<Location> locations = new ArrayList<>();
        Location startLoc = new Location();
        startLoc.setName("Start");
        startLoc.setLat(startLocation.getLatitude().toString());
        startLoc.setLng(startLocation.getLongitude().toString());
        locations.add(startLoc);
        for (Order order : orders) {
            Location location = new Location();
            Address orderAddress = order.getAddress();
            location.setName(order.getKey());
            location.setLat(orderAddress.getCoordinate().getLatitude().toString());
            location.setLng(orderAddress.getCoordinate().getLongitude().toString());
            // TODO: Think about customizing this
            location.setServicetime(5);
            if (addRestriction) {
                ZonedDateTime deliverBy = order.getDeliverBy();
                if (deliverBy.isAfter(now)) {
                    Restrictions restrictions = new Restrictions();
                    long minutes = ChronoUnit.MINUTES.between(now, deliverBy);
                    restrictions.setDue((int) minutes);
                    location.setRestrictions(restrictions);
                }
            }
            locations.add(location);
        }
        return locations;
    }

    private void populateTripFromTourOutput(Trip trip, TourOutput tourOutput, Coordinate startLocation) {
        List<OrderSequence> orderSequences = new ArrayList<>();
        double cumDistance = 0;
        int cumDuration = 0;

        double prevDistance = 0;
        int prevDuration = 0;

        for (int i = 0; i < tourOutput.getRoute().size(); ++i) {
            Route route = tourOutput.getRoute().get(i + "");
            if (i == tourOutput.getCount() - 1) { // last
                cumDistance = route.getDistance();
                cumDuration = route.getArrival();
            }
            OrderSequence orderSequence = new OrderSequence();
            if (i == 0) { // starting point
                continue;
            } else {
                orderSequence.setSequence(i - 1);
                orderSequence.setOrderKey(route.getName());
                orderSequence.setDuration(route.getArrival() - prevDuration);
                orderSequence.setDistance(route.getDistance() - prevDistance);
                Order order = trip.getOrders().stream().filter(o -> o.getKey().equals(route.getName())).findFirst().get();
                orderSequence.setCoordinate(order.getAddress().getCoordinate());
                prevDistance = route.getDistance();
                prevDuration = route.getArrival();
            }
            orderSequences.add(orderSequence);
        }
        trip.setOrderSequence(orderSequences);
        trip.setDuration(cumDuration);
        trip.setDistance(cumDistance);
    }

    private RequestBody formOptimizedRouteRequestBody(Trip trip, Coordinate startLocation, boolean addRestriction) {
        ObjectMapper mapper = new ObjectMapper();
        List<Location> locations = getLocations(trip.getOrders(), startLocation, addRestriction);
        String stringifyLocations = "";
        try {
            stringifyLocations = mapper.writeValueAsString(locations);
        } catch (Exception ex) {
            throw new ServerException("Error stringifying locations");
        }
        return new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("locations", stringifyLocations)
                .build();
    }

    public double calculateDistanceInMeters(Coordinate c1, Coordinate c2) {
        return org.apache.lucene.util.SloppyMath.haversinMeters(c1.getLatitude(), c1.getLongitude(), c2.getLatitude(), c2.getLongitude());
    }

}
