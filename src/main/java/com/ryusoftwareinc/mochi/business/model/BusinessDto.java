package com.ryusoftwareinc.mochi.business.model;

import com.ryusoftwareinc.mochi.core.http.BaseHttpRequest;
import com.ryusoftwareinc.mochi.core.model.Address;

public class BusinessDto extends BaseHttpRequest {
	
	private String key;
	private String name;
	private String phoneNumber;
	private String email;
	private Address address;
	private String employeeFirstName;
	private String employeeLastName;
	private String employeeEmail;
	private String employeePhoneNumber;
	private String employeePassword;
	
	public BusinessDto() {}
	
	public BusinessDto(Business business) {
		this.key = business.getKey();
		this.name = business.getName();
		this.phoneNumber = business.getPhoneNumber();
		this.address = business.getAddress();
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getEmployeeFirstName() {
		return employeeFirstName;
	}

	public void setEmployeeFirstName(String operatorFirstName) {
		this.employeeFirstName = operatorFirstName;
	}

	public String getEmployeeLastName() {
		return employeeLastName;
	}

	public void setEmployeeLastName(String operatorLastName) {
		this.employeeLastName = operatorLastName;
	}

	public String getEmployeeEmail() {
		return employeeEmail;
	}

	public void setEmployeeEmail(String operatorEmail) {
		this.employeeEmail = operatorEmail;
	}

	public String getEmployeePhoneNumber() {
		return employeePhoneNumber;
	}

	public void setEmployeePhoneNumber(String operatorPhoneNumber) {
		this.employeePhoneNumber = operatorPhoneNumber;
	}

	public String getEmployeePassword() {
		return employeePassword;
	}

	public void setEmployeePassword(String operatorHashedPassword) {
		this.employeePassword = operatorHashedPassword;
	}

}
