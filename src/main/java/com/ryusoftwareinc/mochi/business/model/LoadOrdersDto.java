package com.ryusoftwareinc.mochi.business.model;

import com.ryusoftwareinc.mochi.core.http.BaseHttpRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public class LoadOrdersDto extends BaseHttpRequest {

	private String timeZone = "America/Halifax";
	private List<MultipartFile> files;

	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

}
