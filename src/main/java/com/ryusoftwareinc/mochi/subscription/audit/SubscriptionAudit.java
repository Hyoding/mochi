package com.ryusoftwareinc.mochi.subscription.audit;

import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.core.model.AbstractEntity;
import com.ryusoftwareinc.mochi.subscription.SubscriptionConfig;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class SubscriptionAudit extends AbstractEntity {

    @ManyToOne(optional = false)
    private Business business;

    @ManyToOne(optional = false)
    private SubscriptionConfig subscriptionConfig;

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public SubscriptionConfig getSubscriptionConfig() {
        return subscriptionConfig;
    }

    public void setSubscriptionConfig(SubscriptionConfig subscriptionConfig) {
        this.subscriptionConfig = subscriptionConfig;
    }

}
