package com.ryusoftwareinc.mochi.business.service;

import com.ryusoftwareinc.mochi.business.model.ApiOrder;
import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.business.model.Order;
import com.ryusoftwareinc.mochi.business.model.OrderDto;
import com.ryusoftwareinc.mochi.business.repository.OrderRepository;
import com.ryusoftwareinc.mochi.core.config.Config;
import com.ryusoftwareinc.mochi.core.exception.ClientException;
import com.ryusoftwareinc.mochi.core.exception.EntityKeyNotProvidedException;
import com.ryusoftwareinc.mochi.core.model.Address;
import com.ryusoftwareinc.mochi.core.notification.Email;
import com.ryusoftwareinc.mochi.core.notification.NotificationService;
import com.ryusoftwareinc.mochi.core.notification.SMS;
import com.ryusoftwareinc.mochi.core.service.AbstractEntityService;
import com.ryusoftwareinc.mochi.core.util.DateTimeUtil;
import com.ryusoftwareinc.mochi.location.mapbox.LocationService;
import com.ryusoftwareinc.mochi.subscription.SubscriptionConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.Set;

@Service
@Transactional
public class OrderService extends AbstractEntityService<Order, OrderRepository> {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    NotificationService notificationService;

    @Autowired
    BusinessService businessService;

    @Autowired
    LocationService locationService;

    @Autowired
    Config config;

    @Autowired
    TagService tagService;

    @Override
    public OrderRepository getRepo() {
        return orderRepository;
    }

    @Override
    public Order save(Order order) {
        Business business = order.getBusiness();
        SubscriptionConfig subscriptionConfig = business.getSubscription();
        ZonedDateTime dateTime = DateTimeUtil.now();
        final int month = dateTime.getMonth().getValue();
        final int yr = dateTime.getYear();
        final int cntOfOrdersThisMonth = orderRepository.getCountByBusinessAndYearAndMonth(business, yr, month);
        final int includedOrders = subscriptionConfig.getIncOrders();
        if (cntOfOrdersThisMonth >= includedOrders) {
            if (!subscriptionConfig.isCanHaveAddlOrders()) {
                throw new ClientException(subscriptionConfig.getName() + " plan only supports " + includedOrders + " orders. Please upgrade your plan to add more orders");
            }
        }
        return super.save(order);
    }

    public Order updateOrder(OrderDto dto) {
        if (StringUtils.isBlank(dto.getKey())) {
            throw new EntityKeyNotProvidedException("Order key needs to be provided");
        }
        Order order = findByKey(dto.getKey());
        if (order == null) {
            throw new ClientException("This order was not found");
        }
        if (dto.getAddress() != null) {
            order.setAddress(dto.getAddress());
        }
        if (!StringUtils.isBlank(dto.getEmail())) {
            order.setEmail(dto.getEmail());
        }
        if (!StringUtils.isBlank(dto.getPhoneNumber())) {
            order.setPhoneNumber(dto.getPhoneNumber());
        }
        if (!StringUtils.isBlank(dto.getNote())) {
            order.setNote(dto.getNote());
        }
        if (dto.getTagsKey() != null) {
            order.setTags(dto.getTagsKey());
        }
        boolean sendUpdate = false;
        if (dto.getDeliverBy() != null) {
            if (!order.getDeliverBy().isEqual(dto.getDeliverBy())) {
                if (dto.getDeliverBy().isBefore(DateTimeUtil.now())) {
                    throw new ClientException("Update time cannot be in past");
                }
                order.setDeliverBy(dto.getDeliverBy());
                sendUpdate = true;
            }
        }
        if (sendUpdate) {
            sendOrderNotification(order, true);
        }
        return order;
    }

    public void rejectOrder(String orderKey) {
        if (StringUtils.isBlank(orderKey)) {
            throw new EntityKeyNotProvidedException("Order key needs to be provided");
        }
        Order order = findByKey(orderKey);
        order.setStatus(Order.OrderStatus.REJECTED);
        order.setCanceledTime(DateTimeUtil.now());
    }

    public Order convertToOrder(ApiOrder apiOrder) {
        Business business = businessService.findByKey(apiOrder.getKey());
        Address address = apiOrder.getAddress();
        if (address == null) {
            throw new ClientException("Address is required");
        }
        if (address.getCoordinate() == null) {
            locationService.validateAndFillAddress(apiOrder.getAddress());
        }
        Set<String> tagKeys = tagService.convertTagNamesToTagKeys(business.getKey(), apiOrder.getTags());
        return new Order.OrderBuilder()
                .setBusiness(business)
                .setOrderStatus(Order.OrderStatus.ACCEPTED)
                .setNote(apiOrder.getNote())
                .setPhoneNumber(apiOrder.getPhoneNumber())
                .setEmail(apiOrder.getEmail())
                .setAddress(apiOrder.getAddress())
                .setDeliverBy(apiOrder.getDeliverBy())
                .setTags(tagKeys)
                .build();
    }

    public void sendOrderNotification(Order order) {
        sendOrderNotification(order, false);
    }

    private void sendOrderNotification(Order order, boolean isUpdate) {
        if (StringUtils.isBlank(order.getEmail()) && !StringUtils.isBlank(order.getPhoneNumber())) {
            if (!isUpdate) {
                Business business = order.getBusiness();
                final int freeSmsCnt = business.getFreeSmsCount();
                if (freeSmsCnt > 0) {
                    notificationService.sendSms(generateSMSToCustomer(order));
                    business.setFreeSmsCount(freeSmsCnt - 1);
//                    order.setSentSms(true);
                } else {
                    SubscriptionConfig subscriptionConfig = order.getBusiness().getSubscription();
                    if (subscriptionConfig.isCanSendSms()) {
                        notificationService.sendSms(generateSMSToCustomer(order));
                        order.setSentSms(true);
                    }
                }
            }
        } else {
            if (isUpdate) {
                notificationService.sendEmail(generateUpdateOrderEmailToCustomer(order));
            } else {
                notificationService.sendEmail(generateNewOrderEmailToCustomer(order));
            }
        }
    }

    private SMS generateSMSToCustomer(Order order) {
        Business business = order.getBusiness();
        SMS sms = new SMS();
        sms.setFrom(config.getSmsFrom());
        sms.setTo(order.getPhoneNumber());
        sms.setBody(business.getName() + " confirmed your order. " + config.getUrlWebTrack() + "/" + order.getKey());
        return sms;
    }

    private Email generateNewOrderEmailToCustomer(Order order) {
        Business business = order.getBusiness();
        Email email = new Email();
        email.setSubject(business.getName() + " confirmed your order");
        email.setFrom(config.getSystemEmail());
        email.addTo(order.getEmail());
        if (config.isMailBccToSystem()) {
            email.addBcc(config.getSystemEmail());
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Hi,<br/><br/>");
        sb.append("Your order has been confirmed just now<br/><br/>");
        sb.append("Click " + notificationService.htmlLink(config.getUrlWebTrack() + "/" + order.getKey(), "here") + " to track your order!<br/><br/>");
        sb.append("Thank you,<br/>");
        sb.append("Mochi Team");
        email.setHtmlText(sb.toString());
        return email;
    }

    private Email generateUpdateOrderEmailToCustomer(Order order) {
        Business business = order.getBusiness();
        Email email = new Email();
        email.setSubject(business.getName() + " updated your order");
        email.setFrom(config.getSystemEmail());
        email.addTo(order.getEmail());
        if (config.isMailBccToSystem()) {
            email.addBcc(config.getSystemEmail());
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Hi,<br/><br/>");
        sb.append("Your order has been updated just now<br/><br/>");
        sb.append("Click " + notificationService.htmlLink(config.getUrlWebTrack() + "/" + order.getKey(), "here") + " to track your order!<br/><br/>");
        sb.append("Thank you,<br/>");
        sb.append("Mochi Team");
        email.setHtmlText(sb.toString());
        return email;
    }

}
