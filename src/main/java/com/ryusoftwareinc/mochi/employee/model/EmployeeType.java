package com.ryusoftwareinc.mochi.employee.model;

import com.ryusoftwareinc.mochi.core.exception.ConcerningClientException;

public enum EmployeeType {
	OWNER("Owner", true),
	MANAGER("Manager", true),
	OPERATOR("Operator"),
	DRIVER("Driver"),
	;

	private String label;
	private boolean isAdmin;

	private EmployeeType(String label) {
		this.label = label;
	}

	private EmployeeType(String label, boolean isAdmin) {
		this(label);
		this.isAdmin = false;
	}

	public String getLabel() {
		return label;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public static EmployeeType labelOf(String label) {
		for (EmployeeType et : values()) {
			if (et.label.equals(label)) {
				return et;
			}
		}
		throw new ConcerningClientException("This label cannot be found: " + label);
	}
}
