package com.ryusoftwareinc.mochi.core.exception;

public class ServerException extends BaseAppException {

	private static final long serialVersionUID = 6210185004086540140L;
	
	public ServerException(String msg) {
		super(msg);
	}

}
