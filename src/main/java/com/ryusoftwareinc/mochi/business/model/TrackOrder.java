package com.ryusoftwareinc.mochi.business.model;

import com.ryusoftwareinc.mochi.core.model.Coordinate;
import com.ryusoftwareinc.mochi.employee.model.EmployeeDto;

import java.time.ZonedDateTime;

public class TrackOrder {
	
	private EmployeeDto employee;
	private Coordinate location;
	private ZonedDateTime lastUpdated;
	private ZonedDateTime tripStartedTime;
	private ZonedDateTime deliveredTime;
	private OrderDto order;
	private Coordinate businessLocation;
	private String sequenceNumber;
	private String business;
	
	public EmployeeDto getEmployee() {
		return employee;
	}
	public void setEmployee(EmployeeDto employee) {
		this.employee = employee;
	}
	public Coordinate getCoordinate() {
		return location;
	}
	public void setCoordinate(Coordinate coordinate) {
		this.location = coordinate;
	}

	public Coordinate getLocation() {
		return location;
	}

	public void setLocation(Coordinate location) {
		this.location = location;
	}

	public ZonedDateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(ZonedDateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	public OrderDto getOrder() {
		return order;
	}
	
	public void setOrder(OrderDto order) {
		this.order = order;
	}
	
	public Coordinate getBusinessLocation() {
		return businessLocation;
	}
	
	public void setBusinessLocation(Coordinate businessLocation) {
		this.businessLocation = businessLocation;
	}

	public ZonedDateTime getTripStartedTime() {
		return tripStartedTime;
	}

	public void setTripStartedTime(ZonedDateTime tripStartedTime) {
		this.tripStartedTime = tripStartedTime;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public ZonedDateTime getDeliveredTime() {
		return deliveredTime;
	}

	public void setDeliveredTime(ZonedDateTime deliveredTime) {
		this.deliveredTime = deliveredTime;
	}

	public String getBusiness() {
		return business;
	}

	public void setBusiness(String business) {
		this.business = business;
	}
}
