package com.ryusoftwareinc.mochi.report;

import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.business.model.Order;
import com.ryusoftwareinc.mochi.business.model.Trip;
import com.ryusoftwareinc.mochi.business.repository.OrderRepository;
import com.ryusoftwareinc.mochi.business.repository.TripRepository;
import com.ryusoftwareinc.mochi.core.exception.ServerException;
import com.ryusoftwareinc.mochi.core.model.Address;
import com.ryusoftwareinc.mochi.customer.CustomerSurvey;
import com.ryusoftwareinc.mochi.customer.CustomerSurveyRepository;
import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.location.mapbox.LocationService;
import org.apache.poi.ss.usermodel.Workbook;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.*;

@Service
public class ReportFactory {

    private static final Logger log = Logger.getLogger(ReportFactory.class);

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    CustomerSurveyRepository customerSurveyRepository;
    @Autowired
    LocationService locationService;
    @Autowired
    TripRepository tripRepository;

    /*
        For now only supporting entire history
     */
    public Workbook generate(ReportType reportType, Business business, ZonedDateTime start, ZonedDateTime end) {
        log.info("Generating report: " + reportType + " start: " + start + " end: " + end);
        switch(reportType) {
            case DETAILED_ORDER:
                return generateDETAILED_ORDER(business, start, end);
            case TRIPS:
                return generateTRIPS(business, start, end);
            default:
                throw new ServerException("This report is not supported: " + reportType);
        }
    }

    private Workbook generateTRIPS(Business business, ZonedDateTime start, ZonedDateTime end) {
        ReportBuilder reportBuilder = new ReportBuilder();
        reportBuilder.setName(ReportType.TRIPS.getLabel());

        final String[] header = {
                "Trip id", "Employee", "Trip start date", "Trip end date",
                "Orders count", "Distance (km)", "Duration (min)"
        };
        reportBuilder.setHeaderColumn(Arrays.asList(header));

        List<Trip> trips = tripRepository.findByBusinessId(business.getId());
        for (Trip trip : trips) {
            List<String> dataRow = new ArrayList<>();
            dataRow.add(trip.getId().toString());
            Employee employee = trip.getEmployee();
            dataRow.add(employee.getLastName() + ", " + employee.getFirstName());
            dataRow.add(trip.getCreatedDate().toString());
            dataRow.add(trip.getTripEndTime() == null ? "" : trip.getTripEndTime().toString());
            dataRow.add(trip.getOrderSequence().size() + "");
            dataRow.add(trip.getDistance() + "");
            dataRow.add(trip.getDuration() + "");
            reportBuilder.addDataRow(dataRow);
        }

        return reportBuilder.build();
    }

    private Workbook generateDETAILED_ORDER(Business business, ZonedDateTime start, ZonedDateTime end) {
        List<Order> orders = orderRepository.findByBusiness(business);

        Map<Long, CustomerSurvey> customerSurveyByOrderId = new HashMap<>();
        for (CustomerSurvey survey : customerSurveyRepository.findByOrderIn(orders)) {
            customerSurveyByOrderId.put(survey.getOrder().getId(), survey);
        }

        ReportBuilder reportBuilder = new ReportBuilder();
        reportBuilder.setName(ReportType.DETAILED_ORDER.getLabel());

        final String[] header = {
                "Order id", "Created date", "Created by", "Modified date", "Status",
                "Canceled date", "Promised date", "Delivered date", "SMS sent", "note",
                "Phone number", "Email", "Street address", "Unit", "City", "Province", "Country", "Postal code", "Coordinates",
                "Trip id", "Employee", "Delivered coordinates", "Delivery distance diff (m)",
                "Survey id", "Survey created date", "Survey modified date", "Product rating", "Courier rating", "Comment"
        };
        reportBuilder.setHeaderColumn(Arrays.asList(header));

        List<List<String>> dataTable = new ArrayList<>();
        for (Order order : orders) {
            List<String> dataRow = new ArrayList<>();
            dataTable.add(dataRow);

            dataRow.add(order.getId().toString()); // ID
            dataRow.add(order.getCreatedDate().toString()); // created date
            Employee createdBy = order.getCreatedBy();
            dataRow.add(createdBy == null ? "System" : createdBy.getLastName() + ", " + createdBy.getFirstName()); // created by
            dataRow.add(order.getLastModifiedDate() == null ? "" : order.getLastModifiedDate().toString()); // modified date
            dataRow.add(order.getStatus().getLabel()); // Status
            dataRow.add(order.getCanceledTime() == null ? "" : order.getCanceledTime().toString()); // Canceled date
            dataRow.add(order.getDeliverBy() == null ? "" : order.getDeliverBy().toString()); // promised date
            dataRow.add(order.getDeliveredTime() == null ? "" : order.getDeliveredTime().toString()); // delivered date
            dataRow.add(order.isSentSms() ? "Yes" : "No"); // sent sms
            dataRow.add(order.getNote()); // note
            dataRow.add(order.getPhoneNumber());
            dataRow.add(order.getEmail());
            Address address = order.getAddress();
            dataRow.add(address.getStreetAddress());
            dataRow.add(address.getUnit());
            dataRow.add(address.getCity());
            dataRow.add(address.getProvince());
            dataRow.add(address.getCountry());
            dataRow.add(address.getPostalCode());
            dataRow.add(address.getCoordinate() == null ? "" : address.getCoordinate().toString());

            Trip trip = order.getTrip();
            if (trip == null) {
                continue;
            }
            // "Trip id", "Employee", "Delivered coordinates", "Delivery distance diff in meters",
            dataRow.add(trip.getId().toString());
            dataRow.add(trip.getEmployee().getLastName() + ", " + trip.getEmployee().getFirstName());
            dataRow.add(order.getCompletedLocation() == null ? "" : order.getCompletedLocation().toString());
            if (address.getCoordinate() != null && order.getCompletedLocation() != null) {
                double diffDistance = locationService.calculateDistanceInMeters(address.getCoordinate(), order.getCompletedLocation());
                dataRow.add(diffDistance + "");
            } else {
                dataRow.add("");
            }

            // "Survey id", "Survey created date", "Survey modified date", "Product rating", "Courier rating", "Comment"
            CustomerSurvey customerSurvey = customerSurveyByOrderId.get(order.getId());
            if (customerSurvey == null) {
                continue;
            }
            dataRow.add(customerSurvey.getId().toString());
            dataRow.add(customerSurvey.getCreatedDate() == null ? "" : customerSurvey.getCreatedDate().toString());
            dataRow.add(customerSurvey.getProductRating() + "");
            dataRow.add(customerSurvey.getCourierRating() + "");
            dataRow.add(customerSurvey.getComment());
        }
        reportBuilder.setDataTable(dataTable);

        return reportBuilder.build();
    }

}
