package com.ryusoftwareinc.mochi.business.model;

import com.ryusoftwareinc.mochi.core.http.BaseHttpRequest;
import com.ryusoftwareinc.mochi.core.model.Address;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.ZonedDateTime;

public class TagDto extends BaseHttpRequest {

	private String key;
	private String name;
	private String businessKey;

	public TagDto() {}

	public TagDto(Tag tag) {
		this.key = tag.getKey();
		this.name = tag.getName();
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}
}
