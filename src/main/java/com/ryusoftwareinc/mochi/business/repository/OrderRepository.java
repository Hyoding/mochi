package com.ryusoftwareinc.mochi.business.repository;

import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.business.model.Order;
import com.ryusoftwareinc.mochi.core.repository.EntityRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderRepository extends EntityRepository<Order> {

	@Query(value = "SELECT * FROM ORDERS WHERE business_id = (select id from business where key = ?) and status in ('ACCEPTED', 'OUT_FOR_DELIVERY') order by deliver_by", nativeQuery = true)
	List<Order> findActiveOrders(String businessKey);

	@Query(value = "SELECT * FROM ORDERS WHERE business_id = (select id from business where key = ?) and status = 'ACCEPTED' order by deliver_by", nativeQuery = true)
	List<Order> findOrdersToDeliver(String businessKey);
	
	@Query(value = "SELECT * FROM ORDERS WHERE business_id = (select id from business where key = ?) order by created_date desc limit 50", nativeQuery = true)
	List<Order> findOrderHistory(String businessKey);

	@Query("select COUNT(o) from orders o where business = ?1 and year(o.createdDate) = ?2 and month(o.createdDate) = ?3")
	int getCountByBusinessAndYearAndMonth(Business business, int year, int month);

	@Query("select COUNT(o) from orders o where business = ?1 and year(o.createdDate) = ?2 and month(o.createdDate) = ?3 and sentSms is true")
	int getSmsCountByBusinessAndYearAndMonth(Business business, int year, int month);

	List<Order> findByBusiness(Business business);

}
