package com.ryusoftwareinc.mochi.service;

import com.ryusoftwareinc.mochi.EntityFactory;
import com.ryusoftwareinc.mochi.core.model.Address;
import com.ryusoftwareinc.mochi.core.model.Coordinate;
import com.ryusoftwareinc.mochi.location.mapbox.LocationService;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class LocationServiceTest {

    @Autowired
    LocationService service;

    @Autowired
    EntityFactory entityFactory;

    @Test
    @Disabled
    public void getCoordinateFromStreetAddressShouldRetrieveCoordWhenNormal() {
        Address address = entityFactory.generateAddress();
        service.validateAndFillAddress(address);
        Coordinate coordinate = address.getCoordinate();
        assertEquals(44.642133, coordinate.getLatitude());
        assertEquals(-63.575931, coordinate.getLongitude());
    }

}
