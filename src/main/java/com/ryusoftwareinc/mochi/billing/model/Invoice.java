package com.ryusoftwareinc.mochi.billing.model;

import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.core.model.AbstractEntity;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@Entity
public class Invoice extends AbstractEntity {

    @ManyToOne(optional = false)
    private Business business;

    @ElementCollection
    private List<BillableItem> items;

    private double subtotal;

    private double tax;

    private double total;

    @Column(nullable = false)
    private ZonedDateTime billPeriod;

    @Column(nullable = false)
    private ZonedDateTime dueDate;

    @OneToOne(cascade = CascadeType.ALL)
    private Payment payment;

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public List<BillableItem> getItems() {
        return items;
    }

    public void setItems(List<BillableItem> items) {
        this.items = items;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public ZonedDateTime getBillPeriod() {
        return billPeriod;
    }

    public void setBillPeriod(ZonedDateTime billPeriod) {
        this.billPeriod = billPeriod;
    }

    public ZonedDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(ZonedDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

}
