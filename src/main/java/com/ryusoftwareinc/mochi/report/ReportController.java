package com.ryusoftwareinc.mochi.report;

import com.ryusoftwareinc.mochi.core.controller.AbstractBaseController;
import com.ryusoftwareinc.mochi.core.http.HttpResponse;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/report/v1")
@RestController
@CrossOrigin
public class ReportController extends AbstractBaseController {

    private static final Logger log = Logger.getLogger(ReportController.class);

    @Autowired
    ReportService reportService;

    @PostMapping("emailReport")
    public HttpResponse emailReport(@RequestBody ReportRequestDto dto) {
        log.info("emailReport");
        reportService.emailReport(dto);
        return formResponse(null, "Report sent to email");
    }

}
