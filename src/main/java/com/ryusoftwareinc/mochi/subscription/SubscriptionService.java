package com.ryusoftwareinc.mochi.subscription;

import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.business.model.Trip;
import com.ryusoftwareinc.mochi.business.service.BusinessService;
import com.ryusoftwareinc.mochi.business.service.TagService;
import com.ryusoftwareinc.mochi.core.exception.ClientException;
import com.ryusoftwareinc.mochi.core.validator.ErrorCollector;
import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.employee.model.EmployeeType;
import com.ryusoftwareinc.mochi.employee.service.EmployeeService;
import com.ryusoftwareinc.mochi.subscription.audit.SubscriptionAudit;
import com.ryusoftwareinc.mochi.subscription.audit.SubscriptionAuditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubscriptionService {

    private static final String PLAN_Test = "Free";

    @Autowired
    BusinessService businessService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    SubscriptionConfigRepository subscriptionConfigRepository;

    @Autowired
    TagService tagService;

    @Autowired
    SubscriptionAuditRepository subscriptionAuditRepository;

    public SubscriptionConfig getTestPlan() {
        SubscriptionConfig subscriptionConfig = subscriptionConfigRepository.findById(PLAN_Test).get();
        return subscriptionConfig;
    }

    public void validateApiConfiguration(String businessKey) {
        Business business = businessService.findByKey(businessKey);
        SubscriptionConfig subscriptionConfig = business.getSubscription();
        if (!subscriptionConfig.isUseApi()) {
            throw new ClientException(subscriptionConfig.getName() + " plan does not support API");
        }
    }

    public void validateImportConfiguration(String businessKey) {
        Business business = businessService.findByKey(businessKey);
        SubscriptionConfig subscriptionConfig = business.getSubscription();
        if (!subscriptionConfig.isUseImport()) {
            throw new ClientException(subscriptionConfig.getName() + " plan does not support Import");
        }
    }

    public boolean shouldTripUseRouteOptimization(Trip trip) {
        Business business = trip.getEmployee().getBusiness();
        SubscriptionConfig subscriptionConfig = business.getSubscription();
        return subscriptionConfig.isUseRouteOptimization();
    }

    public void validateAddEmployee(Business business) {
        int empCount = employeeService.getRepo().countEnabledById(business.getId());
        SubscriptionConfig subscriptionConfig = business.getSubscription();
        if (empCount >= subscriptionConfig.getMaxEmployees()) {
            throw new ClientException(subscriptionConfig.getName() + " plan only supports " + subscriptionConfig.getMaxEmployees() + " employee(s)");
        }
    }

    public void validateAddTag(Business business) {
        int tagCount = tagService.getRepo().countEnabledById(business.getId());
        SubscriptionConfig subscriptionConfig = business.getSubscription();
        if (tagCount >= subscriptionConfig.getMaxTags()) {
            throw new ClientException(subscriptionConfig.getName() + " plan only supports " + subscriptionConfig.getMaxEmployees() + " tag(s)");
        }
    }

    public SubscriptionPlans getSubscriptionPlans(String businessKey) {
        Business business = businessService.findByKey(businessKey);
        SubscriptionConfig mySubscriptionConfig = business.getSubscription();
        SubscriptionPlans subscriptionPlans = new SubscriptionPlans();
        subscriptionPlans.setMySubscription(mySubscriptionConfig);
        subscriptionPlans.setSubscriptionList(subscriptionConfigRepository.findAllByOrderByPrice());
        return subscriptionPlans;
    }

    public void changeSubscriptionPlan(SubscriptionConfigDto dto) {
        Employee requester = employeeService.findByKey(dto.getRequester().getUserKey());
        if (requester.getType() != EmployeeType.OWNER) {
            throw new ClientException("Only owners can perform this action");
        }

        Business business = businessService.findByKey(dto.getBusinessKey());
        SubscriptionConfig changedSubscription = subscriptionConfigRepository.findById(dto.getName()).get();

        validateSubscriptionChange(business, changedSubscription);
        auditChange(business, changedSubscription);

        business.setSubscription(changedSubscription);
    }

    private void validateSubscriptionChange(Business business, SubscriptionConfig changedSubscription) {
        ErrorCollector ec = new ErrorCollector();
        final int empCount = employeeService.getRepo().countEnabledById(business.getId());
        if (empCount > changedSubscription.getMaxEmployees()) {
            ec.add("You have " + empCount + " employees but this plan only supports " + changedSubscription.getMaxEmployees());
        }
        final int tagCount = tagService.getRepo().countEnabledById(business.getId());
        if (tagCount > changedSubscription.getMaxTags()) {
            ec.add("You have " + tagCount + " tags but this plan only supports " + changedSubscription.getMaxTags());
        }
        ec.boom();
    }

    public void auditChange(Business business, SubscriptionConfig subscriptionConfig) {
        SubscriptionAudit audit = new SubscriptionAudit();
        audit.setBusiness(business);
        audit.setSubscriptionConfig(subscriptionConfig);
        subscriptionAuditRepository.save(audit);
    }

}
