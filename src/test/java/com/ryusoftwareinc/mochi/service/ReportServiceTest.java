package com.ryusoftwareinc.mochi.service;

import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.business.repository.BusinessRepository;
import com.ryusoftwareinc.mochi.core.exception.ServerException;
import com.ryusoftwareinc.mochi.report.ReportFactory;
import com.ryusoftwareinc.mochi.report.ReportType;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

@SpringBootTest
public class ReportServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    BusinessRepository businessRepository;

    @Autowired
    ReportFactory reportFactory;

    @Test
    public void testReport() {
        List<Business> businessList = businessRepository.findByEnabledTrue();
        if (businessList.isEmpty()) {
            return;
        }

        Business business = businessList.get(0);

        Workbook wb = reportFactory.generate(ReportType.DETAILED_ORDER, business, null, null);
        File dir = new File("/tmp/joseph");
        String path = dir.getAbsolutePath();
        String fileLocation = path + "/temp.xlsx";

        try (FileOutputStream outputStream = new FileOutputStream(fileLocation)) {
            wb.write(outputStream);
            wb.close();
        } catch (Exception ex) {
            throw new ServerException("Report generation error");
        }
    }

}
