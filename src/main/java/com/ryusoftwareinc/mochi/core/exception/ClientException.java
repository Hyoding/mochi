package com.ryusoftwareinc.mochi.core.exception;

public class ClientException extends BaseAppException {

	private static final long serialVersionUID = 6210185004086540140L;
	
	public ClientException(String msg) {
		super(msg);
	}

}
