package com.ryusoftwareinc.mochi.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.ryusoftwareinc.mochi.core.config.Config;
import com.ryusoftwareinc.mochi.core.service.HttpRequestService;
import com.ryusoftwareinc.mochi.location.mapbox.OptimizeRoute;

@SpringBootTest
public class HttpRequestServiceTest {

	@Autowired Config config;
	@Autowired HttpRequestService service;

	/*
	  https://api.mapbox.com/optimized-trips/v1/mapbox/driving/13.388860,52.517037;13.397634,52.529407;13.428555,52.523219;13.418555,52.523215
	  ?source=first&roundtrip=true
	  &access_token=pk.eyJ1Ijoiam9zZXBoLXl1IiwiYSI6ImNrZ29mMTI5YjBmNmYycm5xczlodGY1N3AifQ.vQwCHPb3G9C7QPjVrrgG4Q
		 */
	@Test
	public void getShouldWorkWhenNormal() {
		final String url = config.getMapboxUrlOptimization() + "/13.388860,52.517037;13.397634,52.529407;13.428555,52.523219;13.418555,52.523215";
		Map<String, String> params = new HashMap<>();
		params.put("source", "first");
		params.put("roundtrip", "true");
		params.put("access_token", config.getMapboxAccessToken());
		
		OptimizeRoute optRoute = service.get(url, params, OptimizeRoute.class);
		assertNotNull(optRoute);
		assertFalse(optRoute.getTrips().isEmpty());
	}
	
}
