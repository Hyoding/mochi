package com.ryusoftwareinc.mochi.core.validator;

import java.util.HashSet;
import java.util.Set;

public class ErrorCollector {
	
	private Set<String> errors;
	
	public ErrorCollector() {
		errors = new HashSet<>();
	}

	public Set<String> getErrors() {
		return errors;
	}

	public void setErrors(Set<String> errors) {
		this.errors = errors;
	}
	
	public boolean add(String msg) {
		return errors.add(msg);
	}
	
	public boolean add(Exception ex) {
		return errors.add(ex.getMessage());
	}
	
	public boolean isEmpty() {
		return errors.isEmpty();
	}
	
	public void boom() {
		if (!isEmpty()) {
			throw new ValidationException(String.join("\n", errors));
		}
	}

}
