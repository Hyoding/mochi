package com.ryusoftwareinc.mochi.core.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

// TODO: Think about getting some of these values from DB table
@Component
public class Config {
	
	@Value("${app.environment}")
	private Environment env;

	@Value("${mail.bcc_to_system}")
	private boolean mailBccToSystem;
	
	@Value("${spring.mail.username}")
	private String systemEmail;
	
	@Value("${spring.mail.password}")
	private String systemEmailPwd;
	
	@Value("${file.temp.path}")
	private String tempPath;
	
	@Value("${url.web.base}")
	private String urlWebBase;
	
	@Value("${url.web.track}")
	private String urlWebTrack;

	@Value("${url.web.resetPassword}")
	private String urlWebResetPassword;

	@Value("${url.web.importOrders}")
	private String urlWebImportOrders;

	@Value("${url.api.base}")
	private String urlApiBase;

	@Value("${url.api.business.validate}")
	private String urlApiValidateBusiness;

	@Value("${url.api.business.reject}")
	private String urlApiRejectBusiness;

	@Value("${mapbox.url.optimization}")
	private String mapboxUrlOptimization;

	@Value("${mapbox.url.geocoding}")
	private String mapboxUrlGeocoding;
	
	@Value("${mapbox.access_token}")
	private String mapboxAccessToken;

	@Value("${mapbox.style}")
	private String mapboxStyle;

	@Value("${mapbox.supported.countries}")
	private String mapboxSupportedCountries;

	@Value("${trip.max_numb_orders}")
	private Integer tripMaxNumbOrders;

	@Value("${update.LocationFrequency}")
	private Integer updateLocationFrequency;

	@Value("${update.LocationDistance}")
	private Double updateLocationDistance;

	@Value("${expiry.pwd.in_hour}")
	private Integer pwdExpiredInHour;

	@Value("${expiry.registration.in_hour}")
	private Integer registrationExpiredInHour;

	@Value("${sms.from}")
	private String smsFrom;

	@Value("${sms.twilio.account_sid}")
	private String smsTwilioAccountSid;

	@Value("${sms.twilio.auth_token}")
	private String smsTwilioAuthToken;

	@Value("${defaultETA}")
	private Integer defaultETA;
	public Environment getEnv() {
		return env;
	}

	public void setEnv(Environment env) {
		this.env = env;
	}

	public String getSystemEmail() {
		return systemEmail;
	}

	public void setSystemEmail(String emailFrom) {
		this.systemEmail = emailFrom;
	}

	public String getTempPath() {
		return tempPath;
	}

	public void setTempPath(String tempPath) {
		this.tempPath = tempPath;
	}

	public String getUrlWebBase() {
		return urlWebBase;
	}

	public void setUrlWebBase(String urlWebBase) {
		this.urlWebBase = urlWebBase;
	}

	public String getUrlWebTrack() {
		return urlWebBase + urlWebTrack;
	}

	public void setUrlWebTrack(String urlWebTrack) {
		this.urlWebTrack = urlWebTrack;
	}

	public String getUrlApiBase() {
		return urlApiBase;
	}

	public void setUrlApiBase(String urlApiBase) {
		this.urlApiBase = urlApiBase;
	}

	public String getUrlApiValidateBusiness() {
		return urlApiBase + urlApiValidateBusiness;
	}

	public void setUrlApiValidateBusiness(String urlApiValidateBusiness) {
		this.urlApiValidateBusiness = urlApiValidateBusiness;
	}

	public String getUrlApiRejectBusiness() {
		return urlApiBase + urlApiRejectBusiness;
	}

	public void setUrlApiRejectBusiness(String urlApiRejectBusiness) {
		this.urlApiRejectBusiness = urlApiRejectBusiness;
	}

	public String getSystemEmailPwd() {
		return systemEmailPwd;
	}

	public void setSystemEmailPwd(String systemEmailPwd) {
		this.systemEmailPwd = systemEmailPwd;
	}

	public String getMapboxUrlOptimization() {
		return mapboxUrlOptimization;
	}

	public void setMapboxUrlOptimization(String mapboxUrlOptimization) {
		this.mapboxUrlOptimization = mapboxUrlOptimization;
	}

	public String getMapboxAccessToken() {
		return mapboxAccessToken;
	}

	public void setMapboxAccessToken(String mapboxAccessToken) {
		this.mapboxAccessToken = mapboxAccessToken;
	}

	public Integer getUpdateLocationFrequency() {
		return updateLocationFrequency;
	}

	public void setUpdateLocationFrequency(Integer updateLocationFrequency) {
		this.updateLocationFrequency = updateLocationFrequency;
	}

	public Integer getPwdExpiredInHour() {
		return pwdExpiredInHour;
	}

	public void setPwdExpiredInHour(Integer pwdExpiredInHour) {
		this.pwdExpiredInHour = pwdExpiredInHour;
	}

	public void setUrlWebResetPassword(String urlWebResetPassword) {
		this.urlWebResetPassword = urlWebResetPassword;
	}

	public String getUrlWebResetPassword() {
		return urlWebBase + urlWebResetPassword;
	}

	public String getUrlWebImportOrders() {
		return urlWebBase + urlWebImportOrders;
	}

	public void setUrlWebImportOrders(String urlWebImportOrders) {
		this.urlWebImportOrders = urlWebImportOrders;
	}

	public String getSmsTwilioAccountSid() {
		return smsTwilioAccountSid;
	}

	public void setSmsTwilioAccountSid(String smsTwilioAccountSid) {
		this.smsTwilioAccountSid = smsTwilioAccountSid;
	}

	public String getSmsTwilioAuthToken() {
		return smsTwilioAuthToken;
	}

	public void setSmsTwilioAuthToken(String smsTwilioAuthToken) {
		this.smsTwilioAuthToken = smsTwilioAuthToken;
	}

	public String getSmsFrom() {
		return smsFrom;
	}

	public void setSmsFrom(String smsFrom) {
		this.smsFrom = smsFrom;
	}

	public boolean isMailBccToSystem() {
		return mailBccToSystem;
	}

	public void setMailBccToSystem(boolean mailBccToSystem) {
		this.mailBccToSystem = mailBccToSystem;
	}

	public String getMapboxStyle() {
		return mapboxStyle;
	}

	public void setMapboxStyle(String mapboxStyle) {
		this.mapboxStyle = mapboxStyle;
	}

	public Integer getTripMaxNumbOrders() {
		return tripMaxNumbOrders;
	}

	public void setTripMaxNumbOrders(Integer tripMaxNumbOrders) {
		this.tripMaxNumbOrders = tripMaxNumbOrders;
	}

	public String getMapboxUrlGeocoding() {
		return mapboxUrlGeocoding;
	}

	public void setMapboxUrlGeocoding(String mapboxUrlGeocoding) {
		this.mapboxUrlGeocoding = mapboxUrlGeocoding;
	}

	public String getMapboxSupportedCountries() {
		return mapboxSupportedCountries;
	}

	public void setMapboxSupportedCountries(String mapboxSupportedCountries) {
		this.mapboxSupportedCountries = mapboxSupportedCountries;
	}

	public Integer getDefaultETA() {
		return defaultETA;
	}

	public void setDefaultETA(Integer defaultETA) {
		this.defaultETA = defaultETA;
	}

	public Double getUpdateLocationDistance() {
		return updateLocationDistance;
	}

	public void setUpdateLocationDistance(Double updateLocationDistance) {
		this.updateLocationDistance = updateLocationDistance;
	}

	public Integer getRegistrationExpiredInHour() {
		return registrationExpiredInHour;
	}

	public void setRegistrationExpiredInHour(Integer registrationExpiredInHour) {
		this.registrationExpiredInHour = registrationExpiredInHour;
	}
}
