package com.ryusoftwareinc.mochi.business.model;

import com.ryusoftwareinc.mochi.core.model.AbstractEntity;
import com.ryusoftwareinc.mochi.core.model.Address;
import com.ryusoftwareinc.mochi.core.model.Coordinate;
import com.ryusoftwareinc.mochi.core.validator.ErrorCollector;
import com.ryusoftwareinc.mochi.employee.model.Employee;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.Set;

@Entity(name = "orders")
public class Order extends AbstractEntity {

    @OneToOne
    private Trip trip;
    @OneToOne
    private Employee createdBy;
    @OneToOne
    private Business business;
    @Embedded
    private Address address;
    @Enumerated(EnumType.STRING)
    private OrderStatus status;
    private String email;
    private String phoneNumber;
    private boolean sentSms;
    private String note;
    @ElementCollection
    private Set<String> tags;
    private ZonedDateTime deliverBy;
    private ZonedDateTime deliveredTime;
    // used for both CANCELED and REJECTED status
    private ZonedDateTime canceledTime;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="latitude", column=@Column(name="comp_loc_lat")),
            @AttributeOverride(name="longitude", column=@Column(name="comp_loc_lng"))
    })
    private Coordinate completedLocation;

    public Employee getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Employee createdBy) {
        this.createdBy = createdBy;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public ZonedDateTime getDeliverBy() {
        return deliverBy;
    }

    public void setDeliverBy(ZonedDateTime deliverBy) {
        this.deliverBy = deliverBy;
    }

    public ZonedDateTime getDeliveredTime() {
        return deliveredTime;
    }

    public void setDeliveredTime(ZonedDateTime deliveredTime) {
        this.deliveredTime = deliveredTime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public Coordinate getCompletedLocation() {
        return completedLocation;
    }

    public void setCompletedLocation(Coordinate completedLocation) {
        this.completedLocation = completedLocation;
    }

    public ZonedDateTime getCanceledTime() {
        return canceledTime;
    }

    public void setCanceledTime(ZonedDateTime canceledTime) {
        this.canceledTime = canceledTime;
    }

    public boolean isSentSms() {
        return sentSms;
    }

    public void setSentSms(boolean sentSms) {
        this.sentSms = sentSms;
    }

    @Override
    public void populateErrors(ErrorCollector ec) {
        if (deliverBy == null) {
            ec.add("Need to provide estimated delivery time");
        }
        if (status == OrderStatus.DELIVERED && deliveredTime == null) {
            ec.add("Need to provide delivered time if delivered");
        }
        if ((status == OrderStatus.CANCELED || status == OrderStatus.REJECTED) && canceledTime == null) {
            ec.add("Need to provided canceled time");
        }
        if (business == null) {
            ec.add("Business is required");
        }
        if (address == null) {
            ec.add("Address is required");
        } else {
            Coordinate coordinate = address.getCoordinate();
            if (address.getCoordinate() == null || coordinate.getLatitude() == null || coordinate.getLongitude() == null) {
                ec.add("Coordinate is required");
            }
        }
        if (email == null && phoneNumber == null) {
            ec.add("Either customer email or phone number must be provided");
        }
    }

    @Override
    protected void beforeSave() {
        this.email = email == null ? null : this.email.toLowerCase();
    }

    public static class OrderBuilder {

        private Employee createdEmp;
        private Business business;
        private ZonedDateTime deliverBy;
        private Address address;
        private String email;
        private String note;
        private String phoneNumber;
        private OrderStatus status;
        private Set<String> tags;

        public OrderBuilder setBusiness(Business business) {
            this.business = business;
            return this;
        }

        public OrderBuilder setDeliverBy(ZonedDateTime deliverBy) {
            this.deliverBy = deliverBy;
            return this;
        }

        public OrderBuilder setCreatedEmp(Employee emp) {
            this.createdEmp = emp;
            return this;
        }

        public OrderBuilder setAddress(Address address) {
            this.address = address;
            return this;
        }

        public OrderBuilder setEmail(String email) {
            this.email = email;
            return this;
        }

        public OrderBuilder setNote(String note) {
            this.note = note;
            return this;
        }

        public OrderBuilder setPhoneNumber(String number) {
            this.phoneNumber = number;
            return this;
        }

        public OrderBuilder setOrderStatus(OrderStatus status) {
            this.status = status;
            return this;
        }

        public OrderBuilder setTags(Set<String> tags) {
            this.tags = tags;
            return this;
        }

        public Order build() {
            Order order = new Order();
            order.setBusiness(business);
            order.setDeliverBy(deliverBy);
            order.setCreatedBy(createdEmp);
            order.setAddress(address);
            order.setEmail(email);
            order.setNote(note);
            order.setPhoneNumber(phoneNumber);
            order.setStatus(status);
            order.setTags(tags);
            order.boomIfNotValid();
            return order;
        }

    }

    public enum OrderStatus {
        CANCELED("Canceled"), // by customer
        REJECTED("Rejected"), // by business
        ACCEPTED("Accepted"),
        OUT_FOR_DELIVERY("Out for delivery"),
        DELIVERED("Delivered"),
        ;

        private String label;

        private OrderStatus(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }

}
