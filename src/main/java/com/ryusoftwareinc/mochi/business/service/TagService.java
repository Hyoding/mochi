package com.ryusoftwareinc.mochi.business.service;

import com.ryusoftwareinc.mochi.business.model.*;
import com.ryusoftwareinc.mochi.business.repository.TagRepository;
import com.ryusoftwareinc.mochi.core.exception.ClientException;
import com.ryusoftwareinc.mochi.core.service.AbstractEntityService;
import com.ryusoftwareinc.mochi.subscription.SubscriptionService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class TagService extends AbstractEntityService<Tag, TagRepository> {

	private final static Logger log = Logger.getLogger(TagService.class);

	@Autowired
	TagRepository repo;

	@Autowired
	SubscriptionService subscriptionService;

	@Override
	public TagRepository getRepo() {
		return repo;
	}

	@Override
	public Tag save(Tag tag) {
		Business business = tag.getBusiness();
		validateInsertOrUpdate(business, tag.getName());
		subscriptionService.validateAddTag(business);
		return super.save(tag);
	}

	@Override
	public void delete(Tag tag) {
		tag.setName(tag.getName() + "_deleted_" + System.currentTimeMillis());
		super.delete(tag);
	}

	public void validateInsertOrUpdate(Business business, String name) {
		if (repo.findByBusinessKeyAndName(business.getKey(), name) != null) {
			throw new ClientException("There is already a tag with this name");
		}
	}

	public Set<String> convertTagNamesToTagKeys(String businessKey, Set<String> tagNames) {
		if (tagNames == null) {
			return null;
		}
		Set<String> tagKeys = new HashSet<>();
		for (String tagName : tagNames) {
			Tag tag = getRepo().findByBusinessKeyAndName(businessKey, tagName);
			if (tag == null) {
				throw new ClientException("This tag doesn't exist: " + tagName);
			}
			tagKeys.add(tag.getKey());
		}
		return tagKeys;
	}

}
