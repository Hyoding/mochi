package com.ryusoftwareinc.mochi.customer;

import com.ryusoftwareinc.mochi.business.model.Order;
import com.ryusoftwareinc.mochi.core.model.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class CustomerSurvey extends AbstractEntity {

    @OneToOne
    private Order order;

    /*
        Quality of the product
        out of 5
     */
    private int productRating;

    /*
        Rating for courier
        out of 5
     */
    private int courierRating;

    private String comment;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public int getProductRating() {
        return productRating;
    }

    public void setProductRating(int productRating) {
        this.productRating = productRating;
    }

    public int getCourierRating() {
        return courierRating;
    }

    public void setCourierRating(int courierRating) {
        this.courierRating = courierRating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
