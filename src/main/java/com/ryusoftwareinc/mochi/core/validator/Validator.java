package com.ryusoftwareinc.mochi.core.validator;

public interface Validator {

	void populateErrors(ErrorCollector ec);
	
	default boolean isValid() {
		ErrorCollector ec = new ErrorCollector();
		populateErrors(ec);
		return ec.isEmpty();
	}
	
	default void boomIfNotValid() {
		ErrorCollector ec = new ErrorCollector();
		populateErrors(ec);
		ec.boom();
	}
	
}
