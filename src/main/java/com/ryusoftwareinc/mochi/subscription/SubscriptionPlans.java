package com.ryusoftwareinc.mochi.subscription;

import com.ryusoftwareinc.mochi.core.http.HttpResponse;

import java.util.List;

public class SubscriptionPlans extends HttpResponse {

    private SubscriptionConfig mySubscription;
    private List<SubscriptionConfig> subscriptionList;

    public SubscriptionConfig getMySubscription() {
        return mySubscription;
    }

    public void setMySubscription(SubscriptionConfig mySubscription) {
        this.mySubscription = mySubscription;
    }

    public List<SubscriptionConfig> getSubscriptionList() {
        return subscriptionList;
    }

    public void setSubscriptionList(List<SubscriptionConfig> subscriptionList) {
        this.subscriptionList = subscriptionList;
    }
}
