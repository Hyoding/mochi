package com.ryusoftwareinc.mochi.report;

import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.business.service.BusinessService;
import com.ryusoftwareinc.mochi.core.config.Config;
import com.ryusoftwareinc.mochi.core.exception.ClientException;
import com.ryusoftwareinc.mochi.core.exception.ServerException;
import com.ryusoftwareinc.mochi.core.notification.Email;
import com.ryusoftwareinc.mochi.core.notification.NotificationService;
import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.employee.service.EmployeeService;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileOutputStream;

@Service
@Transactional
public class ReportService {

    @Autowired
    ReportFactory reportFactory;
    @Autowired
    BusinessService businessService;
    @Autowired
    NotificationService notificationService;
    @Autowired
    EmployeeService employeeService;
    @Autowired
    ReportRequestRepository reportRequestRepository;
    @Autowired
    Config config;

    public void emailReport(ReportRequestDto dto) {
        String requesterKey = dto.getRequester().getUserKey();
        Employee requester = employeeService.findByKey(requesterKey);
        if (!requester.isEnabled()) {
            throw new ClientException("Your account has been disabled");
        }
        Business business = requester.getBusiness();
        if (!business.isEnabled()) {
            throw new ClientException("Your business is deactivated");
        }

        Workbook workbook = reportFactory.generate(dto.getReportType(), business, dto.getStart(), dto.getEnd());
        File file = generateReport(dto, workbook);
        Email email = generateReportEmail(dto, requester, file);
        notificationService.sendEmail(email);
        logRequest(dto, requester);
    }

    private void logRequest(ReportRequestDto dto, Employee requester) {
        ReportRequest reportRequest = new ReportRequest();
        reportRequest.setRequester(requester);
        reportRequest.setReportType(dto.getReportType());
        reportRequest.setDateFrom(dto.getStart());
        reportRequest.setDateTo(dto.getEnd());
        reportRequestRepository.save(reportRequest);
    }

    private File generateReport(ReportRequestDto dto, Workbook workbook) {
        String fileLocation = config.getTempPath() + "/" + dto.getReportType() + "_" + System.currentTimeMillis() +".xlsx";
        try (FileOutputStream outputStream = new FileOutputStream(fileLocation)) {
            workbook.write(outputStream);
            workbook.close();
        } catch (Exception ex) {
            throw new ServerException(ex.getMessage());
        }
        return new File(fileLocation);
    }

    private Email generateReportEmail(ReportRequestDto dto, Employee requester, File file) {
        Email email = new Email();
        email.setSubject(dto.getReportType().getLabel() + " generated");
        email.setFrom(config.getSystemEmail());
        email.addTo(requester.getEmail());

        StringBuilder sb = new StringBuilder();
        sb.append("Hi " + requester.getFirstName() + ",<br/><br/>");
        sb.append("Please review your requested report.<br/><br/>");
        sb.append("Thank you,<br/>");
        sb.append("Mochi Team");
        email.setHtmlText(sb.toString());
        email.addAttachment(file);

        return email;
    }


}
