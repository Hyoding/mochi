package com.ryusoftwareinc.mochi.business.model;

import com.ryusoftwareinc.mochi.core.model.Coordinate;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@Embeddable
public class OrderSequence {
	
	private Integer sequence;
	private String orderKey;
	@Embedded
	private Coordinate coordinate;
	private Double distance;
	private Integer duration;
	
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	public String getOrderKey() {
		return orderKey;
	}
	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}
	public Coordinate getCoordinate() {
		return coordinate;
	}
	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}
}
