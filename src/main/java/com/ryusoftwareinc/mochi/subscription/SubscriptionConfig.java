package com.ryusoftwareinc.mochi.subscription;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SubscriptionConfig {

    @Id
    private String name;
    private double price;
    private boolean useApi;
    private boolean useRouteOptimization;
    private boolean useShopify;
    private boolean useImport;
    private boolean useReport;
    private int maxEmployees;
    private int maxTags;

    // orders related
    private int incOrders;
    private boolean canHaveAddlOrders;
    private double addlOrderCost;

    // sms related
    private boolean canSendSms;
    private double smsCost;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isUseApi() {
        return useApi;
    }

    public void setUseApi(boolean useApi) {
        this.useApi = useApi;
    }

    public boolean isUseRouteOptimization() {
        return useRouteOptimization;
    }

    public void setUseRouteOptimization(boolean useRouteOptimization) {
        this.useRouteOptimization = useRouteOptimization;
    }

    public boolean isUseShopify() {
        return useShopify;
    }

    public void setUseShopify(boolean useShopify) {
        this.useShopify = useShopify;
    }

    public boolean isUseImport() {
        return useImport;
    }

    public void setUseImport(boolean useImport) {
        this.useImport = useImport;
    }

    public boolean isUseReport() {
        return useReport;
    }

    public void setUseReport(boolean useReport) {
        this.useReport = useReport;
    }

    public int getMaxEmployees() {
        return maxEmployees;
    }

    public void setMaxEmployees(int maxEmployees) {
        this.maxEmployees = maxEmployees;
    }

    public int getMaxTags() {
        return maxTags;
    }

    public void setMaxTags(int maxTags) {
        this.maxTags = maxTags;
    }

    public int getIncOrders() {
        return incOrders;
    }

    public void setIncOrders(int incOrders) {
        this.incOrders = incOrders;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isCanHaveAddlOrders() {
        return canHaveAddlOrders;
    }

    public void setCanHaveAddlOrders(boolean canHaveAddlOrders) {
        this.canHaveAddlOrders = canHaveAddlOrders;
    }

    public boolean isCanSendSms() {
        return canSendSms;
    }

    public void setCanSendSms(boolean canSendSms) {
        this.canSendSms = canSendSms;
    }

    public double getAddlOrderCost() {
        return addlOrderCost;
    }

    public void setAddlOrderCost(double addlOrderCost) {
        this.addlOrderCost = addlOrderCost;
    }

    public double getSmsCost() {
        return smsCost;
    }

    public void setSmsCost(double smsCost) {
        this.smsCost = smsCost;
    }
}
