package com.ryusoftwareinc.mochi.report;

import com.ryusoftwareinc.mochi.core.http.BaseHttpRequest;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.ZonedDateTime;

public class ReportRequestDto extends BaseHttpRequest {

    private ReportType reportType;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime start;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime end;

    public ReportType getReportType() {
        return reportType;
    }

    public void setReportType(ReportType reportType) {
        this.reportType = reportType;
    }

    public ZonedDateTime getStart() {
        return start;
    }

    public void setStart(ZonedDateTime start) {
        this.start = start;
    }

    public ZonedDateTime getEnd() {
        return end;
    }

    public void setEnd(ZonedDateTime end) {
        this.end = end;
    }
}
