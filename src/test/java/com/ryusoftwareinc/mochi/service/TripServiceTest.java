package com.ryusoftwareinc.mochi.service;

import com.ryusoftwareinc.mochi.EntityFactory;
import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.business.model.Order;
import com.ryusoftwareinc.mochi.business.model.Order.OrderStatus;
import com.ryusoftwareinc.mochi.business.model.OrderSequence;
import com.ryusoftwareinc.mochi.business.model.Trip;
import com.ryusoftwareinc.mochi.business.repository.BusinessRepository;
import com.ryusoftwareinc.mochi.business.repository.OrderRepository;
import com.ryusoftwareinc.mochi.business.service.TripService;
import com.ryusoftwareinc.mochi.core.model.Address;
import com.ryusoftwareinc.mochi.core.model.Coordinate;
import com.ryusoftwareinc.mochi.core.util.DateTimeUtil;
import com.ryusoftwareinc.mochi.core.validator.ValidationException;
import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.employee.repository.EmployeeRepository;
import com.ryusoftwareinc.mochi.location.mapbox.Waypoint;
import com.ryusoftwareinc.mochi.subscription.SubscriptionConfig;
import com.ryusoftwareinc.mochi.subscription.SubscriptionConfigRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class TripServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired TripService service;
	@Autowired OrderRepository orderRepo;
	@Autowired EntityFactory entityFactory;
	@Autowired EmployeeRepository empRepo;
	@Autowired BusinessRepository busRepo;
	@Autowired SubscriptionConfigRepository subscriptionConfigRepository;

	@Test
	public void startTripShouldFailIfNoEmployeeOrOrdersProvided() {
		SubscriptionConfig subscriptionConfig = new SubscriptionConfig();
		subscriptionConfig.setName("GG");
		subscriptionConfig = subscriptionConfigRepository.save(subscriptionConfig);

	    Business business = entityFactory.generateBusiness();
	    business.setSubscription(subscriptionConfig);
        business = busRepo.save(business);
	    Employee employee = entityFactory.generateEmployee();
	    employee.setBusiness(business);
	    employee = empRepo.save(employee);
		Set<Order> orders = new HashSet<>();
		try {
			service.startTrip(null, orders, null);
			fail();
		} catch (Exception ex) {}

		try {
			service.startTrip(employee, null, null);
			fail();
		} catch (ValidationException ex) {}

	}

	@Test
	public void startTripShouldWorkWhenNormal() {
		SubscriptionConfig subscriptionConfig = new SubscriptionConfig();
		subscriptionConfig.setUseRouteOptimization(true);
		subscriptionConfig.setName("GG");
		subscriptionConfig = subscriptionConfigRepository.save(subscriptionConfig);

		Set<Order> orders = new HashSet<>();
		Order order1 = entityFactory.generateOrder();
		Business business = order1.getBusiness();
		business.setSubscription(subscriptionConfig);
		busRepo.save(business);
		orderRepo.save(order1);
		orders.add(order1);
		Order order2 = entityFactory.generateOrder();
		order2.setBusiness(business);
		orderRepo.save(order2);
		orders.add(order2);

		Employee employee = entityFactory.generateEmployee();
		employee.setBusiness(business);
		empRepo.save(employee);
		Coordinate coordinate = new Coordinate();
		coordinate.setLatitude(44.648061);
		coordinate.setLongitude(-63.590997);
		Trip trip = service.startTrip(employee, orders, coordinate);
		assertNotNull(trip.getEmployee());
		assertNotNull(trip.getOrders());
		assertFalse(trip.getOrders().isEmpty());
		assertFalse(trip.getOrderSequence().isEmpty());
		assertTrue(trip.getDistance() > 0);
		assertTrue(trip.getDuration() > 0);

		order1 = orderRepo.findByKey(order1.getKey());
		assertTrue(order1.getStatus() == OrderStatus.OUT_FOR_DELIVERY);
	}

	@Test
	public void startTripShouldFailIfAnOrderAlreadyTaken() {
		SubscriptionConfig subscriptionConfig = new SubscriptionConfig();
		subscriptionConfig.setName("GG");
		subscriptionConfig = subscriptionConfigRepository.save(subscriptionConfig);

		Coordinate coordinate = new Coordinate();
		coordinate.setLatitude(44.642133);
		coordinate.setLongitude(-63.575931);
		Set<Order> orders = new HashSet<>();
		Order order1 = entityFactory.generateOrder();
		Business business = order1.getBusiness();
		business.setSubscription(subscriptionConfig);
		busRepo.save(business);
		orderRepo.save(order1);
		orders.add(order1);

		Order order2 = entityFactory.generateOrder();
		order2.setStatus(OrderStatus.OUT_FOR_DELIVERY);
		order2.setBusiness(business);
		orderRepo.save(order2);
		orders.add(order2);

		Employee employee = entityFactory.generateEmployee();
		employee.setBusiness(business);
		empRepo.save(employee);
		try {
			service.startTrip(employee, orders, coordinate);
			fail();
		} catch (ValidationException ex) {}

		order2.setStatus(OrderStatus.DELIVERED);
		try {
			service.startTrip(employee, orders, coordinate);
			fail();
		} catch (ValidationException ex) {}

		assertTrue(order1.getStatus() != OrderStatus.OUT_FOR_DELIVERY);
	}

	@Test
	public void populateOptimizedRouteShouldWorkWhenNormal() {
		Employee employee = entityFactory.generateEmployee();
		employee = empRepo.save(employee);
		Set<Order> orders = new HashSet<>();

		SubscriptionConfig subscriptionConfig = new SubscriptionConfig();
		subscriptionConfig.setName("GG");
		subscriptionConfig = subscriptionConfigRepository.save(subscriptionConfig);

		Business business = entityFactory.generateBusiness();
		business.setSubscription(subscriptionConfig);
		business = busRepo.save(business);
		employee.setBusiness(business);

		Order order1 = new Order();
		Address address1 = new Address();
		Coordinate coord1 = new Coordinate(44.642343, -63.575865);
		address1.setCoordinate(coord1);
		order1.setAddress(address1);
		order1.setDeliverBy(DateTimeUtil.now());
		orders.add(order1);

		Order order2 = new Order();
		Address address2 = new Address();
		Coordinate coord2 = new Coordinate(44.643991, -63.588780);
		address2.setCoordinate(coord2);
		order2.setAddress(address2);
		order2.setDeliverBy(DateTimeUtil.now().plusHours(1));
		orders.add(order2);

		Trip trip = new Trip();
		trip.setEmployee(employee);
		trip.setOrders(orders);
		Coordinate startLocation = new Coordinate(44.646662, -63.592985);
		assertTrue(trip.getOrderSequence().isEmpty());
		assertFalse(trip.getOrders().isEmpty());
		service.populateOptimizedRouteDurationAndDistance(trip, startLocation);
		List<OrderSequence> orderSeq = trip.getOrderSequence();
		assertNotNull(orderSeq);
		assertEquals(orderSeq.size(), trip.getOrders().size());
	}

	@Test
	public void guessOrderShouldGuessCorrectly() {
		Set<Order> orders = new HashSet<>();
		Order order1 = new Order();
		Address add1 = new Address();
		add1.setCoordinate(new Coordinate(44.652442, -63.591079));
		order1.setAddress(add1);
		orders.add(order1);

		Order order2 = new Order();
		Address add2 = new Address();
		add2.setCoordinate(new Coordinate(44.6491657, -63.5776379));
		order2.setAddress(add2);
		orders.add(order2);

		Order order3 = new Order();
		Address add3 = new Address();
		add3.setCoordinate(new Coordinate(44.6421329, -63.5759307));
		order3.setAddress(add3);
		orders.add(order3);

		Order order4 = new Order();
		Address add4 = new Address();
		add4.setCoordinate(new Coordinate(44.6421329, -63.5759307));
		order4.setAddress(add4);
		orders.add(order4);

		Order order5 = new Order();
		Address add5 = new Address();
		add5.setCoordinate(new Coordinate(44.65503, -63.60687));
		order5.setAddress(add5);
		orders.add(order5);

		List<Waypoint> waypoints = new ArrayList<>();
		Waypoint wp1 = new Waypoint();
		wp1.setWaypoint_index(1);
		wp1.setLocation(new ArrayList<>() {{add("-63.591079"); add("44.652442");}});
		waypoints.add(wp1);

		Waypoint wp2 = new Waypoint();
		wp2.setWaypoint_index(2);
		wp2.setLocation(new ArrayList<>() {{add("-63.577823"); add("44.649157");}});
		waypoints.add(wp2);

		Waypoint wp3 = new Waypoint();
		wp3.setWaypoint_index(3);
		wp3.setLocation(new ArrayList<>() {{add("-63.575856"); add("44.641993");}});
		waypoints.add(wp3);

		Waypoint wp4 = new Waypoint();
		wp4.setWaypoint_index(4);
		wp4.setLocation(new ArrayList<>() {{add("-63.575856"); add("44.641993");}});
		waypoints.add(wp4);

		Waypoint wp5 = new Waypoint();
		wp5.setWaypoint_index(5);
		wp5.setLocation(new ArrayList<>() {{add("-63.607097"); add("44.655196");}});
		waypoints.add(wp5);

		List<OrderSequence> orderSequence = service.guessOrderSequence(orders, waypoints);
		assertTrue(orderSequence.size() == 5);
		Set<String> nonDuplicateOrderKey = new HashSet<>();
		for (OrderSequence os : orderSequence) {
			nonDuplicateOrderKey.add(os.getOrderKey());
			assertTrue(os.getSequence() != null);
			assertTrue(os.getOrderKey() != null);
			assertTrue(os.getCoordinate() != null);
		}
		assertEquals(orderSequence.size(), nonDuplicateOrderKey.size());
	}

}
