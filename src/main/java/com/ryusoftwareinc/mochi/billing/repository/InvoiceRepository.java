package com.ryusoftwareinc.mochi.billing.repository;

import com.ryusoftwareinc.mochi.billing.model.Invoice;
import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.core.repository.EntityRepository;

import java.time.ZonedDateTime;
import java.util.List;

public interface InvoiceRepository extends EntityRepository<Invoice> {

    Invoice findByBusinessAndBillPeriod(Business business, ZonedDateTime yearMonth);

    List<Invoice> findAllByPaymentNull();

    Invoice findByBusinessAndPaymentNull(Business business);

}
