package com.ryusoftwareinc.mochi.business.repository;

import com.ryusoftwareinc.mochi.business.model.Tag;
import com.ryusoftwareinc.mochi.core.repository.EntityRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TagRepository extends EntityRepository<Tag> {

	@Query(value = "SELECT * FROM TAG WHERE business_id = (select id from business where key = ?) and name = ?", nativeQuery = true)
	Tag findByBusinessKeyAndName(String businessKey, String name);

	@Query(value = "SELECT * FROM TAG WHERE business_id = (select id from business where key = ?) and enabled = true order by name", nativeQuery = true)
	List<Tag> findEnabledByBusinessKey(String businessKey);

	@Query(value = "select count(*) from TAG where business_id = ? and enabled = true", nativeQuery = true)
	int countEnabledById(Long id);

}
