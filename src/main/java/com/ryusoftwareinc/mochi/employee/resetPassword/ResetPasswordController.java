package com.ryusoftwareinc.mochi.employee.resetPassword;

import com.ryusoftwareinc.mochi.core.controller.AbstractBaseController;
import com.ryusoftwareinc.mochi.core.http.HttpResponse;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/resetPassword/v1")
@RestController
@CrossOrigin
public class ResetPasswordController extends AbstractBaseController {

    private static final Logger log = Logger.getLogger(ResetPasswordController.class);

    @Autowired ResetPasswordService service;

    @PostMapping("employee")
    public HttpResponse reset(@RequestParam String email) {
        log.info("reset employee email " + email);
        service.sendResetPasswordEmail(email);
        return formResponse(null, "Instruction sent to this email!");
    }

}
