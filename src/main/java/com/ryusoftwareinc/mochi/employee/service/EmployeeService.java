package com.ryusoftwareinc.mochi.employee.service;

import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.core.config.Config;
import com.ryusoftwareinc.mochi.core.notification.Email;
import com.ryusoftwareinc.mochi.core.notification.NotificationService;
import com.ryusoftwareinc.mochi.core.service.AbstractEntityService;
import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.employee.repository.EmployeeRepository;
import com.ryusoftwareinc.mochi.employee.resetPassword.ResetPassword;
import com.ryusoftwareinc.mochi.employee.resetPassword.ResetPasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EmployeeService extends AbstractEntityService<Employee, EmployeeRepository> {
	
	@Autowired
	EmployeeRepository repo;

	@Autowired
	NotificationService notificationService;

	@Autowired
	ResetPasswordService resetPasswordService;

	@Autowired
	Config config;

	@Override
	public EmployeeRepository getRepo() {
		return repo;
	}

	@Override
	public void delete(Employee employee) {
		employee.setEmail(employee.getEmail() + "_deleted_" + System.currentTimeMillis());
		super.delete(employee);
	}

	public void sendNewEmployeeEmail(Employee employee) {
		ResetPassword resetPassword = new ResetPassword();
		resetPassword.setEmployee(employee);
		resetPasswordService.save(resetPassword);

		Business business = employee.getBusiness();
		Email email = new Email();
		email.setSubject(business.getName() + " added you as an employee");
		email.setFrom(config.getSystemEmail());
		email.addTo(employee.getEmail());
		email.addBcc(config.getSystemEmail());

		StringBuilder sb = new StringBuilder();
		sb.append("Hi " + employee.getFirstName() + ",<br/><br/>");
		sb.append("You were added as an employee<br/>");
		sb.append("Role: " + employee.getType().getLabel() + "<br/>");
		sb.append("First name: " + employee.getFirstName() + "<br/>");
		sb.append("Last name: " + employee.getLastName() + "<br/>");
		sb.append("Phone: " + employee.getPhoneNumber() + "<br/><br/>");
		sb.append("If any information is incorrect, please let your manager know<br/><br/>");
		sb.append("Click " + notificationService.htmlLink(config.getUrlWebResetPassword() + "/" + resetPassword.getKey(), "here") + " to reset your password<br/><br/>");
		sb.append("This will expire in " + config.getRegistrationExpiredInHour() + " hour(s)<br/><br/>");
		sb.append("Thank you,<br/>");
		sb.append("Mochi Team");
		email.setHtmlText(sb.toString());
		notificationService.sendEmail(email);
	}

}
