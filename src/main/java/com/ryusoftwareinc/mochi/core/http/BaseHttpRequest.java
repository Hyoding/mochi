package com.ryusoftwareinc.mochi.core.http;

import com.ryusoftwareinc.mochi.core.model.Jsonify;

public class BaseHttpRequest implements Jsonify {

	private String businessKey;
	private HttpRequester requester;

	public HttpRequester getRequester() {
		return requester;
	}

	public void setRequester(HttpRequester requester) {
		this.requester = requester;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}

	@Override
	public String toString() {
		return toJSON();
	}

}
