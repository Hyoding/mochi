package com.ryusoftwareinc.mochi.business.model;

import com.ryusoftwareinc.mochi.core.http.BaseHttpRequest;
import com.ryusoftwareinc.mochi.core.model.Coordinate;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TripDto extends BaseHttpRequest {

	private String key;
	private String employeeKey; // in
	private String employeeInitial;
	private String employeePhone;
	private Set<String> orderKeys; // in
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private ZonedDateTime tripEndTime;
	private Coordinate startLocation;
	private Coordinate location; // in
	private List<OrderSequence> orderSequence;
	private double duration;
	private double distance;
	private Set<OrderDto> orders;

	// misc
	private String orderKey;
	
	public TripDto() {}
	
	public TripDto(Trip trip) {
		this.key = trip.getKey();
		this.employeeKey = trip.getEmployee().getKey();
		this.employeeInitial = trip.getEmployee().getFirstName().substring(0, 1) + trip.getEmployee().getLastName().substring(0, 1);
		this.employeePhone = trip.getEmployee().getPhoneNumber();
		this.tripEndTime = trip.getTripEndTime();
		this.location = trip.getLocation();
		this.orderSequence = trip.getOrderSequence().stream().sorted((os1, os2) -> os1.getSequence().compareTo(os2.getSequence())).collect(Collectors.toList());
		this.duration = trip.getDuration();
		this.distance = trip.getDistance();
		this.orders = trip.getOrders().stream().map(o -> new OrderDto(o)).collect(Collectors.toSet());
	}
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getEmployeeKey() {
		return employeeKey;
	}
	public void setEmployeeKey(String employeeKey) {
		this.employeeKey = employeeKey;
	}
	public Set<String> getOrderKeys() {
		return orderKeys;
	}
	public void setOrderKeys(Set<String> orderKeys) {
		this.orderKeys = orderKeys;
	}
	public ZonedDateTime getTripEndTime() {
		return tripEndTime;
	}
	public void setTripEndTime(ZonedDateTime tripEndTime) {
		this.tripEndTime = tripEndTime;
	}
	public Coordinate getLocation() {
		return location;
	}
	public void setLocation(Coordinate location) {
		this.location = location;
	}
	public List<OrderSequence> getOrderSequence() {
		return orderSequence;
	}
	public void setOrderSequence(List<OrderSequence> orderSequence) {
		this.orderSequence = orderSequence;
	}
	public double getDuration() {
		return duration;
	}
	public void setDuration(double duration) {
		this.duration = duration;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	public Set<OrderDto> getOrders() {
		return orders;
	}
	public void setOrders(Set<OrderDto> orders) {
		this.orders = orders;
	}
	public Coordinate getStartLocation() {
		return startLocation;
	}
	public void setStartLocation(Coordinate startLocation) {
		this.startLocation = startLocation;
	}

	public String getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}

	public String getEmployeeInitial() {
		return employeeInitial;
	}

	public void setEmployeeInitial(String employeeInitial) {
		this.employeeInitial = employeeInitial;
	}

	public String getEmployeePhone() {
		return employeePhone;
	}

	public void setEmployeePhone(String employeePhone) {
		this.employeePhone = employeePhone;
	}
}
