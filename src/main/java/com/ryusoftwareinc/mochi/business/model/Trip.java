package com.ryusoftwareinc.mochi.business.model;

import com.ryusoftwareinc.mochi.core.model.AbstractEntity;
import com.ryusoftwareinc.mochi.core.model.Coordinate;
import com.ryusoftwareinc.mochi.core.validator.ErrorCollector;
import com.ryusoftwareinc.mochi.employee.model.Employee;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Trip extends AbstractEntity {

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "trip_id")
	private Set<Order> orders;
	private ZonedDateTime tripEndTime;
	@OneToOne
	private Employee employee;
	@Embedded
	private Coordinate location;
	@ElementCollection
	private List<OrderSequence> orderSequence;
	private double duration; // min
	private double distance; // km
	
	public Trip() {
		this.orders = new HashSet<>();
		this.orderSequence = new ArrayList<>();
	}

	public Set<Order> getOrders() {
		return orders;
	}

	public void setOrders(Set<Order> orders) {
		this.orders = orders;
	}
	
	public boolean addOrder(Order order) {
		return orders.add(order);
	}

	public ZonedDateTime getTripEndTime() {
		return tripEndTime;
	}

	public void setTripEndTime(ZonedDateTime tripEndTime) {
		this.tripEndTime = tripEndTime;
	}

	public Coordinate getLocation() {
		return location;
	}

	public void setLocation(Coordinate location) {
		this.location = location;
	}
	
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<OrderSequence> getOrderSequence() {
		return orderSequence;
	}

	public void setOrderSequence(List<OrderSequence> orderSequence) {
		this.orderSequence = orderSequence;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	@Override
	public void populateErrors(ErrorCollector ec) {
		if (employee == null) {
			ec.add("Trip needs an employee");
		}
	}
	
}
