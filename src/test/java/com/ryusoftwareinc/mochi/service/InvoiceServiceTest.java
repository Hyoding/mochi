package com.ryusoftwareinc.mochi.service;

import com.ryusoftwareinc.mochi.EntityFactory;
import com.ryusoftwareinc.mochi.billing.model.BillableItem;
import com.ryusoftwareinc.mochi.billing.model.Invoice;
import com.ryusoftwareinc.mochi.billing.service.InvoiceService;
import com.ryusoftwareinc.mochi.business.model.Business;
import com.ryusoftwareinc.mochi.business.model.ManualOrder;
import com.ryusoftwareinc.mochi.business.service.BusinessService;
import com.ryusoftwareinc.mochi.core.exception.ConcerningClientException;
import com.ryusoftwareinc.mochi.core.util.DateTimeUtil;
import com.ryusoftwareinc.mochi.employee.model.Employee;
import com.ryusoftwareinc.mochi.employee.service.EmployeeService;
import com.ryusoftwareinc.mochi.schedule.ScheduledJobs;
import com.ryusoftwareinc.mochi.subscription.SubscriptionConfig;
import com.ryusoftwareinc.mochi.subscription.SubscriptionConfigRepository;
import com.ryusoftwareinc.mochi.subscription.SubscriptionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.time.YearMonth;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.ryusoftwareinc.mochi.billing.service.InvoiceService.DUE_DAY_OF_MONTH;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class InvoiceServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	EntityFactory entityFactory;

	@Autowired
	BusinessService businessService;

	@Autowired
	EmployeeService employeeService;

	@Autowired
	InvoiceService invoiceService;

	@Autowired
	SubscriptionConfigRepository subscriptionConfigRepository;

	@Autowired
	SubscriptionService subscriptionService;

	@Autowired
	ScheduledJobs scheduledJobs;

	private Business getBusiness() {
		SubscriptionConfig subscriptionConfig = new SubscriptionConfig();
		subscriptionConfig.setName("Paid plan");
		subscriptionConfig.setPrice(39.99);
		subscriptionConfig.setUseApi(true);
		subscriptionConfig.setUseRouteOptimization(true);
		subscriptionConfig.setUseShopify(true);
		subscriptionConfig.setUseImport(true);
		subscriptionConfig.setUseReport(true);
		subscriptionConfig.setMaxEmployees(100);
		subscriptionConfig.setMaxTags(100);
		subscriptionConfig.setIncOrders(10000);
		subscriptionConfig.setCanHaveAddlOrders(true);
		subscriptionConfig.setAddlOrderCost(0.10);
		subscriptionConfig.setCanSendSms(true);
		subscriptionConfig.setSmsCost(0.02);
		subscriptionConfig = subscriptionConfigRepository.save(subscriptionConfig);

		Business business = entityFactory.generateBusiness();
		business.setSubscription(subscriptionConfig);
		return businessService.save(business);
	}

	private Business getBusinessWithFreePlan() {
		SubscriptionConfig subscriptionConfig = subscriptionService.getTestPlan();
		Business business = entityFactory.generateBusiness();
		business.setSubscription(subscriptionConfig);
		return businessService.save(business);
	}

	@Test
	public void generateInvoiceShouldCreateNewIfNoneMadeYet() {
		final long countBefore = invoiceService.count();
		Business business = getBusiness();
		Invoice invoice = invoiceService.generateInvoice(business);
		assertNotEquals(null, invoice.getId());

		final long countAfter = invoiceService.count();
		assertEquals(countBefore + 1, countAfter);
	}

	@Test
	public void generateInvoiceShouldNotCreateNewIfAlreadyMadeThisMonth() {
		final long countBefore = invoiceService.count();
		Business business = getBusiness();
		invoiceService.generateInvoice(business);
		invoiceService.generateInvoice(business);
		invoiceService.generateInvoice(business);

		final long countAfter = invoiceService.count();
		assertEquals(countBefore + 1, countAfter);
	}

	@Test
	public void generateInvoiceShouldHaveSubscriptionBillableMinimum() {
		Business business = getBusinessWithFreePlan();
		Invoice invoice = invoiceService.generateInvoice(business);
		List<BillableItem> items = invoice.getItems();
		assertEquals(1, items.size());

		BillableItem item = items.get(0);
		assertEquals(BillableItem.Billable.SUBSCRIPTION, item.getBillable());
		assertEquals(1, item.getCount());

		SubscriptionConfig subscription = business.getSubscription();
		assertEquals(subscription.getPrice(), item.getPrice());

		assertTrue(invoice.getSubtotal() <= invoice.getTotal());
		assertEquals(invoice.getTotal(), item.getPrice() * (1 + invoice.getTax()));
	}

	@Test
	public void generateInvoiceShouldHaveCorrectDueDateAndPaidDate() {
		Business business = getBusiness();
		business.setCreatedDate(business.getCreatedDate().minusMonths(1));
		Invoice invoice = invoiceService.generateInvoice(business);
		assertEquals(null, invoice.getPayment());

		ZonedDateTime now = DateTimeUtil.now();
		ZonedDateTime dueDate = invoice.getDueDate();
		assertEquals(now.getYear(), dueDate.getYear());
		assertEquals(now. getMonthValue(), dueDate.getMonthValue());
		assertEquals(DUE_DAY_OF_MONTH, dueDate.getDayOfMonth());

		YearMonth yearMonthNow = YearMonth.now();
		ZonedDateTime billPeriod = invoice.getBillPeriod();
		assertEquals(now.getYear(), billPeriod.getYear());
		assertEquals(now.getMonthValue(), billPeriod.getMonthValue());
		ZonedDateTime expectedDueDate = ZonedDateTime.of(yearMonthNow.getYear(), yearMonthNow.getMonthValue(), DUE_DAY_OF_MONTH, 0, 0, 0, 0, DateTimeUtil.ZONE_ID);
		assertEquals(expectedDueDate, invoice.getDueDate());

		// try getting previous one
		YearMonth previousMonth = YearMonth.now().minusMonths(1);
		invoice = invoiceService.generateInvoice(business, previousMonth);
		assertEquals(DateTimeUtil.convertToDateTime(previousMonth), invoice.getBillPeriod());

		expectedDueDate = ZonedDateTime.of(yearMonthNow.getYear(), yearMonthNow.getMonthValue(), DUE_DAY_OF_MONTH, 0, 0, 0, 0, DateTimeUtil.ZONE_ID);
		assertEquals(expectedDueDate, invoice.getDueDate());
	}

	@Test
	public void generateInvoiceOfPreviousMonthShouldGenerateNullIfBusinessCreatedThisMonth() {
		YearMonth previousMonth = YearMonth.now().minusMonths(1);
		Business business = getBusiness();
		Invoice invoice = invoiceService.generateInvoice(business, previousMonth);
		assertNull(invoice);

		YearMonth previous2Month = YearMonth.now().minusMonths(2);
		invoice = invoiceService.generateInvoice(business, previous2Month);
		assertNull(invoice);
	}

	@Test
	public void generateInvoiceShouldBillSMSCorrectly() {
		Business business = getBusiness();
		Employee employee = entityFactory.generateEmployee();
		employee.setBusiness(business);
		employee = employeeService.save(employee);
		ZonedDateTime edt = DateTimeUtil.now().plusHours(2);

		// create a few orders and test
		ManualOrder orderRequest1 = new ManualOrder();
		orderRequest1.setBusinessKey(business.getKey());
		orderRequest1.setEmployeeKey(employee.getKey());
		orderRequest1.setContactNumber("1231231234");
		orderRequest1.setEmail(null);
		orderRequest1.setAddress(entityFactory.generateAddress());
		orderRequest1.setEstimatedDeliveryTime(edt);
		businessService.createManualOrder(orderRequest1);

		ManualOrder orderRequest2 = new ManualOrder();
		orderRequest2.setBusinessKey(business.getKey());
		orderRequest2.setEmployeeKey(employee.getKey());
		orderRequest2.setContactNumber("1231231234");
		orderRequest2.setEmail(null);
		orderRequest2.setAddress(entityFactory.generateAddress());
		orderRequest2.setEstimatedDeliveryTime(edt);
		businessService.createManualOrder(orderRequest2);

		ManualOrder orderRequest3 = new ManualOrder();
		orderRequest3.setBusinessKey(business.getKey());
		orderRequest3.setEmployeeKey(employee.getKey());
		orderRequest3.setContactNumber(null);
		orderRequest3.setEmail("emailProvided");
		orderRequest3.setAddress(entityFactory.generateAddress());
		orderRequest3.setEstimatedDeliveryTime(edt);
		businessService.createManualOrder(orderRequest3);

		// if configured to not use SMS then sms shd not be sent
		business.getSubscription().setCanSendSms(false);
		ManualOrder orderRequest4 = new ManualOrder();
		orderRequest4.setBusinessKey(business.getKey());
		orderRequest4.setEmployeeKey(employee.getKey());
		orderRequest4.setContactNumber("1231231234");
		orderRequest4.setEmail(null);
		orderRequest4.setAddress(entityFactory.generateAddress());
		orderRequest4.setEstimatedDeliveryTime(edt);
		businessService.createManualOrder(orderRequest2);

		Invoice invoice = invoiceService.generateInvoice(business);
		List<BillableItem> items = invoice.getItems();
		assertEquals(2, items.size());

		BillableItem smsBill = items.stream().filter(i -> i.getBillable() == BillableItem.Billable.SMS).findFirst().get();
		assertEquals(2, smsBill.getCount());

		SubscriptionConfig subscriptionConfig = business.getSubscription();
		double expectedSmsCost = subscriptionConfig.getSmsCost() * 2;
		assertEquals(expectedSmsCost, smsBill.getPrice() * smsBill.getCount());

		assertTrue(invoice.getSubtotal() <= invoice.getTotal());
		assertTrue(invoice.getTotal() == ((expectedSmsCost + subscriptionConfig.getPrice()) * (1 + invoice.getTax())));
	}

	@Test
	public void generateInvoiceShouldBillAddlOrdersCorrectlyIfOrdersExceedIncOrders() {
		Business business = getBusiness();
		SubscriptionConfig subscriptionConfig = business.getSubscription();
		subscriptionConfig.setIncOrders(0);

		Employee employee = entityFactory.generateEmployee();
		employee.setBusiness(business);
		employee = employeeService.save(employee);
		ZonedDateTime edt = DateTimeUtil.now().plusHours(2);

		ManualOrder orderRequest1 = new ManualOrder();
		orderRequest1.setBusinessKey(business.getKey());
		orderRequest1.setEmployeeKey(employee.getKey());
		orderRequest1.setContactNumber("somePhoneNumber");
		orderRequest1.setEmail("someEmail");
		orderRequest1.setAddress(entityFactory.generateAddress());
		orderRequest1.setEstimatedDeliveryTime(edt);

		// 3 orders created
		businessService.createManualOrder(orderRequest1);
		businessService.createManualOrder(orderRequest1);
		businessService.createManualOrder(orderRequest1);

		Invoice invoice = invoiceService.generateInvoice(business);
		List<BillableItem> items = invoice.getItems();
		assertFalse(items.isEmpty());

		BillableItem addlOrdersBill = items.stream().filter(i -> i.getBillable() == BillableItem.Billable.ADDL_ORDERS).findFirst().get();
		assertEquals(3, addlOrdersBill.getCount());

		final double addlOrdersPrice = subscriptionConfig.getAddlOrderCost() * 3;
		assertEquals(addlOrdersPrice, addlOrdersBill.getPrice() * 3);

		assertTrue(invoice.getSubtotal() <= invoice.getTotal());
		assertTrue(invoice.getTotal() == ((addlOrdersPrice + subscriptionConfig.getPrice()) * (1 + invoice.getTax())));
	}

	@Test
	public void generateInvoiceShouldBillAddlOrdersCorrectlyIfOrdersNotExceedIncOrders() {
		Business business = getBusiness();
		SubscriptionConfig subscriptionConfig = business.getSubscription();
		subscriptionConfig.setIncOrders(100);

		Employee employee = entityFactory.generateEmployee();
		employee.setBusiness(business);
		employee = employeeService.save(employee);
		ZonedDateTime edt = DateTimeUtil.now().plusHours(2);

		ManualOrder orderRequest1 = new ManualOrder();
		orderRequest1.setBusinessKey(business.getKey());
		orderRequest1.setEmployeeKey(employee.getKey());
		orderRequest1.setContactNumber("somePhoneNumber");
		orderRequest1.setEmail("someEmail");
		orderRequest1.setAddress(entityFactory.generateAddress());
		orderRequest1.setEstimatedDeliveryTime(edt);

		// 3 orders created
		businessService.createManualOrder(orderRequest1);
		businessService.createManualOrder(orderRequest1);
		businessService.createManualOrder(orderRequest1);

		Invoice invoice = invoiceService.generateInvoice(business);
		List<BillableItem> items = invoice.getItems();
		assertFalse(items.isEmpty());

		Optional<BillableItem> addlOrdersBillOpt = items.stream().filter(i -> i.getBillable() == BillableItem.Billable.ADDL_ORDERS).findFirst();
		assertFalse(addlOrdersBillOpt.isPresent());
	}

	@Test
	public void generateInvoiceShouldHaveCorrectStateEvenThoughGeneratedMultipleTimesInASinglePayPeriod() {
		Business business = getBusiness();
		SubscriptionConfig subscriptionConfig = business.getSubscription();
		subscriptionConfig.setIncOrders(1);

		Employee employee = entityFactory.generateEmployee();
		employee.setBusiness(business);
		employee = employeeService.save(employee);
		ZonedDateTime edt = DateTimeUtil.now().plusHours(2);

		ManualOrder orderRequest1 = new ManualOrder();
		orderRequest1.setBusinessKey(business.getKey());
		orderRequest1.setEmployeeKey(employee.getKey());
		orderRequest1.setContactNumber("somePhoneNumber");
		orderRequest1.setEmail(null);
		orderRequest1.setAddress(entityFactory.generateAddress());
		orderRequest1.setEstimatedDeliveryTime(edt);

		// 3 orders created with phone numbers. this is 2 addl orders
		businessService.createManualOrder(orderRequest1);
		businessService.createManualOrder(orderRequest1);
		businessService.createManualOrder(orderRequest1);

		Invoice invoice = invoiceService.generateInvoice(business);
		double subtotal = 0;
		for (BillableItem billItem : invoice.getItems()) {
			double price = 0;
			switch (billItem.getBillable()) {
				case SUBSCRIPTION:
					price = subscriptionConfig.getPrice();
					break;

				case SMS:
					price = subscriptionConfig.getSmsCost() * 3;
					break;

				case ADDL_ORDERS:
					price = subscriptionConfig.getAddlOrderCost() * 2;
					break;
			}
			assertEquals(price, billItem.getPrice() * billItem.getCount());
			subtotal += price;
		}
		assertEquals(invoice.getSubtotal(), subtotal);
		assertTrue(invoice.getTotal() >= subtotal);

		// add addl 2 orders with 2 sms
		businessService.createManualOrder(orderRequest1);
		businessService.createManualOrder(orderRequest1);

		invoice = invoiceService.generateInvoice(business);
		subtotal = 0;
		for (BillableItem billItem : invoice.getItems()) {
			double price = 0;
			switch (billItem.getBillable()) {
				case SUBSCRIPTION:
					price = subscriptionConfig.getPrice();
					break;

				case SMS:
					price = subscriptionConfig.getSmsCost() * 5;
					break;

				case ADDL_ORDERS:
					price = subscriptionConfig.getAddlOrderCost() * 4;
					break;
			}
			assertEquals(price, billItem.getPrice() * billItem.getCount());
			subtotal += price;
		}
		assertEquals(invoice.getSubtotal(), subtotal);
		assertTrue(invoice.getTotal() >= subtotal);
	}

	// TODO: add test cases for paying invoice and paid invoices
	@Test
	public void payInvoiceShouldSetPaymentNotNull() {
		Business business = getBusiness();
		Invoice invoice = invoiceService.generateInvoice(business);
		assertNull(invoice.getPayment());

		invoiceService.payInvoice(invoice);
		assertNotNull(invoice.getPayment());
	}

	@Test
	public void payInvoiceShouldFailIfAlreadyPaid() {
		Business business = getBusiness();
		Invoice invoice = invoiceService.generateInvoice(business);
		invoiceService.payInvoice(invoice);
		try {
			invoiceService.payInvoice(invoice);
			fail();
		} catch (ConcerningClientException ex) {}
	}

	@Test
	public void generateInvoiceShouldNotNeedToPayForFreePlan() {
		Business business = getBusinessWithFreePlan();
		Invoice invoice = invoiceService.generateInvoice(business);
		assertNotNull(invoice.getPayment());

		try {
			invoiceService.payInvoice(invoice);
			fail();
		} catch (ConcerningClientException ex) {}
	}

	@Test
	public void generateInvoiceForPreviousMonthShouldGenerateCorrectNumberOfInvoices() {
		Business business1 = getBusiness();
		business1.setCreatedDate(business1.getCreatedDate().minusMonths(1));

		Business business2 = getBusiness();
		business2.setCreatedDate(business2.getCreatedDate().minusMonths(1));

		Business business3 = getBusiness();
		business3.setCreatedDate(business3.getCreatedDate().minusMonths(1));

		final int genCnt = scheduledJobs.generateInvoiceForPreviousMonth();
		assertTrue(genCnt >= 3);

		List<Invoice> invoices = invoiceService.getRepo().findAllByPaymentNull();
		assertTrue(invoices.size() >= 3);

		List<Business> businessList = invoices.stream().map(i -> i.getBusiness()).collect(Collectors.toList());
		assertTrue(businessList.contains(business1));
		assertTrue(businessList.contains(business2));
		assertTrue(businessList.contains(business3));
	}

	@Test
	public void disableBusinessesThatDidNotPayBillShouldWorkProperly() {
		Business business1 = getBusiness();
		business1.setCreatedDate(business1.getCreatedDate().minusMonths(2));
		invoiceService.generateInvoice(business1, YearMonth.now().minusMonths(2));

		Business business2 = getBusiness();
		business2.setCreatedDate(business2.getCreatedDate().minusMonths(2));
		Invoice invoice2 = invoiceService.generateInvoice(business2, YearMonth.now().minusMonths(2));
		invoiceService.payInvoice(invoice2);

		Business business3 = getBusiness();
		business3.setCreatedDate(business3.getCreatedDate().minusMonths(2));
		invoiceService.generateInvoice(business3, YearMonth.now().minusMonths(2));

		final int cnt = scheduledJobs.disableBusinessesThatDidNotPayBill();
		assertEquals(2, cnt);

		assertFalse(business1.isEnabled());
		assertFalse(business3.isEnabled());
	}

	@Test
	public void businessWithFreePlanCannotSendSms() {
		Business business = getBusinessWithFreePlan();
		Employee employee = entityFactory.generateEmployee();
		employee.setBusiness(business);
		employee = employeeService.save(employee);
		ZonedDateTime edt = DateTimeUtil.now().plusHours(2);

		// create a few orders and test
		ManualOrder orderRequest1 = new ManualOrder();
		orderRequest1.setBusinessKey(business.getKey());
		orderRequest1.setEmployeeKey(employee.getKey());
		orderRequest1.setContactNumber("1231231234");
		orderRequest1.setEmail(null);
		orderRequest1.setAddress(entityFactory.generateAddress());
		orderRequest1.setEstimatedDeliveryTime(edt);
		businessService.createManualOrder(orderRequest1);

		Invoice invoice = invoiceService.generateInvoice(business);
		boolean noSmsBill = invoice.getItems().stream().filter(i -> i.getBillable() == BillableItem.Billable.SMS).findFirst().isEmpty();
		assertTrue(noSmsBill);
	}

	@Test
	public void businessWithFreePlanCanSendSmsIfPrecharged() {
		Business business = getBusinessWithFreePlan();
		business.setFreeSmsCount(3);
		Employee employee = entityFactory.generateEmployee();
		employee.setBusiness(business);
		employee = employeeService.save(employee);
		ZonedDateTime edt = DateTimeUtil.now().plusHours(2);

		// create a few orders and test
		ManualOrder orderRequest1 = new ManualOrder();
		orderRequest1.setBusinessKey(business.getKey());
		orderRequest1.setEmployeeKey(employee.getKey());
		orderRequest1.setContactNumber("1231231234");
		orderRequest1.setEmail(null);
		orderRequest1.setAddress(entityFactory.generateAddress());
		orderRequest1.setEstimatedDeliveryTime(edt);

		businessService.createManualOrder(orderRequest1);
		businessService.createManualOrder(orderRequest1);
		businessService.createManualOrder(orderRequest1);

		Invoice invoice = invoiceService.generateInvoice(business);
		boolean noSmsBill = invoice.getItems().stream().filter(i -> i.getBillable() == BillableItem.Billable.SMS).findFirst().isEmpty();
		assertTrue(noSmsBill);
		assertEquals(0, business.getFreeSmsCount());

		businessService.createManualOrder(orderRequest1);
		businessService.createManualOrder(orderRequest1);

		invoice = invoiceService.generateInvoice(business);
		noSmsBill = invoice.getItems().stream().filter(i -> i.getBillable() == BillableItem.Billable.SMS).findFirst().isEmpty();
		assertTrue(noSmsBill);
		assertEquals(0, invoice.getTotal());
	}

}
