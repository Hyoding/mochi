package com.ryusoftwareinc.mochi.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ryusoftwareinc.mochi.business.model.Order;
import com.ryusoftwareinc.mochi.business.model.TrackOrder;
import com.ryusoftwareinc.mochi.business.repository.OrderRepository;
import com.ryusoftwareinc.mochi.business.service.TripService;

@Service
public class CustomerService {

	@Autowired
	OrderRepository orderRepo;
	@Autowired
	TripService tripService;
	@Autowired
	CustomerSurveyRepository customerSurveyRepository;

	public TrackOrder trackOrder(String orderKey) {
		orderRepo.validateKey(orderKey);
		Order order = orderRepo.findByKey(orderKey);
		return trackOrder(order);
	}
	
	public TrackOrder trackOrder(Order order) {
		return tripService.trackOrder(order);
	}

	public void submitSurvey(CustomerSurveyDto dto) {
		Order order = orderRepo.findByKey(dto.getOrderKey());
		CustomerSurvey customerSurvey = customerSurveyRepository.findByOrder(order);
		if (customerSurvey == null) {
			customerSurvey = new CustomerSurvey();
			customerSurvey.setOrder(order);
		}
		customerSurvey.setProductRating(dto.getProductRating());
		customerSurvey.setCourierRating(dto.getCourierRating());
		customerSurvey.setComment(dto.getComment());
		customerSurveyRepository.save(customerSurvey);
	}
	
}
