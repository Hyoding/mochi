package com.ryusoftwareinc.mochi.subscription;

import com.ryusoftwareinc.mochi.core.http.BaseHttpRequest;

public class SubscriptionConfigDto extends BaseHttpRequest {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
