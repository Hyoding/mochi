package com.ryusoftwareinc.mochi.business.model;

import com.ryusoftwareinc.mochi.business.model.Order.OrderStatus;
import com.ryusoftwareinc.mochi.core.model.Address;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

public class ManualOrder {

	private String employeeKey;
	private String businessKey;
	private String msgFromBusiness;
	private String contactNumber;
	private String email;
	private Address address;
	private ZonedDateTime estimatedDeliveryTime;
	private OrderStatus orderStatus = OrderStatus.ACCEPTED;
	private Set<String> tags;
	
	public ManualOrder() {
		tags = new HashSet<>();
	}
	
	public ManualOrder(OrderDto dto) {
		this.employeeKey = dto.getRequester().getUserKey();
		this.businessKey = dto.getBusinessKey();
		this.msgFromBusiness = dto.getNote();
		this.contactNumber = dto.getPhoneNumber();
		this.email = dto.getEmail();
		this.address = dto.getAddress();
		this.estimatedDeliveryTime = dto.getDeliverBy();
		this.tags = dto.getTagsKey();
	}
	
	public String getBusinessKey() {
		return businessKey;
	}
	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}
	public String getMsgFromBusiness() {
		return msgFromBusiness;
	}
	public void setMsgFromBusiness(String msgFromBusiness) {
		this.msgFromBusiness = msgFromBusiness;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public ZonedDateTime getEstimatedDeliveryTime() {
		return estimatedDeliveryTime;
	}
	public void setEstimatedDeliveryTime(ZonedDateTime estimatedDeliveryTime) {
		this.estimatedDeliveryTime = estimatedDeliveryTime;
	}
	public OrderStatus getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getEmployeeKey() {
		return employeeKey;
	}

	public void setEmployeeKey(String employeeKey) {
		this.employeeKey = employeeKey;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}
}