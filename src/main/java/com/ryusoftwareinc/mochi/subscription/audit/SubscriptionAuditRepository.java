package com.ryusoftwareinc.mochi.subscription.audit;

import com.ryusoftwareinc.mochi.core.repository.EntityRepository;

public interface SubscriptionAuditRepository extends EntityRepository<SubscriptionAudit> {

}
