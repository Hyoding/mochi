package com.ryusoftwareinc.mochi.api;

import com.ryusoftwareinc.mochi.business.model.ApiOrder;
import com.ryusoftwareinc.mochi.business.model.ImportOrdersDto;
import com.ryusoftwareinc.mochi.business.model.LoadOrdersDto;
import com.ryusoftwareinc.mochi.core.controller.AbstractBaseController;
import com.ryusoftwareinc.mochi.core.http.HttpResponse;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
    This is public API
 */
@RequestMapping("/api/order/v1")
@RestController
@CrossOrigin
public class BusinessApiController extends AbstractBaseController {

    private static final Logger log = Logger.getLogger(BusinessApiController.class);

    @Autowired
    ApiService service;

    @PostMapping("create")
    public HttpResponse createApiOrder(@RequestBody ApiOrder request) {
        log.info("create " + request.getKey());
        validateRequest(request);
        ApiOrder order = service.saveApiOrder(request);
        return formResponse(order, "Order created");
    }

    @PostMapping("update")
    public HttpResponse editOrder(@RequestBody ApiOrder apiDto) {
        log.info("update" + apiDto.getOrderKey());
        validateRequest(apiDto);
        ApiOrder order = service.updateApiOrder(apiDto);
        return formResponse(order, "Order updated");
    }

    @PostMapping("cancel")
    public HttpResponse cancelOrder(@RequestBody ApiOrder apiDto) {
        log.info("cancel" + apiDto.getOrderKey());
        validateRequest(apiDto);
        service.cancelOrder(apiDto.getOrderKey());
        return formResponse(null, "Order canceled");
    }

    @PostMapping(value = "loadFile", consumes = {"multipart/form-data"})
    public HttpResponse loadOrdersFile(@ModelAttribute LoadOrdersDto dto) {
        log.info("loadFile");
        validateRequest(dto);
        List<ApiOrder> orders = service.convertToOrders(dto);
        return formResponse(orders, "Import orders successful");
    }

    @PostMapping("import")
    public HttpResponse importOrders(@RequestBody ImportOrdersDto dto) {
        log.info("import");
        validateRequest(dto);
        service.importOrders(dto.getOrders());
        return formResponse(null, "Orders imported");
    }

}
