package com.ryusoftwareinc.mochi;

import com.ryusoftwareinc.mochi.core.util.DateTimeUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

@SpringBootTest
class MochiApplicationTests {

	@Test
	void contextLoads() {
		ZonedDateTime d = ZonedDateTime.now();
		System.out.println(d);
		ZonedDateTime c = d.withZoneSameInstant((DateTimeUtil.ZONE_ID));
		System.out.println(c);
		ZoneId id = ZoneId.of("-4");
		System.out.println(id);
		System.out.println(DateTimeUtil.ZONE_ID);

		String str = DateTimeUtil.format(d, "gibberish");
		System.out.println(str);
	}

}
