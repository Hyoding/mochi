package com.ryusoftwareinc.mochi.customer;

import com.ryusoftwareinc.mochi.business.model.TrackOrder;
import com.ryusoftwareinc.mochi.core.controller.AbstractBaseController;
import com.ryusoftwareinc.mochi.core.http.HttpResponse;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/customer/v1")
@RestController
@CrossOrigin
public class CustomerController extends AbstractBaseController {

    private static final Logger log = Logger.getLogger(CustomerController.class);

    @Autowired CustomerService service;

    @GetMapping("trackOrder")
    public HttpResponse trackOrder(@RequestParam String orderKey) {
        log.info("trackOrder " + orderKey);
        TrackOrder trackOrder = service.trackOrder(orderKey);
        return formResponse(trackOrder, null);
    }

    @PostMapping("submitSurvey")
    public HttpResponse submitSurvey(@RequestBody CustomerSurveyDto dto) {
        log.info("submitSurvey");
        service.submitSurvey(dto);
        return formResponse(null, "Thanks for submitting the survey!");
    }

}